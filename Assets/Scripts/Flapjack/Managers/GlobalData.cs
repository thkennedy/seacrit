﻿using UnityEngine;

public class GlobalData : MonoBehaviour
{	
	public static GlobalData GdInstence;
    public static SoundManager SoundMan;

	// Layerinfo
	public int _layerSceneGeo = 13;
	public static int LayerSceneGeo {get{return GdInstence._layerSceneGeo;}}
	public int _layerPlayer = 16;
	public static int LayerPlayer {get{return GdInstence._layerPlayer;}}
	public int _layerFishPrey = 14;
	public static int LayerFishPrey {get{return GdInstence._layerFishPrey;}}
	public int _layerFishPredator = 15;
	public static int LayerFishPredator {get{return GdInstence._layerFishPredator;}}
	
	//Sprites
	public GameObject _spriteYUM;
	public static GameObject SpriteYUM {get{return GdInstence._spriteYUM;}}
	
	public GameObject _spriteHit;
	public static GameObject SpriteHit {get{return GdInstence._spriteHit;}}
	
	//XML
	public TextAsset XMLSpawner;
	public TextAsset XMLZoneData;
	public TextAsset XMLAssetsData;
	
    // Use this for initialization
    void Awake () 
	{
        SoundMan = gameObject.GetComponent<SoundManager>();
    }    
}