using UnityEngine;
using System.Collections;

public class Fish: PoolResource
{
	public string FishName;

	public bool CanEat = false; 			// Can this fish eat things?
	public bool CanDamagePlayer = false;	// if false no damage is done to player on collisions, bites, etc
	
	
	
	// CORE STATS that can be altered
	
	// Size
	[HideInNormalInspector] public float FishWeight = 1.0f;		// linear value representing the size of the fish - non-linear transform to get localscale
	public float FishWeightMin = 0.1f;	// minimum size of fish - can't be damaged below this but can be eaten
	public float FishWeightMax = 0.5f;
	public float FishWeightGrowRate = 2.0f;
	public float initWeightMean = 0.4f;
	public float initWeightVariance = 0.2f;
	
	// Energy
	public float Energy = 100.0f;			// current energy
	public float EnergyMax = 100.0f;		// Maximum energy we can have
	public float EnergyRegenRate = 0.0f;		// rate energy increases per second
	
	// Acquired Speed boost
	[HideInNormalInspector] public float AcquiredSpeedBoost = 0.0f;	// linear speed boost - gets non-linear transform applied to speed
	
	// Attack
	public float Attack = 0.1f;
	
	// Defense (note American spelling)
	public float Defense = 0.1f;
	
    // Links to other components and structures
	[HideInNormalInspector] public FishAsset Data;
	
	[HideInNormalInspector] public Movement Move;
	[HideInNormalInspector] public CoreAI Ai;
	[HideInNormalInspector] public FoodResourceInfo FoodRes;
	
	protected int ActiveGroupSize; //how big is my flock?
	 
	protected GameObject Mouth;
	protected float SizeMod = 1f;
	
	//biting variables
	private float _biteTimer; //Used to see when the player last bit a fish.
	private float _biteDelay = 1; //Used with biteTimer to ensure biting doesn't happen in consecutive frames
	private float _biteDamageMod = 0.1f; //modifies the damage done by biting.  Higher value --> more damage
	
	public bool FishActive = true; //for disabling the Update loop

	[HideInNormalInspector] public float TimeSinceSpawn;
	[HideInNormalInspector] public float MinimumVelocityForDamage = 100f;	// We will do damage if we hit a fish and we are going faster than this

	[HideInNormalInspector] public bool IsBeingEaten = false;
	[HideInNormalInspector] public Fish FishEatingMe = null;		// link to a fish that is eating this one
	[HideInNormalInspector] public Transform BeingEatenTransform = null;
	private float _eatenRate;
	public float minEatSize = 0.05f;		// kill the fish if its below this
	
	// ANIMATION STATES
	[HideInNormalInspector] public int NumFishBeingEaten = 0;		// used to keep track of when we finish chewing
	[HideInNormalInspector] public bool isChewing = false;
	[HideInNormalInspector] public bool isBiting = false;
	[HideInNormalInspector] public bool isBeingEaten = false;
	
	// DAMAGE TAKING OPTIONS
	public bool damageSize = true;		// if true damage gets applied to FishWeight
	public bool damageEnergy = false;		// if true damage gets applied to energy
	public float damageEnergyScale = 100.0f;
	public float gobbleScale = 0.75f;
	
	// Nutrition
	public float NutritionSizeMod = 1.0f;
	public float StatGainEnergyMax = 0.0f;
	public float StatGainSpeedBoost = 0.0f;
	
	// general convenience flags
	public bool isPlayer;		// is this the player fish?
	public bool isNPC;			// AI fish

	//UI
	private Material _healthBar;
	
	// DEATH OPTIONS
	public Component ItemToDropOnDeath=null;
	
	
// ----------------------------------------------- INIT FUNCTIONS --------------------------------------------

	
	void Start()
	{	
		Initialize();
	}
        
	public virtual void Initialize()
	{
		gameObject.tag = "Fish";
		Move = gameObject.GetComponent<Movement>();
		Ai = gameObject.GetComponent<CoreAI>();
		FoodRes  = gameObject.GetComponent<FoodResourceInfo>();
		float delta = Random.Range (-1.0f, 1.0f);
		Resize(initWeightMean+ initWeightVariance * delta, FishWeightMax );
		FoodRes.setFishMeatValue(GetSize());	// TODO - add this to resize component?
		
		Move.AppliedForce *= (1.0f + delta * 0.2f);
		
		TimeSinceSpawn = 0f;
		
		// set up the healthbar
		
		// set health bar
		_healthBar = null;
		Transform healthTrans = transform.Find("HealthBar");
		//Debug.Log("healthTrans  = "+healthTrans);
		if (healthTrans!=null)
		{
			
			MeshRenderer[] mr = healthTrans.GetComponentsInChildren<MeshRenderer> ();
			foreach (MeshRenderer r in mr)
			{
				//Debug.Log("mat name = "+r.materials[0].name);
				if (r.materials[0].name == "New Material (Instance)")
				{
					_healthBar = r.materials[0];
				}
			}
		}
		//Debug.Log("Healthbar mat  = "+_healthBar);
		
			
		
		
		return;
	}

    public virtual Fish ReInitialize(FishAsset fishData, Movement.Zplane zPlane)
	{
		/* Reinint/Restart the Fish, Give it the correct refrences and construct new non-unity Objects
		* */
		
		
		if(Move == null)
		{
		    Debug.LogError("ReInit - no movement component found");
		}
		//this.movementData=moveData;
		
		Ai = gameObject.GetComponent<CoreAI>();
		//this.animationData=animData;
		// raycaster = transform.Search("Raycaster");
		
		return this;
	}

	
// ----------------------------------------------------- UPDATE FUNCTIONS ------------------------------------
	
	protected virtual void FixedUpdate()
	{
		_biteTimer += Time.deltaTime;
		if (IsBeingEaten)
		{
			FishWeight -= _eatenRate * Time.deltaTime;
			if (FishWeight < minEatSize)
			{
				Kill();
			}
			else
			{
				Resize(FishWeight,FishWeightMax );
			}
		}
	}

	public void Update()
	{
		TimeSinceSpawn += Time.deltaTime;
		Energy += EnergyRegenRate * Time.deltaTime;
		if (Energy > EnergyMax)
		{
		    Energy = EnergyMax;
		}
		isChewing = NumFishBeingEaten >0;
		UpdateAnimationParameters();
	}
	
// ------------------------------------------------- ANIMATION FUNCTIONS -------------------------------------

	public virtual void UpdateAnimationParameters()
	{
		Animator animHead = transform.GetComponentInChildren<Animator>();
		//Animator animHead = animator;
		//Debug.Log("stateinfo = "+animHead.GetCurrentAnimatorStateInfo(0).nameHash);
		
		if (animHead ==null) { return;}
		//Debug.Log("Amin.avatar = " +anim.avatar+"  isvalid  = "+anim.avatar.isValid);
		if (animHead == null)
		{
			Debug.LogError("Fish "+gameObject.name+" does not have a body animator component!");
			return;
		}
		//anim.animation["Swim"].speed = 0.5;
		animHead.SetBool("isChewing",isChewing);
		animHead.SetBool("isBiting",isBiting);
		
		Animator animBody = transform.GetComponentInChildren<Animator>();
		//Animator animBody = animator;
		//Debug.Log("animBody = "+animBody);
		if (animBody == null)
		{
			Debug.LogError("Fish "+gameObject.name+" does not have a head animator component!");
			return;
		}
		float turn = Move.turningValue*0.01f;
		float swimspeed = Mathf.Abs(transform.rigidbody.velocity.magnitude*0.3f) +  Mathf.Abs(turn)*3.0f;
		if (!isAlive() )
		{
			swimspeed = -1f;
			turn = 0f;
		}
		animBody.SetFloat("SwimSpeed",swimspeed);
		animBody.SetFloat("TurnRight",turn);
		animBody.SetBool("isBeingEaten",isBeingEaten);
		
		if (_healthBar!=null)
		{
			_healthBar.SetFloat("_Health", Energy / EnergyMax);
		}
		
		
		
	}
	
// ------------------------------------------------------ GENERAL STAT FUNCTIONS ------------------------------
        
	
	public void Resize(float curWeight, float maxWeight)
	{
		FishWeight = curWeight;
		FishWeightMax = maxWeight;
		transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f) * FishWeight; //TODO make non-linear
	}
	/*
	public void AddSize(float amt)
	{
		Resize(FishWeight + amt);
	}
	
	public void MultiplySize(float mod)
	{
		Resize(FishWeight * mod);
	}
	*/
	public float GetSize()
	{
	    return FishWeight;
	}
	
	
	public void AddStats(Fish foe)
	{
		EnergyMax += foe.StatGainEnergyMax;
		AcquiredSpeedBoost += foe.StatGainSpeedBoost;
		Debug.Log("New EnergyMax = "+EnergyMax);
		Debug.Log("New AcquiredSpeedBoost = "+AcquiredSpeedBoost);
	}

// -------------------------------------- EATING AND GROWING -----------------------------------------------------
        
	public void Eat(Fish foe)
	{
		Debug.LogError("eat called in fish!");
		return;
	}
	
	public void GrowByEating(float amt) // increases FishWeight in a non-linear way
	{
		float cursize = GetSize();
		float ds = amt * FishWeightGrowRate * (1f - cursize / FishWeightMax);	// increase non-linear and peak at FishWeightMax
		Resize(cursize + ds, FishWeightMax + ds *  cursize / FishWeightMax );	// increase FishWeight
	}
	
	public bool EatCheck(Fish f)
	{
		if (f.damageSize)
		{
			return GetSize () > f.GetSize () / f.gobbleScale;
		}
		if (f.damageEnergy)
		{
			return f.Energy <= 0.01f;
		}
		return false;
	}

// --------------------------------------- DAMAGE AND DEATH ---------------------------------------------------

	public float GetHeadbuttDamage(Fish foe)
	{
		return 0.1f;
	}

	public void TakeDamage(float amount,Fish nastyFish)
	{
		
		if (Ai!=null)
		{
			Ai.TakeDamage(amount,nastyFish);		// allow the ai to respond
		}
		if (damageSize)
		{
			float x = FishWeight - amount;
			if (x < FishWeightMin) { x= FishWeightMin;}
			Resize (x, FishWeightMax);
			return;
		}
		if (damageEnergy)
		{
			float x = Energy - amount * damageEnergyScale;
			if (x < 0) { x= 0;}
			Energy = x;
			Debug.Log("Fish taking energy damage!  energy now  = "+Energy);
			float damageLevel = 1.0f -Energy/ EnergyMax;
			SkinnedMeshRenderer mesh = transform.GetComponentInChildren<SkinnedMeshRenderer>();
			mesh.materials[0].SetFloat("_Damage",damageLevel);
			return;
		}
	}
	
	public float ModifyDealtDamage(float x) // allows the fish to modify damage given out based on their stats
	{
		return x;
	}

	public void Kill()
	{
		//Debug.Log("Killing fish : "+gameObject.name);
		//var spawner = GameObject.Find("Managers").GetComponent<Spawner>();	
		
		if (FishEatingMe !=null)
		{
			FishEatingMe.NumFishBeingEaten--;
		}
		Spawner.GotEaten(this);
		isBeingEaten = false; // TODO zero other animation states
	}
	
	public void BeingEaten(Fish FishEating, Transform eatpos, float eatrate)
	{
		Helper.SetCollidersEnable(gameObject, false);	// disable collisions
		Debug.Log(FishEating.name + " is eating "+ name +"  eatpos = "+eatpos);
		BeingEatenTransform = eatpos;
		IsBeingEaten = true;
		_eatenRate = eatrate;
		FishEatingMe = FishEating;
		gameObject.rigidbody.velocity = Vector3.zero;
		gameObject.rigidbody.angularVelocity = Vector3.zero;
		gameObject.rigidbody.Sleep();
		isBeingEaten = true;
		if (ItemToDropOnDeath!=null)
		{
			Debug.Log("Dropping : "+ItemToDropOnDeath);
			Component dropped = (Component) Instantiate(ItemToDropOnDeath);
			dropped.transform.position = transform.position;
		}
    }   

// -------------------------------------- MISC -----------------------------------------------------------
    
    public bool isAlive()
    {
    	    return Energy > 0.0f;
    }
}