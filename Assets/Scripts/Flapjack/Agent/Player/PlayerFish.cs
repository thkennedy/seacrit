﻿using UnityEngine;
using System.Collections;

public class PlayerFish : Fish
{
    
	private InputManager _input;
	private CameraScript _cam;

	// UI
	private Material _rightDial;
	private Material _leftDial;
	private Material _energyBar;
	private Material _weightBar;
	private MeshRenderer _rightBlink;
	private MeshRenderer _leftBlink;
	private bool doRightBlink = false;
	private bool doLeftBlink = false;
	
	private float _blinkAmount;		// used to blink the dial
	private float blinkSpeed = 4f;
	[HideInInspector] public bool LeftAbilUsed = false;
	[HideInInspector] public bool RightAbilUsed = false;
	
	// FishParts
	public Component IntitialHead;
	public Component IntitialBody;
	
	[HideInInspector] public Component Body;
	[HideInInspector] public Component Head;
	
	// misc
	[HideInNormalInspector] public Zone currentZone = null;
	
	//Abilities
	void Start()
	{
		Initialize();
	}
	
    public void LateInit()
    {
    	gameObject.name = "0" + FishName;
            
    	Debug.Log ("LateInit of: "+ gameObject.name + " mod: " + SizeMod + " size: " + GetSize());
    }
	
    public override void Initialize()
	{
		Debug.Log("Doing playerFish Initialize");
		gameObject.tag = "Player";
		FishName = name;
		Move = gameObject.GetComponent<Movement>();
		_cam = GameObject.Find("Main Camera").GetComponent<CameraScript>();
		_cam.Initialise(gameObject);
		_input = gameObject.GetComponent<InputManager>();
		GameObject.Find("Managers").GetComponent<WaterAndSkyManager>().Initialise(gameObject);
		Resize (initWeightMean,FishWeightMax );
		
		gameObject.transform.ChangeLayersRecursively("FishPlayer");
		//gameObject.layer = GlobalData.LayerPlayer;

		// UI setup
		// find UI Components 
		MeshRenderer[] mr = GameObject.Find ("UI Root (2D)").GetComponentsInChildren<MeshRenderer> ();
		for (int i =0; i< mr.Length; i++)
		{
			if (mr[i].materials[0].name == "RadialUI 2 (Instance)")
			{
				_rightDial = mr[i].materials[0];
			}
			if (mr[i].materials[0].name == "RadialUI (Instance)")
			{
				_leftDial = mr[i].materials[0];
			}
			if (mr[i].materials[0].name == "Energy Bar (Instance)")
			{
				_energyBar = mr[i].materials[0];
			}
			if (mr[i].name == "radialUIBubbleRight")
			{
				_rightBlink = mr[i];
			}
			if (mr[i].name == "radialUIBubbleLeft")
			{
				_leftBlink = mr[i];
			}
			if (mr[i].name == "StatBarWeight")
			{
				_weightBar = mr[i].materials[0];
			}
			Debug.Log ("name " + mr[i].name+"  mat name = "+mr[i].materials[0].name);
		}
		Debug.Log ("_weightBar = " + _weightBar);
		return ;
    }
	
    void Update()
	{
		base.Update();
		
		// UI 
		_energyBar.SetFloat ("_Progress",( Energy / EnergyMax) );
		_weightBar.SetFloat ("_Progress",( FishWeight / FishWeightMax) );
		
		_blinkAmount += blinkSpeed * Time.deltaTime;
		if (_blinkAmount > 1.0f) { _blinkAmount = 0f;}

		if (doRightBlink)
		{
			_rightBlink.enabled = true;
			if (_blinkAmount > 0.5f)
			{
				_rightBlink.enabled = false;
			}
		}
		if (doLeftBlink)
		{
			_leftBlink.enabled = true;
			if (_blinkAmount > 0.5f)
			{
				_leftBlink.enabled = false;
			}
		}
		
		
		
	}
	
	public void setLeftDialActive( bool act)
	{
		doLeftBlink = !act;
		if (act)	// make sure we re-enable the dial in case it was disabled
		{
			_leftBlink.enabled = true;
		}
	}
	
	public void setRightDialActive( bool act)
	{
		doRightBlink = !act;
		if (act)
		{
			_rightBlink.enabled = true;
		}
	}
	
	public void setLeftDialAmount(float x)
	{
		 _leftDial.SetFloat ("_Progress",x );
	}
	
	public void setRightDialAmount(float x)
	{
		 _rightDial.SetFloat ("_Progress",x );
	}

	// ------------------------------------------------- ANIMATION FUNCTIONS -------------------------------------

	public override void UpdateAnimationParameters()
	{
		Animator animHead = Head.GetComponentInChildren<Animator>();
		if (animHead == null)
		{
			Debug.LogError("Fish "+gameObject.name+" does not have a body animator component!");
			return;
		}
		animHead.SetBool("isChewing",isChewing);
		animHead.SetBool("isBiting",isBiting);
		
		Animator animBody = Body.GetComponentInChildren<Animator>();
		if (animBody == null)
		{
			Debug.LogError("Fish "+gameObject.name+" does not have a head animator component!");
			return;
		}
		float turn = Move.turningValue*0.01f;
		float swimspeed = Mathf.Abs(transform.rigidbody.velocity.magnitude*0.3f) +  Mathf.Abs(turn)*1.0f;
		animBody.SetFloat("SwimSpeed",swimspeed);
		animBody.SetFloat("TurnRight",turn);
		
		
	}

}