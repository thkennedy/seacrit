﻿using UnityEngine;
using System.Collections;

public class BossCreator : MonoBehaviour {

	// Used to create a boss fish - can be added directly to the level
	
	public string XMLPrefabFishName;	// name of XML fish to be used
	public float AnchorDist = 1.0f;
	
	public float PlayerDetectionDistance;	// distance at which the boss detects the player
	public float PlayerAttackDistance;		// distance at which the boss will attack the player
	
	private bool _firstUpdate = true;

	
	void CreateFish (string name, Movement.Zplane zPlane) 
    {
    	    /*
		// First spawn the new fish
        GameObject f = FishBuilder.CreateFish(name, zPlane);
		f.transform.position = transform.position;
		
		BossAI cai = f.GetComponentInChildren<BossAI>();
		cai.AnchorDist = AnchorDist;
		cai.AnchorPos = transform.position;
		
		cai.PlayerDetectionDistance = PlayerDetectionDistance;
		cai.PlayerAttackDistance = PlayerAttackDistance;
		*/
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (_firstUpdate)
		{
			CreateFish(XMLPrefabFishName, Movement.Zplane.Normal);	// Had to move this to update rather than Start() because otherwise FishBuilder in not initialized. PJ
			_firstUpdate = false;
		}
	}
	
	void OnDrawGizmos() 
    {
		Gizmos.color = new Color (0.0f, 1.0f, 0.0f, 0.7f);
		Gizmos.DrawSphere(transform.position,AnchorDist );
	}
}
