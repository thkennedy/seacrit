using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FishRepeller : MonoBehaviour {

	// This class is used to repel fish from geometry or other areas to help them avoid running into it!
	
	//public float power;

	public Vector3 offset;	// location or offset of this repeller

	public float fmax;
	public float dmin;
	public float dmax;

	public bool useVelocityFactor = true;

	public void setProperties(float fmax_in, float dmin_in, float dmax_in) {
		fmax = fmax_in;
		dmin = dmin_in;
		dmax = dmax_in;
	}

	public void setProperties(FishRepeller r) {
		if (r==null)
		{
			Debug.Log ("ERROR: null object passed to setProperties in FishRepeller", gameObject);
		}
		fmax = r.fmax;
		dmin = r.dmin;
		dmax = r.dmax;
	}

	public virtual Vector3 getForce(Vector3 fpoint, Vector3 vel_point, Vector3 vel_repeller = default(Vector3))	{// returns the force felt at <location> from this repeller
		//Debug.Log ("FishRepeller scale = "+scale, gameObject);
		if (fmax < 0.00000001f)	// ignore disabled repellers
		{
			return Vector3.zero;
		}
		Vector3 curvec = fpoint - (transform.position + offset);
		curvec = curvec / transform.lossyScale.x;

		float d = curvec.magnitude;	// distance between the two points / localscale
		if (d > dmax) { return Vector3.zero;}

		Vector3 relvel = vel_point - vel_repeller;
		curvec.Normalize ();
		float velfactor = 1.0f;

		if (useVelocityFactor) 
		{
			velfactor = 1.0f + 15f* relvel.magnitude* Mathf.Clamp (Vector3.Dot (-curvec, relvel.normalized), 0.0f, 1.0f);
		}
		if (d < dmin) 
		{
			return curvec * velfactor * fmax;
		}
		if (d <= dmax) 
		{
			return curvec * velfactor * fmax * ( 1.0f - (d - dmin) / (dmax - dmin) );		// linear falloff
		}
		// else d > dmax
		return Vector3.zero; // SHOULD NEVER HAPPEN!
	}

	void OnDrawGizmos() {
		Gizmos.color = new Color (1.0f, 0.0f, 0.0f, 0.2f);
		Gizmos.DrawSphere(transform.position + offset,dmin * transform.lossyScale.x );
		Gizmos.color = new Color (0.0f, 1.0f, 0.0f, 0.2f);
		Gizmos.DrawSphere(transform.position + offset,dmax * transform.lossyScale.x );
	}

}