﻿using UnityEngine;
using System.Collections;

public class FishRepellerLine : FishRepeller {

	public Vector3 p0 = new Vector3(0f,0f,0f);
	public Vector3 p1 = new Vector3(1f,0f,0f);		// define the points on the line
	
	
	// Use this for initialization
	void Start () {
		p0 = transform.GetChild(0).position;
		p1 = transform.GetChild(1).position;
	}
		
		
	public override Vector3  getForce(Vector3 fpoint, Vector3 vel_point, Vector3 vel_repeller = default(Vector3))	{// returns the force felt at <location> from this repeller
		//Debug.Log ("FishRepeller getForce");
		if (fmax < 0.00000001f)	// ignore disabled repellers
		{
			return Vector3.zero;
		}
		Vector3 curvec = fpoint - getClosestPoint(fpoint);
		curvec = curvec / transform.lossyScale.x;

		float d = curvec.magnitude;	// distance between the two points / localscale
		
		Vector3 relvel = vel_point - vel_repeller;
		curvec.Normalize ();
		float velfactor = 1.0f;

		if (useVelocityFactor) 
		{
			velfactor = 1.0f + 15f* relvel.magnitude* Mathf.Clamp (Vector3.Dot (-curvec, relvel.normalized), 0.0f, 1.0f);
		}
		if (d < dmin) 
		{
			return curvec * velfactor * fmax;
		}
		if (d < dmax) 
		{
			//Debug.Log("Returning force dir = "+curvec+"  mag = " + velfactor * fmax * ( 1.0f - (d - dmin) / (dmax - dmin) ) );
			return curvec * velfactor * fmax * ( 1.0f - (d - dmin) / (dmax - dmin) );		// linear falloff
		}
		// else d >=dmax
		return curvec * 0.0f;
	}
	
	private Vector3 getClosestPoint(Vector3 p_in) // returns the closest point on the line segment to p
	{
		//Debug.Log("Getting closest point to "+p_in);
		// first find intersection with line
		Vector3 p = p_in;
		Vector3 d = p1-p0;
		float t= Vector3.Dot(d , p - p0) / Vector3.Dot(d,d);
		if (t>=0f && t<=1f) 
		{ 
			//Debug.Log("t= "+t+" Returning "+ (p0 + t*d +transform.position + offset) );
			//Debug.DrawLine(p_in,  p0 + t*d, Color.red);
			return p0 + t*d  ;
		}
		p.z+=10000f;
		return p;
		// otherwise check end points
		d = p-p0;
		Vector3 d1 = p-p1;
		if ( (p-p0).magnitude < (p-p1).magnitude) // can probably just use t for this but too lazy to check!
		{
			//Debug.Log("Returning "+ (p0+transform.position + offset) );
			//Debug.DrawLine(p_in,  p0, Color.red);
			return p0;
		}
		//Debug.Log("Returning "+ (p1+transform.position + offset) );
		//Debug.DrawLine(p_in,  p1, Color.red);	
		return p1;
	}
	
	void OnDrawGizmos() {
		Vector3 tp0 = transform.GetChild(0).position;
		Vector3 tp1 = transform.GetChild(1).position;
		
		Gizmos.color = new Color (1.0f, 0.0f, 0.0f, 0.2f);
		Gizmos.DrawSphere(tp0,dmin * transform.lossyScale.x );
		Gizmos.DrawSphere(tp1,dmin * transform.lossyScale.x );
		Gizmos.DrawLine(tp0 , tp1  );
		Gizmos.DrawLine(tp0 + new Vector3(0f,dmin * transform.lossyScale.x,0f), tp1 + new Vector3(0f,dmin * transform.lossyScale.x,0f) );
		Gizmos.DrawLine(tp0 + new Vector3(0f,-dmin * transform.lossyScale.x,0f), tp1 + new Vector3(0f,-dmin * transform.lossyScale.x,0f) );
		
		Gizmos.DrawLine(tp0 + new Vector3(0f,dmin * transform.lossyScale.x*0.5f,0f), tp1 + new Vector3(0f,dmin * transform.lossyScale.x*0.5f,0f) );
		Gizmos.DrawLine(tp0 + new Vector3(0f,-dmin * transform.lossyScale.x*0.5f,0f), tp1 + new Vector3(0f,-dmin * transform.lossyScale.x*0.5f,0f) );
		
		
		Gizmos.color = new Color (0.0f, 1.0f, 0.0f, 0.2f);
		Gizmos.DrawSphere(tp0,dmax * transform.lossyScale.x );
		Gizmos.DrawSphere(tp1,dmax * transform.lossyScale.x );
		Gizmos.DrawLine(tp0 + new Vector3(0f,dmax * transform.lossyScale.x,0f), tp1 + new Vector3(0f,dmax * transform.lossyScale.x,0f) );
		Gizmos.DrawLine(tp0 + new Vector3(0f,-dmax * transform.lossyScale.x,0f), tp1 + new Vector3(0f,-dmax * transform.lossyScale.x,0f) );
		
		
		
		
		
		
	}
	
}
