﻿using UnityEngine;
using System.Collections;

public class FoodResourceInfo : MonoBehaviour {

	// This class contains information about what this object has to offer if eaten

	// PUBLIC

	// resources available
	public float plantFood = 0.0f;		// DO NOT modify these directly, use the setter methods.
	public float fishMeat = 0.0f;

	// rough size and location
	public Vector3 center = new Vector3(0,0,0);
	public float radius = 1.0f;

	// Static members
	static public int numSettings = 2; 	// WARNING - If you add any new resource types make sure you update these values!!
	static public int index_plantFood = 0;
	static public int index_fishMeat = 1;

	// PRIVATE

	private float[] resAsArray;
	

	void Start () {
		// create initial array
		createArray ();
		updateArray ();
	}

	void createArray()
	{
		resAsArray = new float[numSettings];
	}

	public void updateArray ()	// should be called whenever values are changed
	{
		if (resAsArray !=null)
		{
			resAsArray [index_plantFood] = plantFood;
			resAsArray [index_fishMeat] = fishMeat;
		}
	}

	public void setParameters(float plantFood_in, float fishMeat_in)
	{
		plantFood = plantFood_in;
		fishMeat = fishMeat_in;
		updateArray ();
	}

	public void setParameters(float[] inputs)	// NOTE - order should be the same as the above public variables
	{
		if (inputs.Length != numSettings)
		{
			Debug.LogError ("Invalid number of inputs passed to setParamenters in FoodResourceInfo");
			return;
		}
		// first update the array
		for (int i=0; i < inputs.Length; i++)
		{
			resAsArray[i]=inputs[i];
		}
		// now the individual values
		plantFood = resAsArray [index_plantFood];
		fishMeat = resAsArray [index_fishMeat];

	}

	public void setFishMeatValue(float value)
	{
		fishMeat = value;
		updateArray ();
	}

	public float getValue(float[] pref) // returns the dot product of pref with the resources
	{
		if (pref.Length != numSettings)
		{
			Debug.LogError("getValue in FoodResourceInfo called with preferences of invalid length! Returning 0");
			return 0f;
		}
		float val = 0f;
		for (int i=0; i<numSettings; i++)
		{
			val += pref[i]*resAsArray[i];
		}
		return val;
	}

	void OnDrawGizmos() {
		Gizmos.color = new Color (0.0f, 1.0f, 0.0f, 0.2f);
		Gizmos.DrawSphere(transform.position+center,radius );
	}


}
