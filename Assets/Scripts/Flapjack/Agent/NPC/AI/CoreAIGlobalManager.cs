using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoreAIGlobalManager : MonoBehaviour {
	
	// This class co-ordinates all of the AI and is responsible for managing global data required by the AIs.
	// This should be added to the playerFish object

	public int MaxNumRepellers = 200;			// maximum number of repellers we can consider
	public int MaxNumResources = 300;			// maximum number of resources we can consider
	public int MaxNumEaters = 200;			// maximum number of resources we can consider
	public float ScanRadius = 20;		// radius around the player to collect repellers in

	public FishRepeller[] Repellers;	// list of repellers currently in the vicinity of the player
	public int NumRepellers;			// number stored

	public FoodResourceInfo[] Resources;	// list of resources around
	public int NumResources;				// number stored

	public Fish[] Eaters;			// list of fish that eat others
	public int NumEaters;			// number stored

	
	// Use this for initialization
	void Start () {
		// initialise repelling structure - quicker to do once and then copy rather than create new structures each tick.
		Repellers = new FishRepeller[MaxNumRepellers];
		NumRepellers = 0;
		Resources = new FoodResourceInfo[MaxNumResources];
		NumResources = 0;
		Eaters = new Fish[MaxNumEaters];
		NumEaters = 0;
	}

	// Update is called once per frame
	void Update () 
	{
		FindNearbyObjects();	// finds repellers and resources etc that we should consider
	}

	private void FindNearbyObjects() 	// This creates a master list of all repelling objects and resources in the vicinity of the player.
	{
	// now get world hits
	
	int nrep = 0;
	int nres = 0;
	int neat = 0;
	Collider[] hitsWorld = Physics.OverlapSphere(transform.position, ScanRadius);
	//Debug.Log ("hits_world.length = " + hits_world.Length+"  pos = "+transform.position +" radius = "+scanRadius);
	
	for (int i=0; (i < hitsWorld.Length); i++) {
		// now find the repeller structure in each item
		//Debug.Log("i="+i+"  name = "+hitsWorld[i].collider.name);
		FishRepeller[] rep = hitsWorld[i].collider.GetComponentsInChildren<FishRepeller>();
		for (int j=0; j <rep.Length; j++)
		{
			//Debug.Log("NAME : "+rep[j].gameObject.name);
			Repellers[nrep++]=rep[j];
		}
		FoodResourceInfo res = hitsWorld[i].collider.gameObject.GetComponentInChildren<FoodResourceInfo>();
		//Debug.Log ("Collider "+i+" = " +hits_world[i].collider.gameObject.name+" res =  "+res);
		if (res != null)
		{
			Resources[nres++]=res;
		}
		Fish f = Helper.GetFish(hitsWorld[i].collider);
		if (f != null)
		{
			if (f.CanEat)
			{
			Eaters[neat++]=f;
			}
		}
	}
	
	NumRepellers = nrep;
	NumResources = nres;
	NumEaters = neat;
	}

}
