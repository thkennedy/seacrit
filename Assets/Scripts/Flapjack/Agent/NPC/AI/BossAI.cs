﻿using UnityEngine;
using System.Collections;

public class BossAI : CoreAI {

	/*
	public float PlayerDetectionDistance;	// distance at which the boss detects the player
	public float PlayerAttackDistance;		// distance at which the boss will attack the player
	public float AttackPause = 2.0f;
	public float DashForce = 2f;


    private bool _isHuntingPlayer;
	private float _attackTimer;
	private bool _attackReady = true;
	
	// Use this for initialization
	void Start () 
    {
		DoInit();
		gameObject.GetComponentInChildren<Fish>().CanDamagePlayer = true;	// so we can damage the player
		
		UseAnchor = true;
		AnchorPos = transform.position;
		
		ShowRandomTargetLines = true;
		//Debug.Log("boss TargetCycleTime = " + TargetCycleTime);
	}
	
	public void DoAttack()
	{
		//StartCoroutine(Abilities.IDash (gameObject, true, DashForce, true));
	}

    void Update() // This is the main class for movement and behaviour
    {			
		if (_newTargetCount < 0)
		{
			_newTargetCount = TargetCycleTime;
			_needsNewTarget =true;
		}
		
		_attackTimer += Time.deltaTime;
		_attackReady = _attackTimer >= AttackPause;
		
		if ( ReachedTarget() )
		{
			_needsNewTarget =true;
		}
		
		float playerd = (_playerFish.transform.position - transform.position).magnitude;
		float ad = ( transform.position - AnchorPos).magnitude;
		
		_isHuntingPlayer = (playerd <= PlayerDetectionDistance) && ( ad < AnchorDist);

		if (_isHuntingPlayer)
		{
			TargetPos = _playerFish.transform.position;
			TargetRadius = 0.1f;
			if (playerd < PlayerAttackDistance && _attackReady)
			{
				DoAttack();
				_attackTimer = 0.0f;
			}	
		}
		else
		{
			if (_needsNewTarget)
			{
				GetNextTarget();
				_newTargetCount = TargetCycleTime;
				_needsNewTarget=false;
			}
		}
		// now check the speed
		float maxspeed = IdleSpeedRatio;
		if (_isHuntingPlayer && HuntSpeedRatio > maxspeed) { maxspeed = HuntSpeedRatio;}
		_currentVelocityScale = maxspeed;
	}
	
	public override bool isHunting() { return true;}	// allows the autoeat to work
	*/
}
