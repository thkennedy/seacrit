﻿using UnityEngine;
using System.Collections;

public class AIState : MonoBehaviour {
	
	// this is a state of behaviour for a NPC fish
	public string stateName="UnknownState";
	
	public CoreAI core;	// what is controlling us
	public Animator anim;	// the animator we are using - so we can update animation variables
	
	// general movement settings
	public bool AvoidObstacles = true;		// if false won't bother trying to avoid geometry - so will bump into things
	public bool AvoidPredators = true;		// will speed up and try to get away from predators. Can adjust how close with PredForceFlee
	
	// force settings
	public float RepellerForceScale = 1.0f;		// this is used to adjust the strength of repellers
	public float AvoidForceScale = 2.0f;	// used to control the tangential force applied, to slip around a repeller
	public float AvoidWaterSurfaceDist = 0.5f;
	public float AvoidWaterSurfaceScale = 100f;

	
	// main parameter used to control movement
	public Vector3 moveVector;

	// movement parameters that get copied into fish at the end of the tick
	[HideInNormalInspector] public float _currentVelocityScale = 1.0f;		// percent of max velocity we are using.
	
	
	// Functions that can be overriden to implement behaviour
	
	public virtual void beginState() {}// called when entering the state
	public virtual void endState() {}	// called when ending the state
	public virtual void stateUpdate() {}	// update fn
	public virtual void stateFixedUpdate() {}	// fixedupdate fn
	public virtual float wantsControl() { return 0f;}	// used to determine how much this state wants to be active (higher is more urgent)
		// general ranges:
		// -10 	- actively dont want
		// 0  	- average - idle / default behaviour should be here
		// 10 	- really want
		// 20 	- URGENT!
	
	// interrupts
	public virtual void TakeDamage(float amount,Fish nastyFish) {}	// called if we take damage
		
	// convenience functions
	
	private void gotoState(string stateName) { core.gotoState(stateName); }
	
	public Vector3 GetOptimalDirection(Vector3 targetdir)	// returns the optimal movement direction to avoid predators, geometry etc
	{
		// basic forces
		Vector3 repForce = Vector3.zero;
		Vector3 avoidForce = Vector3.zero;
		Vector3 foodForce = Vector3.zero;
		// first calculate all of the required forces

		targetdir.Normalize();

		if (AvoidObstacles)
		{
			repForce = core.GetRepellerForce();
			float distBelowSurface = 10f - transform.position.y;
			if ( distBelowSurface > 0f && distBelowSurface < AvoidWaterSurfaceDist) // add force to avoid surface
			{
				repForce.y -= AvoidWaterSurfaceScale * (1.0f - distBelowSurface /AvoidWaterSurfaceDist );
			}
			//repForce *= Mathf.Sqrt( 1f+ core._velocitySmoothed.magnitude);
			avoidForce = getAvoidDirection (repForce, core._velocitySmoothed) * repForce.magnitude;		// direction to go around the repelling force
		}
		
		if (AvoidPredators)
		{
			repForce += core.GetPredatorForce();
		}

		Vector3 finalDir = targetdir + RepellerForceScale * (  repForce + avoidForce * AvoidForceScale) ;
		
		return finalDir.normalized;
		
	}
	
	private static Vector3 getAvoidDirection(Vector3 n, Vector3 desired) // returns a direction perpendicular to n which is closest to our desired direction
	{ 
		Vector3 d1 = new Vector3 (n.y, - n.x, 0);
		Vector3 d2 = new Vector3 (- n.y, n.x, 0);
		d1.Normalize();
		d2.Normalize();
		if (Vector3.Dot (d1, desired) > Vector2.Dot (d2, desired)) {
			return d1;
		}
		// else
		return d2;
	}
	
	public static Vector3 GetRandomTarget(Vector3 center, float radius, Vector3 defaultpos)	// returns a new target position, radius away
	{
		int maxtries = 20;
		int curtry = 0;
		Vector3 nt = defaultpos;
		float a = Random.Range(0f, 6.283f);
		bool valid = false;
		while (!valid && curtry < maxtries)
		{
			
			valid = true;
			a += 6.3f / maxtries;
		
			nt.x = center.x + radius * Mathf.Cos(a);
			nt.y = center.y + radius * Mathf.Sin(a);
			nt.z = 0f;
			
			if (nt.y>10f ) {valid = false;}	// discard this rather than salvage to preseve random dist
		
			curtry++;
			
			if (valid) { valid = Helper.isValidSpawnPoint(nt); }
		}
		if (valid) { return nt;}
		return defaultpos;
	}
}
