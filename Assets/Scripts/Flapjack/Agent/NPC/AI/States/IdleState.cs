﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("AI/IdleState")]

public class IdleState : AIState {
	
	// Idle behaviour consists of swimming to random targets
	public float SpeedRatio = 0.2f;	// ratio of total speed the fish uses when just roaming around.	
	public float TargetCycleTime = 1.0f;	// time in seconds between a new target being assigned to the fish in the idle state
	public float TargetRadius = 1.0f;		// radius to look for the next destination position
	[HideInNormalInspector] public Vector3 TargetPos;			// where this fish is heading
	private float _targetCycleTimer;		// timer used to cycle our next target position
     
	public virtual void beginState() // called when entering the state
	{
		TargetPos = GetRandomTarget(transform.position,TargetRadius,Vector3.zero);
		_targetCycleTimer = TargetCycleTime;
	}	
	//public virtual void endState() {}	// called when ending the state
	
	public override void stateUpdate()  // update fn
	{
		_targetCycleTimer -= Time.deltaTime;
		if (_targetCycleTimer < 0.0f) 
		{
			_targetCycleTimer = TargetCycleTime;
			TargetPos = GetRandomTarget(transform.position,TargetRadius,Vector3.zero);
		}
	}	
	
	public override void stateFixedUpdate() // fixedupdate fn
	{
		moveVector = GetOptimalDirection(TargetPos - transform.position) * SpeedRatio;
	}	
	
	public override float wantsControl() { return 0f;}	// used to determine how much this state wants to be active (higher is more urgent)
	
	
}
