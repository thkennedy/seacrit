﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("AI/HuntWithAbility")]

public class HuntWithAbility : AIState {
	
	// This behaviour will charge at the target then back of in cycles, triggering an ability as it goes
	
	public float SpeedRatio = 1.0f;	// ratio of total speed the fish uses when just roaming around.	
	
	public bool TriggerOnPlayerDamage = true;
	public bool TriggerOnNPCDamage = false;
	
	public float HuntTime = 2f;
	public float HuntPauseTime = 2f;
	public int NumberOfCycles = 2;
	
	public bool useAbility = false;
	public string AbilityTransformName;	// name of the ability we will trigger
	public float abilityCharge = 1.0f;
	
	private float _huntTimer;
	private int _cycleCount;
	
	public float ControlValueOnTrigger = 15f; // how much we want to hunt when damaged
	
	private Fish TargetFish;	// fish we are hunting
	
	[HideInNormalInspector] public Vector3 TargetPos;			// where this fish is heading
	
	private Ability abilityToUse;			// ability we will be using
	private bool	readyToFire = false;
	
	
	void Start()
	{
		Transform t = transform.Find(AbilityTransformName);
		if (t!=null) {
			abilityToUse = t.GetComponent<Ability>();
		}
	}
    
	
	public virtual void beginState() // called when entering the state
	{
		TargetPos = TargetFish.transform.position;
		_huntTimer = HuntTime + HuntPauseTime;
	}
	
	public override void stateUpdate()  // update fn
	{
		if (TargetFish==null || !TargetFish.isAlive()) // invalid target or its dead
		{
			_huntTimer = -1f;	// effectively stop hunting
			_cycleCount = -1;
		}
		TargetPos = TargetFish.transform.position;
	}	
	
	public override void stateFixedUpdate() // fixedupdate fn
	{
		moveVector = GetOptimalDirection(TargetPos - transform.position) * SpeedRatio;
	}	
	
	public override float wantsControl() // used to determine how much this state wants to be active (higher is more urgent)
	{
		if (_cycleCount < 0) { return -1f;}	// no more cycles
		_huntTimer -= Time.deltaTime;
		
		if (_huntTimer > HuntPauseTime)	// then are in the hunting part of the cycle
		{
			readyToFire = true;
			return ControlValueOnTrigger;	// we want to hunt
		}
	
		if (_huntTimer > 0) // we are pausing
		{
			if (readyToFire)
			{
				if (useAbility) { abilityToUse.onRelease(abilityCharge); }
				readyToFire = false;
			}	
			return -1f;
		}
		// if we get here then there are cycles to go and we have finished pausing
		_huntTimer = HuntTime + HuntPauseTime;
		_cycleCount--;
		return -1f;
	}
	
	public override void TakeDamage(float amount,Fish nastyFish) 	// called if we take damage
	{
		if (nastyFish == null) { return;}	// only fish can damage us
		bool valid = false;
		if ( nastyFish is PlayerFish)	
		{
			if (TriggerOnPlayerDamage ) { valid =  true;}	// player damaged us and we respond to that
		}
		else // not playerFish, so NPC
		{
			if (TriggerOnNPCDamage ) { valid =  true;}		// NPC damaged us and we respond to that
		}
		if (valid)	
		{
			_huntTimer = HuntTime + HuntPauseTime;
			_cycleCount = NumberOfCycles-1;
			TargetFish = nastyFish;
		}
	}
	
	
	
}
