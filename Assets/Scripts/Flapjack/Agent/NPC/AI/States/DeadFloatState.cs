﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("AI/DeadState")]

public class DeadFloatState : AIState {
	
	// Dead Behaviour of the fish
	
	public float FloatVelocity = 0.5f;	// how fast it floats up
	
	public float TimeBeforeKill = 10f;		// seconds before killing
	public float ShrinkTime = 1f;			// how long before shrinking to nothing
	public float ControlValueOnTrigger = 20f; // make this high so nothing can revive us!	
	public float WaterLevelOffset = 0.1f;	// to let them float above the water if need be
	
	public float TurnUpsideDownRate = 1f;
	
	private float _timer;
	private Fish _thisFish = null;
	private float _shrinkRate ;
	private Quaternion _targetRotation;
	
	public override void beginState() // called when entering the state
	{
		//Debug.Log("Starting dead state");
		_timer  = TimeBeforeKill + ShrinkTime;
		_shrinkRate = _thisFish.transform.localScale.x / ShrinkTime;
		Debug.Log("Transform  y = " + _thisFish.transform.rotation.eulerAngles.y);
		if (Mathf.Abs(_thisFish.transform.rotation.eulerAngles.y - 180) < 90 )
		{
			_targetRotation =  Quaternion.Euler(180,180,0);
		}
		else
		{
			_targetRotation =  Quaternion.Euler(180,0,0);
		}
		_thisFish.transform.ChangeLayersRecursively("Default");
		
	}
	
	public override void stateUpdate()  // update fn
	{
		//Debug.Log("Dead stateUpdate");
		_timer -= Time.deltaTime;
		if (_timer >0 && _timer < ShrinkTime) // then we are shrinking
		{
			_thisFish.transform.localScale  -= new Vector3(1f,1f,1f) * _shrinkRate * Time.deltaTime;
		}
		if (_timer < 0) 
		{
			_thisFish.Kill();
		}
		// calculate rotation
		_thisFish.transform.rotation = Quaternion.Slerp(_thisFish.transform.rotation, _targetRotation, Time.deltaTime * TurnUpsideDownRate);
		// set velocity
		float v = FloatVelocity;
		if (_thisFish.transform.position.y >( 10+WaterLevelOffset) ) { v = 10+WaterLevelOffset - _thisFish.transform.position.y;}
		
		_thisFish.rigidbody.velocity = new Vector3(0,v,0);
	}	
	
	public override void stateFixedUpdate() // fixedupdate fn
	{

	}	
	
	public override float wantsControl() // used to determine how much this state wants to be active (higher is more urgent)
	{
		if (_thisFish == null)
		{
			_thisFish = Helper.GetAncestor<Fish>(this);
			if (_thisFish == null) { return -1f;}
		}
		//Debug.Log("Checking dead state, Energy = "+_thisFish.Energy);
		if (!_thisFish.isAlive())	// then dead - so act it!
		{
			return ControlValueOnTrigger;	// we want to hunt
		}
		return -1f;
	}	
	
}
