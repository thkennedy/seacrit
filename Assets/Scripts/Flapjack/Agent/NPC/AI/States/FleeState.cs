﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("AI/FleeState")]

public class FleeState : AIState {
	
	// Idle behaviour consists of swimming to random targets
	public float SpeedRatio = 0.2f;	// ratio of total speed the fish uses when just roaming around.	
	public float PredForceTrigger = 1.0f; // will flee when the predator force is greater than this
	public float PredForceEnd = 1.0f; // will stop fleeing when the predator force is less than this
	
	public float ControlValueOnTrigger = 15f;
	private float _fleeTimer = 0f;
	
	[HideInNormalInspector] public Vector3 TargetPos;			// where this fish is heading
    
	public override void stateUpdate()  // update fn
	{
		TargetPos = core._predatorForce + transform.position;
	}	
	
	public override void stateFixedUpdate() // fixedupdate fn
	{
		moveVector = GetOptimalDirection(TargetPos - transform.position) * SpeedRatio;
	}	
	
	public override float wantsControl() // used to determine how much this state wants to be active (higher is more urgent)
	{
		//Debug.Log("pred force = "+core.GetPredatorForce());
		float f =  core.GetPredatorForce().magnitude;
		
		if (core.getCurrentState() == this) // then we are currently fleeing - should we stop?
		{
			if (f < PredForceEnd) { return -1f;}	// yes, stop
			// else {
			return ControlValueOnTrigger;	// keep fleeing
		}
		// otherwise not currently fleeing
		if (f > PredForceTrigger) 
		{ 	
			//Debug.Log("Trigger, f = "+f);
			return ControlValueOnTrigger;
		}
		return -1f;
	}
	
	
	
}
