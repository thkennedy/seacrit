﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("AI/HuntOnDamageChargeState")]

public class HuntOnDamageChargeState : AIState {
	
	// Idle behaviour consists of swimming to random targets
	public float SpeedRatio = 0.2f;	// ratio of total speed the fish uses when just roaming around.	
	
	public float HuntTime = 2f;
	private float _huntTimer;
	
	public float ControlValueOnTrigger = 15f; // how much we want to hunt when damaged
	
	private Fish TargetFish;	// fish we are hunting
	
	[HideInNormalInspector] public Vector3 TargetPos;			// where this fish is heading
    
	
	public virtual void beginState() // called when entering the state
	{
		TargetPos = TargetFish.transform.position;
		_huntTimer = HuntTime;
	}
	
	public override void stateUpdate()  // update fn
	{
		if (TargetFish==null || !TargetFish.isAlive()) // invalid target or its dead
		{
			_huntTimer = -1f;	// effectively stop hunting
		}
		_huntTimer -= Time.deltaTime;
		TargetPos = TargetFish.transform.position;
	}	
	
	public override void stateFixedUpdate() // fixedupdate fn
	{
		moveVector = GetOptimalDirection(TargetPos - transform.position) * SpeedRatio;
	}	
	
	public override float wantsControl() // used to determine how much this state wants to be active (higher is more urgent)
	{
		if (_huntTimer > 0f)
		{
			return ControlValueOnTrigger;	// we want to hunt
		}
		return -1f;
	}
	
	public override void TakeDamage(float amount,Fish nastyFish) 	// called if we take damage
	{
		Debug.Log("Taking damage!");
		_huntTimer = HuntTime;
		TargetFish = nastyFish;
	}
	
	
	
}
