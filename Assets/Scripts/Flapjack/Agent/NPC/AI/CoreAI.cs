using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("AI/CoreAI")]

public class CoreAI : MonoBehaviour {


	// private vars
	private GameObject _playerFish;		// player
	private Fish _thisFish;		// our fish
	private CoreAIGlobalManager _aiManager;
	private Movement _move;				// movement structure that's used to move the fish.
	
	public bool drawStateName = false;		// if true will display the state as text
	public AIState _currentState;
	public AIState[] _knownStates;	// list of available states
	
	private bool PredatorForceValid = false;
	private bool RepellerForceValid = false;
	
	public float MaxPredDist = 1.0f;		// used to ignore far away predators and avoid calculations
	
	// cached values
	[HideInNormalInspector] public Vector3 _predatorForce;		// force from predators - only updated if avoidPredators is true;
	[HideInNormalInspector] public Vector3 _repelForce;		// force from repellers
	
	// velocity smoothed to avoid jerky updates and unstable movement
	[HideInNormalInspector] public  Vector3 _velocitySmoothed;	
	public float velocitySmoothAmount = 0.8f;
	
	// --------------------- GENERAL UNITY OVERRIDES -------------------------------------------------------
	
	void Start()
	{
		
		// find the players fish
		PlayerFish[] pflist = FindObjectsOfType(typeof(PlayerFish)) as PlayerFish[];
		if (pflist.Length !=1)
		{
			Debug.LogError ("Incorrect number of Playerfish Objects in the scene = "+pflist.Length);
		}
		else
		{
			_playerFish = pflist[0].gameObject;
		}

		// find AIManager
		_aiManager = _playerFish.GetComponentInChildren<CoreAIGlobalManager>();
		if (_aiManager == null)
		{
			Debug.LogError ("No AIManager found for AI fish : "+gameObject.tag);
		}

		// find Movement class
		_move = gameObject.GetComponent<Movement>();
		if (_move == null)
		{
			Debug.LogError ("No Movement found for AI fish : "+gameObject.tag);
		}

		// find Movement class
		_thisFish = gameObject.GetComponent<Fish>();
		if (_thisFish == null)
		{
			Debug.LogError ("No Fish found for AI fish : "+gameObject.tag);
		}

		if (_thisFish.CanEat)
		{
			gameObject.transform.ChangeLayersRecursively("FishPredator");
		}
		else
		{
			gameObject.transform.ChangeLayersRecursively("FishPrey");
		}
		
		// Now to find a list of all attached AIStates
		_knownStates = gameObject.GetComponentsInChildren<AIState>();
		foreach (AIState state in _knownStates)
		{
			state.core = this;
		}
   
	}
	
	 public void Update() // This is the main class for movement and behaviour
	 {	
	 	 // invalidate values for the predator and repeller force
	 	 PredatorForceValid = false;
	 	 RepellerForceValid = false;
	 	 
	 	 // first find out if we need to change states
	 	 
	 	 AIState best = null;
	 	 float mostUrgent = -9999f;
	 	 
	 	 foreach (AIState state in _knownStates)
	 	 {
	 	 	 float x = state.wantsControl();
	 	 	 if (x > mostUrgent)
	 	 	 {
	 	 	 	 mostUrgent = x;
	 	 	 	 best = state;
	 	 	 }
	 	 }
	 	 if (best != null && best != _currentState)	// then we need to swap states
	 	 {
	 	 	 gotoState(best);
	 	 }
	 	 
	 	 if (_currentState ==null) { return;}
	 	
	 	 _currentState.anim = _thisFish.transform.GetComponentInChildren<Animator>();
	 	 _currentState.stateUpdate();
	 	 
	}
	
	void FixedUpdate()
	{
		if (_currentState ==null) { return;}
		
		_currentState.stateFixedUpdate();
		
		_velocitySmoothed = velocitySmoothAmount * _velocitySmoothed + (1.0f  - velocitySmoothAmount) * transform.rigidbody.velocity;
		_move.InputDirection = _currentState.moveVector;

	}
	
	// ------------------------------- STATE FUNCTIONS ---------------------------------------------

	public AIState getCurrentState() { return _currentState;}
	
	public string getCurrentStateName() 
	{
		if (	_currentState == null) { return "nullState";}
		return _currentState.stateName;
	}
	
	public void gotoState(string stateName)
	{
		foreach (AIState state in _knownStates)
		{
			if (state.stateName == stateName) 
			{ 
				gotoState(state);
				return;
			}
		}
		if (_currentState!=null) { _currentState.endState();}	// end the current state - chance to finish animations et
		_currentState = null;	// couldn't find a state!
		Debug.LogError("Couldn't find valid state named "+stateName);
	}
	
	public void gotoState(AIState state)
	{
		if (_currentState!=null) { _currentState.endState();}	// end the current state - chance to finish animations et
		_currentState = state;
		_currentState.core = this;
		state.beginState();
	}
	
	// ------------------------------ FORCE FUNCTIONS ------------------------------------------------
	
	public Vector3 GetRepellerForce() // find the best way to move to avoid geometry and predators. 
	{	
		if (RepellerForceValid) { return _repelForce;} // cached value
		// first find all objects surrounding us
		Vector3 sumvec = new Vector3();
		
		for (int i=0; i< _aiManager.NumRepellers; i++) {	// calculate the average vector away from them, weighted. 
			sumvec += _aiManager.Repellers[i].getForce(transform.position, _velocitySmoothed);
		}
		
		sumvec.z = 0;	// to keep the force in the plane
		
		// cache values
		_repelForce = sumvec;
		RepellerForceValid = true;
		return sumvec;
	}
	
	public Vector3 GetPredatorForce()		// sums force over each possible predator
	{
		if (PredatorForceValid) { return _predatorForce;} // cached value
		Vector3 sumvec = new Vector3();
		for (int i=0; i< _aiManager.NumEaters; i++) {	// calculate the average vector away from them, weighted. 
			sumvec += GetSinglePredatorForce(_aiManager.Eaters[i].gameObject);
		}
		sumvec.z = 0;	// to keep the foce in the plane
		
		// cache values
		_predatorForce = sumvec;
		PredatorForceValid = true;
		
		return sumvec;
	}
	
	private Vector3 GetSinglePredatorForce(GameObject p)	// finds the force due to predator p
	{
		if (_currentState ==null) { return Vector3.zero;};
		if (p == gameObject) { return Vector3.zero;}	// don't apply force to yourself
		Vector3 f = (transform.position - p.transform.position);		// direction to avoid other
		if (f.magnitude > MaxPredDist) { return Vector3.zero;}	// ignore it
		f = f / (0.1f + f.sqrMagnitude) *20f;
		Rigidbody rb = p.GetComponentInChildren<Rigidbody> ();
		if (rb != null)
		{
			f = f * ( rb.velocity.magnitude + 2.0f);
		}
		Fish otherFish = p.GetComponentInChildren<Fish> ();
		if (otherFish != null)	// then the predator is a fish (should always be true?)
		{	
			f = f * GetThreat(otherFish);
		}
		
		return f;
	}
	
	private Vector3 getAvoidDirection(Vector3 n, Vector3 desired) // returns a direction perpendicular to n which is closest to our desired direction
	{ 
		Vector3 d1 = new Vector3 (n.y, - n.x, 0);
		Vector3 d2 = new Vector3 (- n.y, n.x, 0);
		d1.Normalize();
		d2.Normalize();
		if (Vector3.Dot (d1, desired) > Vector2.Dot (d2, desired)) {
			return d1;
		}
		// else
		return d2;
	}
	
	// --------------------------------------------- Interrupts -----------------------------------------------
	
	public void TakeDamage(float amount,Fish nastyFish)
	{
		foreach (AIState state in _knownStates)
		{
			state.TakeDamage(amount, nastyFish);
		}
		return;	
	}
	
	// ---------------------------------------------- MISC -----------------------------------------------------
	
	private float GetThreat(Fish other) // returns a scaling factor depending on how much of a threat this fish is to us
	{
		if (!other.EatCheck(_thisFish))	// for now just 1 or 0 -TODO change this to float to reflect chance of being damaged.
		{
			return 0f;
		}
		return 1f;
	}
	
	void OnGUI()
    {
    	    if (drawStateName)
    	    {
		    Vector3 objPos = Camera.main.WorldToScreenPoint(transform.position);
		    Rect r = new Rect(objPos.x, Camera.main.pixelHeight - objPos.y, 100, 50);
		    GUI.Label(r,getCurrentStateName());
	    }
        
    }
	
		
			
		
	
	
}
