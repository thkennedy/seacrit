﻿using UnityEngine;
using System.Collections;

public class Ability : MonoBehaviour {

	[HideInNormalInspector] public Fish _thisFish;			// fish we are attached to 
	[HideInNormalInspector] public PlayerFish _playerFish;		// PlayerFish we are attached to 
	[HideInNormalInspector] public Movement move;
	[HideInNormalInspector] public AbilityManager _AM;		// ability manager that is controlling us
	[HideInNormalInspector] public Animator anim; 
	public bool usesLMB = false;
	public bool usesRMB = false;	
	public float validChargeMin = 0.0f;		// onRelease only gets called if the charge is between these values (inclusive)
	public float validChargeMax = 1.0f;
	
	void Start()
	{
		_thisFish 	 = Helper.GetAncestor<Fish>(this);
		_playerFish 	 = Helper.GetAncestor<PlayerFish>(this);
		_AM			 = Helper.GetAncestor<AbilityManager>(this);		
		if (_thisFish !=null) { move = _thisFish.transform.GetComponent<Movement>(); }
		anim = transform.parent.GetComponentInChildren<Animator>();
	}
	
	public virtual void StartCharge() {}	// This is called when the mouse button goes down
	public virtual void updateIdle() {} // called when no mouse activity
	public virtual void updateCharge(float charge) {}	// this is called while the mouse button is held down. c represents the amount of charge (0 to 1)
	public virtual void onRelease(float charge)	{}	// called when the mouse button is released. c is the amount of charge
	
}
