﻿

using UnityEngine;
using System.Collections;
using System.Reflection;
using System;

public class AbilityManager : MonoBehaviour
{
	//references
	protected Movement move;
	protected Fish _thisFish;
	protected PlayerFish _playerFish;
	
	// animator (if present)
	public Animator anim; 
	
	public float[] chargeRateEnergy = {1.0f,1.0f};	//charge rate (rate of energy consumed per second)
	private float[] ChargeAbs = {0.0f,0.0f};		//current charge level = amount of energy consumed
	private float[] ChargeRel = {0.0f,0.0f};		//current charge level - float from 0 to 1

	//cost and charging
	public float[] EnergyCostInstant = {1f,1f};
	public float[] EneryCostFullyCharged = {2f,2f};
	
	public float[] CoolDownTimeInstant = {1f,1f};
	public float[] CoolDownTimeFullCharge = {6f,6f};	// NOTE this starts counting from triggering of ability
	
	public float InstantAbilityTimeWindow = 0.2f;
	private float[] _instantTimer = {0f,0f};
	private float[] _cooldownTimer = {0f,0f};	// used for cooldown
	
	public bool isActive = true;
	
	// state info and mousebuttons
	private bool[] lastMouseHold = {false,false};
	private bool[] lastMouseUp = {false,false};
	
	private Ability[] abilities;	// list of abilities
	private bool hasLMBAbility = false;
	private bool hasRMBAbility = false;
	
	void Start()
	{
		Debug.Log("EnergyCostInstant rel init ="+EnergyCostInstant.Length);
		Initialise();
		if (_thisFish == null) 
		{
			isActive =false;		// deactivate if not attached to a fish so we can easily edit prefabs in scene
		}
		updateAbilityList();
	}
	
	public void Initialise()
	{
		_thisFish 	 = Helper.GetFish(this);
		_playerFish 	 = Helper.GetPlayerFish(this);
		if (_thisFish !=null) { move = _thisFish.transform.GetComponent<Movement>(); }
		anim = transform.parent.GetComponentInChildren<Animator>();
	}
	
	public void updateAbilityList()
	{
		abilities = GetComponentsInChildren<Ability>();
		hasLMBAbility = false;
		hasRMBAbility = false;
		foreach(Ability a in abilities)
		{
			if (a.usesLMB) { hasLMBAbility = true;}
			if (a.usesRMB) { hasRMBAbility = true;}
		}
		//Debug.Log("Number of abilities : "+abilities.Length);
	}
	
	public static void UpdateAllAbilityLists(Component c)
	{
		AbilityManager[] alist = c.transform.GetComponentsInChildren<AbilityManager>();
		foreach(AbilityManager a in alist)
		{       
			a.updateAbilityList();
		}
	}
	
	public void checkCharge(int mouseButton)
	{
		bool newMouseHold = Input.GetMouseButtonDown(mouseButton) || Input.GetMouseButton(mouseButton);
		bool newMouseUp = Input.GetMouseButtonUp(mouseButton);
		//Debug.Log("newMouseHold = "+ newMouseHold+"  newMouseUp = "+ newMouseUp+"  lastMouseHold = "+lastMouseHold+"  lastMouseUp = "+lastMouseUp);
		if (_cooldownTimer[mouseButton] >0f) // still in cooldown
		{
			lastMouseHold[mouseButton] = false;
			lastMouseUp[mouseButton] = false;		// pretend we didn't press a mouse button
			foreach (Ability a in abilities)
			{
				if (a.usesLMB && mouseButton ==0 || a.usesRMB && mouseButton ==1)
				{
					a.updateIdle();
				}
			}
			return;
		}
		if(newMouseHold)// we are trying to charge
		{
			if(! lastMouseHold[mouseButton])// then this is the first frame with the mouse button down
			{
				if( _thisFish.Energy > EnergyCostInstant[mouseButton])
				{
					_thisFish.Energy -= EnergyCostInstant[mouseButton];	// take away the energy needed
					_instantTimer[mouseButton] = InstantAbilityTimeWindow;
					foreach (Ability a in abilities)
					{
						if (a.usesLMB && mouseButton ==0 || a.usesRMB && mouseButton ==1)
						{	
							a.StartCharge();
						}
					}
				}
			}
			else	// otherwise this is not the first charging frame
			{
				float EnergyCost = chargeRateEnergy[mouseButton] * Time.deltaTime;
				if (_thisFish.Energy-EnergyCost <0f ) // don't have enough energy to charge fully on this frame
				{
					EnergyCost = _thisFish.Energy;
				}
				if( ChargeAbs[mouseButton] <EneryCostFullyCharged[mouseButton])
				{
					_instantTimer[mouseButton] -= Time.deltaTime;
					if (_instantTimer[mouseButton] < 0f) // then the delay has expired, start charging
					{
						if ( (ChargeAbs[mouseButton] +EnergyCost) > EneryCostFullyCharged[mouseButton] ) // then we will overcharge
						{
							EnergyCost = EneryCostFullyCharged[mouseButton] - ChargeAbs[mouseButton];
							_thisFish.Energy -= EnergyCost;
							ChargeAbs[mouseButton]	= EneryCostFullyCharged[mouseButton];	// do it this way so always the exact value we need
							ChargeRel[mouseButton] =1.0f;
						}
						else
						{
							_thisFish.Energy -= EnergyCost;
							ChargeAbs[mouseButton] 	 += EnergyCost;
							ChargeRel[mouseButton] = ChargeAbs[mouseButton] / EneryCostFullyCharged[mouseButton];
						}
						foreach (Ability a in abilities)
						{
							if (a.usesLMB && mouseButton ==0 || a.usesRMB && mouseButton ==1)
							{
								a.updateCharge(ChargeRel[mouseButton]);
							}
						}
					}
				}
			}
		}
		if(newMouseUp) // then the mouse was released on this frame
		{
			
			foreach (Ability a in abilities)
			{
				if (a.usesLMB && mouseButton ==0 || a.usesRMB && mouseButton ==1)
				{
					//Debug.Log("Checking on release!");
					if (ChargeRel[mouseButton] >= a.validChargeMin &&  ChargeRel[mouseButton] <= a.validChargeMax)
					{
						a.onRelease(ChargeRel[mouseButton]);
						_cooldownTimer[mouseButton] = CoolDownTimeInstant[mouseButton] + CoolDownTimeFullCharge[mouseButton] * ChargeRel[mouseButton];
					}
				}
			}
			
			ChargeAbs[mouseButton] = 0.0f;	// do it this way so always the exact value we need
			ChargeRel[mouseButton] = 0.0f;
		}
		if (!newMouseUp && !newMouseHold) // no button pressed
		{
			
			foreach (Ability a in abilities)
			{
				if (a.usesLMB && mouseButton ==0 || a.usesRMB && mouseButton ==1)
				{
					a.updateIdle();
				}
			}
		}
			
		lastMouseHold[mouseButton] = newMouseHold;
		lastMouseUp[mouseButton] = newMouseUp;
		return;
	}

	void Update()
	{
		if (!isActive) { return;}
		//player input for abilities
		if(_thisFish.isPlayer)
		{
			if (hasLMBAbility)
			{
				_cooldownTimer[0] -= Time.deltaTime;
				//Debug.Log("charge rel a ="+ChargeRel.Length);
				checkCharge(0); 
				//Debug.Log("charge rel b ="+ChargeRel.Length);
				_playerFish.setLeftDialAmount(ChargeRel[0]);
				_playerFish.setLeftDialActive(_cooldownTimer[0] <=0.0f);
			}
			if (hasRMBAbility)
			{
				_cooldownTimer[1] -= Time.deltaTime;
				checkCharge(1); 
				_playerFish.setRightDialAmount(ChargeRel[1]);
				_playerFish.setRightDialActive(_cooldownTimer[1] <=0.0f);
				//Debug.Log("rmb cooldown = "+_cooldownTimer[1]+"  dial active = "+ (_cooldownTimer[1] <=0.0f) +" go = "+gameObject.transform.root.name );
			}
		}
		
	}

}