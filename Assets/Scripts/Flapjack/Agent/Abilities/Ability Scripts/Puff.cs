﻿using UnityEngine;
using System.Collections;

public class Puff : Ability {
	
	public Transform puffBone;					//the puff bones that puffs the fish out
	public Transform rootObject;				//the root bone that gives global scaling increase
	//public GameObject hitSphere;				//The game object that turns on and hits and repells enemies while puffing
	public float maxPuff;						//max puff scale value
	public float minPuff;						//minumum scale values for puff
	public float puffRate;						//how quickly the fish puff up
	public float baseScaleChange;				//how much the root bone scales with puff
	public float deflateRate;					//how quickly the fish deflates per frame
	public float puffCharge;					//% of puff the fish has towards fully puffed
	public float puffPull;						//how much the fish darts forward as they suck in water
	public ParticleSystem inhale;				//emits bubbles when puffing

	public void start(){
		puffBone.transform.localScale = Vector3.one * minPuff;	//set the puff to normal unpuffed values
		inhale.emissionRate = 0f;
	}

	public void SetBaseScale(){
		puffCharge = (puffBone.transform.localScale.x - minPuff)/ (maxPuff - minPuff);	//The ammount of puff on the fish from 0 to 1
		rootObject.transform.localScale = Vector3.one * ( 1 + baseScaleChange * puffCharge);	//gives base scale the same % increase as the puffbone
	}
	
	public override void onRelease(float charge)
	{
		//Debug.Log("Used Ability");
		inhale.emissionRate = 0f;
	}
	
	public override void updateIdle()
	{
		customUpdateIdle();	
	}
	public override void updateCharge(float charge)
	{
		customUpdate();
		puffBone.transform.localScale = Vector3.one * (Mathf.Clamp(puffBone.transform.localScale.x + puffRate * Time.deltaTime * 30, minPuff, maxPuff));
		SetBaseScale();
		if (charge >= 1) inhale.emissionRate = 0f;
			else inhale.emissionRate = 20f;
		_thisFish.rigidbody.AddForce (_thisFish.transform.right * 1000f * puffPull * Time.deltaTime * 30);// instant impulse
	} 
	public void customUpdate()
	{


	}
	public void customUpdateIdle()
	{
		if (puffBone.transform.localScale.x > minPuff){		//if puffed
			puffBone.transform.localScale = Vector3.one * (puffBone.transform.localScale.x - deflateRate * Time.deltaTime * 30);	//deflates the puff
			SetBaseScale();
		}
	}
}