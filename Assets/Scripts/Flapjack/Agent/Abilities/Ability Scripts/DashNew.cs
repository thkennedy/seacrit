﻿using UnityEngine;
using System.Collections;

public class DashNew : Ability {

	public float damage;		//how much damage the dash delivers to other fish on impact
	public float burstDash;
	public bool doesDamage = false;		//whether or not the dash deals damaage
	public bool passThrough = false;	//whether or  not the fish can pass through other fish while dashing
	public float burstDashDuration;	//how long the burst dash lasts until brakes are applied or collision is reenabeled
	public float burstDashDurationCounter;	//The counter for burst dash duration
	public float extendedDashDuration;	//how  long the passive bonus boost lasts on dash
	public float extendedDashDurationCounter;	//used as the actual counter
	public bool isDashing = false;
	public bool isBurstDashing = false;
	public float dashSpeedBonus;
	public float chargeModifier = 1f;
	float chargeAmmount;
	public float knockBack;
	public GameObject impactEffect;
	public GameObject dashCone;
	public bool rapidDash = false;
	public float reload;
	public float reloadTime;
	public Color dashColor;
	public Color targetColor;
	public float colorAdjustModifier;
	public Vector3 knockBackDirection;
	public Vector3 previousInputDirection;
	public float rapidDashThreshold = 1f;
	public Vector3 newDirection;
	public bool firstFrame;

	public float brakeModifier = 0f;


	public void ability(){
		if (!rapidDash){
			newDirection = move.InputDirection.normalized;
		}
		else _thisFish.transform.localRotation = Quaternion.LookRotation (newDirection.normalized);
		isDashing = true;
		isBurstDashing = true;
		knockBackDirection = move.InputDirection.normalized;
		burstDashDurationCounter = burstDashDuration;	//set burst duration
		extendedDashDurationCounter = extendedDashDuration;	//set duration bonus based on charge
		if (passThrough) {
			Helper.ChangeLayersRecursively(_thisFish.transform, "PassThroughFish");	//sets to no collide if  ability allows
			gameObject.layer = LayerMask.NameToLayer("FishPlayer");	//Allows dash to hit fish
		}
			else gameObject.layer = LayerMask.NameToLayer("FishPlayer");	//Allows dash to hit fish
		_thisFish.rigidbody.AddForce (chargeAmmount * newDirection.normalized * 10f * burstDash, ForceMode.Impulse);// instant impulse
		move.AbilityForceMod = 1f + dashSpeedBonus;	//gives the passive speed bonus of dash
		newDirection = Vector3.zero;
	}
	
	public override void onRelease(float charge)
	{
		chargeAmmount = 1f + (charge * chargeModifier);
		if (!rapidDash) ability ();
			else if (passThrough) gameObject.layer = LayerMask.NameToLayer("Default");	//ensures dash collision is off on rapid dash
		reload = 0f;
		firstFrame = true;

		//Debug.Log("Used Ability");
	}
	
	public override void updateIdle()
	{
		customUpdate();	
	}

	public virtual void OnTriggerEnter (Collider hit) {
		Debug.Log("Hit Fish");
		Fish otherFish = Helper.GetFish(hit);
			if (otherFish != null && isBurstDashing)	// we hit another fish
			{
				otherFish.TakeDamage(damage, _thisFish);
			otherFish.transform.rigidbody.AddForce(knockBackDirection * knockBack * 10f, ForceMode.Impulse);
				GlobalData.SoundMan.playAbilitySound(otherFish.audio,0);
				GameObject newObject = (GameObject) Instantiate (impactEffect, transform.position, Quaternion.identity);
			}
			//else // we hit some non-fish
	}

	public override void updateCharge(float charge)
	{
		newDirection = move.InputDirection - previousInputDirection;
		if ((rapidDash) && (reload > reloadTime) && !firstFrame && (Mathf.Abs(newDirection.magnitude / Time.deltaTime) > rapidDashThreshold)) {
			burstDashDurationCounter = burstDashDuration;	//set burst duration
			extendedDashDurationCounter = extendedDashDuration + chargeModifier * charge;	//set duration
			ability();
			reload -= reloadTime;
		}
		previousInputDirection = move.InputDirection;
		firstFrame = false;
		customUpdate();
	}
	
	public void customUpdate()
	{
		if(reload<reloadTime) reload += Time.deltaTime;
		if (extendedDashDurationCounter > 0) extendedDashDurationCounter -= Time.deltaTime;	//counts down the speed bonus
		else if (isDashing){
			move.AbilityForceMod = 1f;	//sets speed to default when expired
			isDashing = false;
		}
		if (burstDashDurationCounter > 0) {
			burstDashDurationCounter -= Time.deltaTime;	//counts down the burst duration
			targetColor = dashColor;
		}
		else if (isBurstDashing){
			_thisFish.rigidbody.AddForce (-_thisFish.rigidbody.velocity * brakeModifier, ForceMode.Impulse);	//brake after impulse dash duration
			_thisFish.gameObject.layer = 13;	//sets collision back to playerFish
			Helper.ChangeLayersRecursively(_thisFish.transform, "FishPlayer");
			targetColor = Color.black;
			gameObject.layer = LayerMask.NameToLayer("Default");	//moves dash to collision layer that can't hit fish
			isBurstDashing = false;
		}
		dashCone.renderer.material.color = dashCone.renderer.material.color + (targetColor - dashCone.renderer.material.color) / colorAdjustModifier;
	}

	public void StartCharge()
	{
		previousInputDirection = move.InputDirection;
		extendedDashDurationCounter = extendedDashDuration;
	}
}