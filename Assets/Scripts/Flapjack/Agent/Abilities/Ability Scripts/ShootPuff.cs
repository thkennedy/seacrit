using UnityEngine;
using System.Collections;

public class ShootPuff : ShootProjectile {

	public Transform puffBone;
	public float minPuff = .2f;
	public float maxPuff = 1f;
	public float puffProjectileCost;
	

	public override void onRelease(float charge)
	{
		base.onRelease(charge);
		
		if (puffBone == null ) { return;}
		
		if (puffBone.transform.localScale.x > (minPuff + puffProjectileCost)){
			puffBone.transform.localScale -= Vector3.one * puffProjectileCost;
		}
		//Debug.Log("FIYA");
	}

	public override void updateIdle()
	{
		base.updateIdle();
	}
	public override void updateCharge(float charge)
	{
		base.updateCharge(charge);
	}
	
	public void customUpdate()
	{
		base.customUpdate();

	}
}