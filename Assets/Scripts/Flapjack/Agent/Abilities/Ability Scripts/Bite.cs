﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Abilities/Bite")]

public class Bite : Ability {
	
	public float biteDelay = 0.4f;		// delay before actual damage occurs - to help sync with animation
	
	public float BiteForce = 0.5f;
	public float Damage = 0.2f;
	public float DamageRadius = 0.2f;
	public float BiteRepel = 0.04f;
	
	private float _biteActionTimer=0f;
	private bool biting = false;

	public override void onRelease(float charge)		// called when the mouse button is released. c is the amount of charge
	{
		Debug.Log("Bite on release!");
		_biteActionTimer = biteDelay;
		biting = true;
		_thisFish.rigidbody.AddForce(move.InputDirection.normalized *1000 * BiteForce);// instant impulse
		_thisFish.isBiting = true;	// allows animation on fish
	}
	
	void Update()
	{
		if (!biting) { return;}
		
		_biteActionTimer -= Time.deltaTime;
		if (_biteActionTimer < 0.0f) 
		{
			// create an array to keep track of what objects have had damage applied.
			// We need this because damage will be called for each collider, and some objects will have multiple colliders
			Collider[] colliders = Physics.OverlapSphere(transform.position,DamageRadius);	// get list of colliders
			int[] damaged = new int[colliders.Length];
					
			for (int i=0; i < colliders.Length; i++)
			{
				Collider c = colliders[i];
				Debug.DrawLine(transform.position,c.transform.position, Color.red);
				bool valid = true;
				Fish otherFish = Helper.GetFish(c.transform);
				valid = otherFish != null;
				if (valid)
				{
					damaged[i] = otherFish.GetInstanceID();	// unique id
				}
				// now check to see if we've already damaged it
				int j=0;
				while (j <i && valid)
				{
					if (damaged[j++] == damaged[i]) {  valid =false;}
				}
				if(valid && _thisFish != otherFish)
				{
					Debug.Log("applying "+Damage+" to "+otherFish.gameObject.name);
					otherFish.TakeDamage(Damage, _thisFish);	// automatically checks size of fish and prevents going below min size
					otherFish.rigidbody.AddForce((otherFish.transform.position - _thisFish.transform.position).normalized * 1000f * BiteRepel );// instant impulse
					Instantiate(GlobalData.SpriteHit , otherFish.transform.position + new Vector3(0f,0f,-0.5f), Quaternion.identity );
				}
			}
			// find an audiosource 
			GlobalData.SoundMan.playRandomBite(_thisFish.audio);
			biting = false;
			_thisFish.isBiting = false;	
		}
	}
	
}
