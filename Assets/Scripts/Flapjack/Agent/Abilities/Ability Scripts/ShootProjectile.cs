using UnityEngine;
using System.Collections;

[AddComponentMenu("Abilities/ShootProjectile")]

public class ShootProjectile : Ability {
	//GameObjects
	public GameObject spine;					//The projectile fired (some could  be explosive at a later date)
	//public GameObject impactEffect;			//The effect called at the point of impact
	//public Transform firePoint;				//from where spines are fired from (usually fish's mouth)

	//Projectile Properties to instill on creation
	public float damage;						//Base damage of the spines launched
	public float speed;							//Starting speed of the projectile
	public float speedVariance = 0f;					//how much speed can vary per shot
	public float inheritVelocity = 0f;			//how  much of the firing fishes velocity bullets inherit
	public float lifetime;						//How long the projectile exists before being destroyed
	public float projectileSize = 1f;			//How large the projectile is

	//Fire params
	//public float baseSpines;					//lowest number of spines to fire on a tap click
	//public float chargeSpines;					//How many spines a charge would fire, % of this number is based on charge (must always fire at least 1)
	//public float fullchargeSpines;				//How many spines are fired at full charge
	//public float fireTime;						//Duration of time between each projectile being fired off
	//public float arc;							//how wide of an arc the projectiles are fired out in front
	//public float arcIncriment;					//how quickly each projectile steps pie incriment
	public float accuracy;						//base ammount of how much spines wavers from 100% accuracy
	public float accuracyCurrent;				//innaccuracy caused by rapid fire
	public float accuracyDegredation;			//how much each spine fired reduces accuracy due to rapid fire
	public float accuracyRecovery;				//how much accuracy is recovered each frame
	public float accuracydegredationMax;		//The maximum ammount of potential degraded accuracy possible
	public int burst = 1;						//how many projectiles are fired per burst


	public bool automatic;					//Whether or  not holding the mouse button down fires off spines in rapid succession
	public float reloadTime;						//the time between firing  off spines
	private float reload = 0f;

	//Affects on the player when fired
	public float stopMomentum;					//how much the players current forward momentum is stopped on fire;
	public float kickBack; 						//How much impulse knockback is applied to the player fish when they fire the projectile
	public float kickBackChargeModifier;		//How much extra kickback is added at a full charge;
	public float knockBack;

	public void fireSpine(){
		for (int rounds = burst; rounds > 0; rounds --) {
			float randomAngleOffset = Random.Range(-.5f * (accuracyCurrent + accuracy), .5f * (accuracyCurrent + accuracy)); //sets accuracy of current spine
			//GameObject newObject = Instantiate (spine, launchPoint, Quaternion.Euler (0f, 0f, launchPoint.eulerAngles.z)) as GameObject;
			Transform spawnPoint = transform.Find("SpawnLocation");
			Vector3 dir =  move.InputDirection.normalized;		// direction our projectile will travel
			//Debug.Log("direction = "+dir);
			Quaternion rot = Quaternion.FromToRotation(Vector3.right,dir);
			GameObject newObject = (GameObject) Instantiate (spine, spawnPoint.position, rot); //creates projectile
			newObject.transform.Rotate (Vector3.forward * randomAngleOffset);
			//Debug.Log("Projectile = "+newObject);
			Projectile p = newObject.GetComponentInChildren<Projectile>();
			p.firingFish = _thisFish;
			p.rigidbody.velocity = _thisFish.rigidbody.velocity*inheritVelocity + p.transform.right * (speed + Random.Range(-speedVariance,speedVariance));
			p.damage = damage;
			p.transform.localScale *= projectileSize;
			p.knockBack = knockBack;
		}
		if (accuracyCurrent < accuracydegredationMax) accuracyCurrent += accuracyDegredation;
		GlobalData.SoundMan.playAbilitySound(_thisFish.audio,1);
	}

	public override void onRelease(float charge)
	{
		fireSpine();
		_thisFish.rigidbody.AddForce (-_thisFish.rigidbody.velocity * stopMomentum, ForceMode.Impulse);  //% of fish momentum stopped on fire
		_thisFish.rigidbody.AddForce (-move.InputDirection.normalized * (kickBack + (charge * kickBackChargeModifier* kickBack)), ForceMode.Impulse);	//kickback
		//Debug.Log("FIYA");
	}

	public override void updateIdle()
	{
		customUpdate();	
	}
	public override void updateCharge(float charge)
	{
		if (automatic){
			reload += Time.deltaTime;
			if (reload > reloadTime) {
				fireSpine();
				_thisFish.rigidbody.AddForce (-move.InputDirection.normalized * kickBack, ForceMode.Impulse);
				reload = 0f;
			}
		}
		//customUpdate();
	}
	
	public void customUpdate()
	{
		if (accuracyCurrent > 0) accuracyCurrent = Mathf.Clamp(accuracyCurrent - accuracyRecovery, 0, accuracydegredationMax);

	}

	/*
public void FireProjectile (){
	currentBurstCount += 1;
	Vector3 origin;
	origin = firingPoint.position;
	float randomAngleOffset = Random.Range(-.5f * currentAccuracy, .5f * currentAccuracy);
	firingPoint.rotation = Quaternion.Euler(0f,0f, firingPoint.eulerAngles.z + randomAngleOffset);
	GameObject newObject = Instantiate (firedProjectile, origin, Quaternion.Euler (0f, 0f, player.eulerAngles.z)) as GameObject;
	Vector3 currentVelocity = player.rigidbody.velocity;
	newObject.GetComponent<BulletProjectile>().damage = damage;
	if (parentToPlayer == true) newObject.transform.parent = player;	//Translates the global representation of the rigidbody's velocity into a local coordinate representation.
	Vector3 currentVelocityLocal = player.transform.InverseTransformDirection(currentVelocity); 
	newObject.rigidbody.AddForce (firingPoint.right * velocity, ForceMode.Impulse);
	newObject.rigidbody.AddForce (firingPoint.right * xVelocity, ForceMode.Impulse);
	//float playerVelocity = player.GetComponent("currentVelocityLocal")();
	newObject.rigidbody.AddForce (planet.transform.right * currentVelocityLocal.x * playerVelocityModifier, ForceMode.Impulse);
	firingPoint.rotation = Quaternion.Euler(0f,0f, firingPoint.eulerAngles.z - randomAngleOffset);
	currentAccuracy = Mathf.Clamp (currentAccuracy += degradeAccuracy, maxAccuracy, minAccuracy);
	
	//Sound
	if (currentBurstCount % audioPlayRate == 0) audio.PlayOneShot(fireNoise);
}

public override void Fire (){
	base.Fire ();
	wantsToFire = true;
	if ((firingBurst == false) && (refireDelayTime >= refireDelay)){
		firingBurst = true;
		burstFireDelayTime = 0;
		FireProjectile ();
	}
}

public override void OnUpdate ()
{
	base.OnUpdate ();
	
}

public override void FireCease (){
	base.FireCease();
	wantsToFire = false;
}

public override void OnFixedUpdate (){
	base.OnFixedUpdate ();
	if (firingBurst == false){
		currentAccuracy = Mathf.Clamp (currentAccuracy -= recoverAccuracy, maxAccuracy, minAccuracy);
		if (refireDelayTime < refireDelay) refireDelayTime ++;
	}
	if (firingBurst == true){
		burstFireDelayTime ++;
		if (burstFireDelayTime == burstFireDelay){
			FireProjectile();
			burstFireDelayTime = 0;
		}
	}
	if (currentBurstCount == burstCount) {
		firingBurst = false;
		currentBurstCount = 0;
		refireDelayTime = 0;
	}
	if (wantsToFire == true) Fire();
}
	 */
}
