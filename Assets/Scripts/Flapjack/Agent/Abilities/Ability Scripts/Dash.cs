﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Abilities/Dash")]

public class Dash : Ability {

	public float Force = 2f;
	public float SpeedMod = 2f;
	public float SpeedupTime = 4f;
	private bool isDashing = false;
	
	public override void onRelease(float charge)		// called when the mouse button is released. c is the amount of charge
	{
		//Debug.Log("Dash on release");
		_thisFish.rigidbody.AddForce (move.InputDirection.normalized * 1000f *Force);// instant impulse
		StartCoroutine(IDash());
		//Debug.Log("anim = "+anim);
	}

	public IEnumerator IDash()
	{
		isDashing = true; 
		move.LockInputMagnitude = true;
		move.AbilityForceMod *= SpeedMod;
		yield return new WaitForSeconds(SpeedupTime);
		move.AbilityForceMod /= SpeedMod;
		move.LockInputMagnitude = false;
		isDashing = false;
	}
	
	public override void updateIdle() {updateAnimSettings();} // called when no mouse activity
	public override void updateCharge(float charge) {updateAnimSettings();}	// this is called while the mouse button is held down. c represents the amount of charge (0 to 1)
	
	public void updateAnimSettings()	// gives us a chance to do any per-frame operations like update animation vars
	{
		if (anim!=null)
		{
			//Debug.Log("isDashing = "+isDashing);
			anim.SetBool("isDashing",isDashing);
		}
	}
}
