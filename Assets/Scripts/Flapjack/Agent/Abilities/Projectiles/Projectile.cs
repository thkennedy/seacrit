﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float damage;						//Base damage of the spines launched
	public float knockBack;						//How much the projectile knocks the target back that was hit on collision
	public float speed;							//Starting speed of the projectile
	public Transform rotationMesh;				//The mesh to be rotated by spin
	public float rotationSpeed;					//how  fast the projectile spins about its axis
	public float lifetime = 1f;					//How long the projectile exists before being destroyed
	public Fish firingFish;						//The fish that fired the projectile
	public float diminishRate = .97f;			//how  quickly the projectile  fades
	public float killScale = .05f;				//The scale at which the projectile is killed after being shrunk
	public float ZLockValue = 0f;					// lock the particle to a specific plane
	public GameObject impactEffect;				//particles that play on impact


	public virtual void OnTriggerEnter(Collider hit) {
		Fish otherFish = Helper.GetFish(hit);
		Debug.Log("otherFish = "+ otherFish+" go = "+hit.gameObject.name+ "  firing = "+ firingFish);
		if (otherFish != firingFish)	// dont kill the fish firing
		{
			if (otherFish != null)	// then we hit another fish
			{
				otherFish.TakeDamage(damage, firingFish);
				otherFish.transform.rigidbody.AddForce(transform.right * knockBack, ForceMode.Impulse);
				GlobalData.SoundMan.playAbilitySound(otherFish.audio,0);
				
			}
			//else // we hit some non-fish
			GameObject newObject = (GameObject) Instantiate (impactEffect, transform.position, Quaternion.identity);
			DestroyObject (gameObject); 
		}
	}

	public virtual void OnFixedUpdate() {
		//transform.Translate (speed/100f, 0f, 0f);		//Moves the projectile at its intended speed forward
		rotationMesh.Rotate (Vector3.right * rotationSpeed);
		lifetime -= Time.deltaTime;
		if (lifetime < 0f) transform.localScale *= diminishRate * Time.deltaTime;
		if (transform.localScale.x < killScale) DestroyObject (gameObject);	//kills object if lifetime is up
		Vector3 pos = transform.position;
		pos.z = ZLockValue;
		transform.position = pos;
	}
	// Use this for initialization
	void Start () {

	}
	// Update is called once per frame
	void FixedUpdate () {
		OnFixedUpdate();
	}
}
/*Ideas:
Projectile that has a falloff and falls downard (would be hard to program AI to fire accurately)



 */