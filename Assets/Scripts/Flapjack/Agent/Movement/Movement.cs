﻿/*DESCRIPTION:

Controls all transformations for all fish. Serves as the base class for npcMovement and playerMovement.

moveFish(): creates a vector based off this idea (http://natureofcode.com/book/chapter-6-autonomous-agents/ , section 6.3),
then translates the fish by adding a force

rotateFish(): rotates the to face the input direction (taking horizontal bias into account).  After determining horizontal
bias, the vector direction is converted into an angle in degrees.  The player is then rotated to face this angle
by using the Quaternion.Slerp() function.

*/
using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
	#region variables

    public enum Zplane
    {
        Normal = 0,
        Back = 5
    }

    //movement
    private float stopAreaRadius = 0.15f;	// only used if isMouseControlled = true
	[HideInNormalInspector] public Vector3 InputDirection;
	public bool isMouseControlled = false;
	public float drag = 4;
	public float AppliedForce;
	public float AbilityForceMod = 1;
	private float _appForceTarget;
    private float _appForceSmoothDv;
	[HideInNormalInspector] public bool Moving = true;
	public float FixedZ = 0.0f;
	public float AngularDrag = 40f;		// I couldn't get it to work in the prefab so put it here! -PJ
	public bool LockInputMagnitude = false;
	
	[HideInNormalInspector] public float turningValue;		// float used to indicate which direction the fish is turning
	
	//rotation
	float horizontalBiasMod = 0.2f;
	float rotationSpeed = 0.08f;	
	
	private Fish _thisFish;
	
	#endregion
	
	#region functions

	//initialization
	public Movement Start()
	{
        // switch z movment plane
	   

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z * 0.95f + 0.05f * FixedZ);	// so the player / fish doesn't move away from the plane accidentally.

		//set values for variables
		_appForceTarget = AppliedForce;
		rigidbody.useGravity = false;
		_thisFish = Helper.GetFish(transform);
		if (_thisFish == null)
		{
			Debug.LogError("Can't find FISH for Movement!");
		}
		return this;
	}

	void Update()
	{
		if (isMouseControlled) // then read the mouse and set the inputdirection
		{
			//movement
			Moving = true;
			InputDirection = new Vector3(Input.mousePosition.x,Input.mousePosition.y,0) - new Vector3(Screen.width/2,Screen.height/2,0);//assign inputDirection
			InputDirection = InputDirection * (1/((float)Screen.height/2));//normalise inputDirection
			if(InputDirection.magnitude>1){InputDirection=InputDirection.normalized;}//makes sure inputDireciton does not exceed 1
			else if(InputDirection.magnitude<stopAreaRadius){Moving = false;}//enables player to sotp
		}
	}
	//movement
	void FixedUpdate ()
	{
		
		if (_thisFish.IsBeingEaten)
		{
			Transform newpos = _thisFish.BeingEatenTransform;
			
			Transform x = _thisFish.FishEatingMe.GetComponentInChildren<DamageGobble>().transform.Find("EatPos");
			
			gameObject.transform.position = newpos.position;
			gameObject.transform.rotation = newpos.rotation;
			
			gameObject.rigidbody.velocity = Vector3.zero;		// Need these to stop the fish moving from the spot we want!
			gameObject.rigidbody.angularVelocity = Vector3.zero;
			return;
		}
		if (!_thisFish.isAlive()) { return;}	// disable all movement
		if(gameObject.tag == "Player")
		{
			//move player
			if(transform.position.y < 10) //underwater
			{
				//used to ensure a smooth change of velocity when entering water
				if(Moving)AppliedForce = Mathf.SmoothDamp(AppliedForce, _appForceTarget*AbilityForceMod, ref _appForceSmoothDv,0.2f);
				else AppliedForce = Mathf.SmoothDamp(AppliedForce, 0, ref _appForceSmoothDv, 0.2f);
				
				rigidbody.useGravity = false;
				rigidbody.drag = drag;
				MoveFish();
				RotateFish(InputDirection);
			}
			else //breaching
			{
				//used to ensure a smooth change of velocity when entering water
				AppliedForce = Mathf.SmoothDamp(AppliedForce,0,ref _appForceSmoothDv,0.2f);
				
				rigidbody.useGravity = true;
				rigidbody.drag = 0;
				RotateFish(rigidbody.velocity);
			}
		}
		else // Not the player - AI
		{
			if(transform.position.y < 10) //underwater
			{
				rigidbody.useGravity = false;
				rigidbody.drag = drag;
				MoveFish();
				RotateFish(InputDirection);
			}
			else // breaching
			{
				rigidbody.useGravity = true;
				rigidbody.drag = 0;
				RotateFish(rigidbody.velocity);
			}
		}
		rigidbody.angularDrag = AngularDrag;
		transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z *0.95f + 0.05f *FixedZ);	// so the player / fish doesn't move away from the plane accidentally.
	}

	public float getAppliedForce()
	{
		float f = AppliedForce;
		f *= Mathf.Sqrt(1.0f + _thisFish.AcquiredSpeedBoost);
		return f;
	}
	void MoveFish()
	{
		float f= getAppliedForce();
		//Debug.Log("f = "+f+"  AppliedForce = "+AppliedForce);
		rigidbody.AddForce( f * ((LockInputMagnitude)?InputDirection.normalized:InputDirection) - rigidbody.velocity );
		
	}

	private void RotateFish(Vector3 direction)
	{
		//horizontal bias
		direction = direction + (1/rigidbody.velocity.magnitude)*horizontalBiasMod*((direction.x>0) ? new Vector3(1,0,0) : new Vector3(-1,0,0)).normalized;
		
		//convert vector to angle
		float angle = -1 * ((Mathf.Atan2(direction.x,direction.y) * Mathf.Rad2Deg) -90);
		if (float.IsNaN (angle) )
		{	
			angle = 0f;	// hack
		}
		if(angle > 180) angle -= 360;
		
		//rotate player
		
		if(angle > -90 && angle < 90) //facing right
		{
			transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(new Vector3(0, 0, angle)), rotationSpeed);
			turningValue = -transform.rotation.eulerAngles.y;
		}
		else //facing left
		{
			transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(new Vector3(0, 180, 180-angle)), rotationSpeed);
			turningValue = 180 - transform.rotation.eulerAngles.y;
		}
		if (Mathf.Abs(turningValue +360) < Mathf.Abs(turningValue) )
		{
			turningValue = turningValue +360;
		}
	}
	#endregion
}
