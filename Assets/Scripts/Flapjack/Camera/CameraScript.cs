﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
	#region variables
	//references
	GameObject player;

	//camera related variables
	float camZPosSpeedMod;
	float fieldOfViewMin = 45f;
	float fieldOfViewMax = 80f;
	float fieldOfViewPlayerSizeMod = 5.0f;
	float fieldOfViewSpeedMod = 7f;
	float fieldOfViewV;
	float distanceFromPlayer;

	public float frustumHeight;
	public float frustumWidth;
	#endregion
	
	#region cameraFunctions
	public CameraScript Initialise(GameObject p)
	{
		player = p;
		camZPosSpeedMod = 0.75f;

		return this;
	}
	void Update()
	{
		if (player== null) { return;}
		//calculate and set the camera's field of view
		camera.fieldOfView = Mathf.SmoothDamp(camera.fieldOfView, fieldOfViewMin + fieldOfViewPlayerSizeMod * player.transform.localScale.x + fieldOfViewSpeedMod * player.rigidbody.velocity.magnitude,ref fieldOfViewV,0.8f);
		if(camera.fieldOfView>fieldOfViewMax) camera.fieldOfView = fieldOfViewMax;

		transform.position = new Vector3(player.transform.position.x,player.transform.position.y,-3);

		//calculate the camera frustums for spawner, taken from http://docs.unity3d.com/Documentation/Manual/FrustumSizeAtDistance.html
		frustumHeight = 2*3*Mathf.Tan(camera.fieldOfView*0.5f*Mathf.Deg2Rad);
		frustumWidth = frustumHeight*camera.aspect;
	}
	#endregion
}