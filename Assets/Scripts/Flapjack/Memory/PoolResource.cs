using UnityEngine;
using System.Collections;

/// A representation of a single element in pool
public class PoolResource: MonoBehaviour
{
	[HideInInspector] public bool IsHidden;
	[HideInInspector] public readonly bool isPartOfGroup;
	[HideInInspector] public int PoolId;
}