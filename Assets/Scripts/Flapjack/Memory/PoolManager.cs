﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PoolManager : MonoBehaviour
{
    // the pool's limit to the number of elements (this number must be highr then the maximum number of fish in scene)
    private const int PoolLimit = 100;
    // the pool
    private static List<Fish> _pool = new List<Fish>();
    // the names of ojects in the pool (the index should match the index of _pool)
    private static List<string> _poolNames = new List<string>();
    // defulat position to give to hidden fish
    private static readonly Vector3 DefulatPos = new Vector3(0, -100, 0);
    private static GameObject _liveFish, _deadFish;

    void Awake()
    {
        var allFish = new GameObject {name = "FishPopulation"};
        _liveFish = new GameObject { name = "LiveFish" };
        _liveFish.transform.parent = allFish.transform;
        _deadFish = new GameObject { name = "DeadFish" };
        _deadFish.transform.parent = allFish.transform;
    }

    public static PoolResource PoolCreate(string name, Vector3 position, Quaternion rotation, Movement.Zplane zPlane)
    {
        GameObject element;
        position.z = (int)zPlane;
        // if fisj alreadu exist in pool
        if (_poolNames.Contains(name))
        {
            var index = GetLastHiddenElement(name);
            //if there is a hidden fish that we want
            if (index != -1)
            {
                element = _pool[index].gameObject;
                _pool[index].IsHidden = false;
                element.SetActive(true);
                // TODO: reset any needed fish stats
                element.transform.position = position;
                element.transform.rotation = rotation;
                element.transform.parent = _liveFish.transform;
                return element.GetComponent<PoolResource>();
            }
        }

        // if fish does not exist in pool
        // make room for new fish
        // and add new fish to pool
        MakeRoomInPool();
        element = FishBuilder.CreateFish(name, zPlane);
		element.transform.position = position;
        element.transform.parent = _liveFish.transform;
        _pool.Add(element.GetComponent<Fish>());
        _poolNames.Add(element.name);
        return element.GetComponent<PoolResource>();
    }

    public static void PoolDestroy(GameObject poolObj)
    {
        poolObj.SetActive(false);
        poolObj.transform.position = DefulatPos;
        poolObj.transform.parent = _deadFish.transform;
    }

    public static void PoolClean()
    {
            _pool.Clear();
            _poolNames.Clear();
    }

    /// <summary>
    /// retruns the index of last hidden element matching the given name.
    /// if not found, returns -1.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private static int GetLastHiddenElement(string name)
    {
        for(var i=0; i<_pool.Count; i++)
        {
            if(_poolNames[i] == name)
            {
                if(_pool[i].IsHidden)
                {
                    return i;
                }
            }
        }
        return -1;
    }

    // used to make room for other pool elements
    private static void MakeRoomInPool()
    {
        var indexToRemove = 0;
        // if the pool count has reachd the limit
        if (_pool.Count >= PoolLimit)
        {
            // use this loop to find the first hiddn pool element
            for (var i = 0; i < _pool.Count; i++ )
            {
                if(_pool[i].IsHidden)
                {
                    indexToRemove = i;
                    return;
                }
            }

            //Debug.Log("Making room in pool. Removing " + _pool[indexToRemove].gameObject.name + " from pool");

            // remove the element in the pool to make room for a new one
            _pool.RemoveAt(indexToRemove);
            _pool.TrimExcess();
            _poolNames.RemoveAt(indexToRemove);
            _poolNames.TrimExcess();
        }
    }
}
