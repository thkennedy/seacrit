﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

    public class FishBuilder : MonoBehaviour {
	
        private static AssetManagerData _assetsData;
        private static readonly Vector3 DefulatPos = new Vector3(-32, 10, 0);
        private static GameObject _fishGo;

        public static Dictionary<string, FishAsset> FishByName = new Dictionary<string, FishAsset>();

        void Start()
        {
            // read the data from xml
			GlobalData gd = GameObject.Find("Managers").GetComponent<GlobalData>();
			_assetsData = Helper.Deserialize<AssetManagerData>(gd.XMLAssetsData);
            _assetsData.PostConstruct();
            DataToDictionary();
        }

        // creates values for all fish in Dictionary so they can be accessed by name
        private void DataToDictionary()
        {
            foreach (var fish in _assetsData.FishRaces)
            {
                FishByName.Add(fish.FishName, fish);
            }
        }

        /// <summary>
        /// creates npc fish. Instamtiates a new game object from a prefab.
        /// </summary>
        /// <param name="fishAsset"></param>
        /// <returns>a npc <see cref="Fish"/> object or a 'boxy' -default object if prefab doesnt exsist</returns>
        public static GameObject CreateFish(string fishName, Movement.Zplane zPlane)
        {
        	  // Debug.Log("Trying to create " + fishName);
            if(!FishByName.ContainsKey(fishName))
            {
                Debug.LogError("Trying to create " + fishName + " but such fish does not exist in AssetsData xml");
                return null;
            }
            string prefabPath = FishByName[fishName].AssetLocation;

            //try to locate prefab
            try 
            {
                _fishGo = (GameObject)Instantiate(Resources.Load(prefabPath, typeof(GameObject)));
            }
            catch
            {
                Debug.LogError("Trying to create " + fishName + " but such fish does not have a prefab");
                return null;
            }

            _fishGo.transform.position = DefulatPos;
            if (!CheckComponents(_fishGo)) return null;
            InitRigidBody(_fishGo.rigidbody);
            //Fish fi = _fishGo.AddComponent<Fish>().Initialize(FishByName[fishName], zPlane);
            return _fishGo;
        }

        /// <summary>
        /// creates player
        /// </summary>
        /// <param name="fishAsset">xml data</param>
        /// <returns>a player <see cref="Fish"/> object</returns>
        public static GameObject CreatePlayer()
        {
            const string fishName = "PlayerFish";
            if (!FishByName.ContainsKey(fishName))
            {
                Debug.LogError("Trying to create " + fishName + " but such fish does not exist in AssetsData xml");
                return null;
            }
            string prefabPath = FishByName[fishName].AssetLocation;

            //try to locate prefab
            try
            {
                _fishGo = (GameObject)Instantiate(Resources.Load(prefabPath, typeof(GameObject)));
            }
            catch
            {
                Debug.LogError("Trying to create " + fishName + " but such fish does not have a prefab");
                return null;
            }
            //_fishGo.transform.position =  Vector3.zero;
            PlayerFish pf = _fishGo.GetComponentInChildren<PlayerFish>();
            // now make a body and other parts
            Component body;
            Component head;
            
            // Create the BODY
            //Component body = (Component)Instantiate(Resources.Load("Prefabs/FishParts/sharkBody", typeof(Component)));
           
            try
            {
            	  //body = (Component)Instantiate(Resources.Load("Prefabs/FishParts/baracudaBody", typeof(Component)));
            	  body = (Component)Instantiate(pf.IntitialBody);
            	  body.transform.parent = _fishGo.transform;
            	  body.transform.position = _fishGo.transform.position;
            	  body.transform.rotation = _fishGo.transform.rotation;
            	  body.transform.localScale = new Vector3(1f,1f,1f);
            	  body.name = "Body";
            }
            catch
            {
                Debug.LogError("Error trying to create the players body!");
                return null;
            }

            // Create the HEAD
            try
            {
			  head = (Component)Instantiate(pf.IntitialHead);
			  Transform root = body.transform.Find("Root");
			  head.transform.parent = root;
			  head.transform.position = root.position;
			  head.transform.rotation = root.rotation;
			  head.transform.localScale = new Vector3(1f,1f,1f);
			  head.name = "Head";
		  }
		  catch
            {
                Debug.LogError("Error trying to create the players head!");
                return null;
            }
           
            // Create other components
             _fishGo.transform.position = DefulatPos;
             
            if (!CheckComponents(_fishGo)) return null;
           
           
            InitRigidBody(_fishGo.rigidbody);
            //PlayerFish pf = _fishGo.GetComponentInChildren<PlayerFish>();
            
			//pf.CanEat = true;
		  _fishGo.name = fishName;
		  
		  // now store the component info
		  pf.Body = body;
		  pf.Head = head;
		  
		  // now update the colors
		  PickupItem.UpdateFishComponentColors(pf.Head, pf.Body);
		  
		  Helper.ChangeLayersRecursively(_fishGo.transform,"FishPlayer");
		  
          return _fishGo;
        }
	
        /// <summary>
        /// creates a default-asset(made of box primitives) fish
        /// </summary>
        /// <param name="fishAsset"></param>
        /// <returns>a boxy npc <see cref="Fish"/> object</returns>
        public static GameObject CreateDefault()
        {
            const string prefabPath = "Temp/Default";
            const string fishName = "__BASE__";
            //try to locate prefab
            try
            {
                _fishGo = (GameObject)Instantiate(Resources.Load(prefabPath, typeof(GameObject)));
            }
            catch
            {
                Debug.LogError("Trying to create " + fishName + " but such fish does not have a prefab");
                return null;
            }

            _fishGo.transform.position = DefulatPos;
            if (!CheckComponents(_fishGo)) return null;
            InitRigidBody(_fishGo.rigidbody);

            //_fishGo.AddComponent<Fish>().Initialize(FishByName[fishName], Movement.Zplane.Normal);

            return _fishGo;
        }
	
        /// <summary>
        /// Give rigidbody initial vaues
        /// </summary>
        /// <param name="rb"></param>
        private static void InitRigidBody(Rigidbody rb)
        {
            rb.useGravity = false;
            rb.constraints = RigidbodyConstraints.FreezePositionZ;
            rb.mass = 1;
            rb.drag = 4;
            rb.angularDrag = 4;
        }

        /// <summary>
        /// check no components are missing
        /// </summary>
        /// <param name="go">The ptop GO of the fish prefab</param>
        /// <param name="name">the fish name</param>
        /// <returns>are all components in place?</returns>
        private static bool CheckComponents(GameObject go)
        {
            var name = go.name;
            if(!go.GetComponentInChildren<Rigidbody>()) {
                Debug.LogError("SERIOUS ERROR! " + name + " has no RigidBody");
                return false;
            }
            return true;
        }
    }