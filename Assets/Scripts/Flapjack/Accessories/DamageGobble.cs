﻿using UnityEngine;
using System.Collections;

public class DamageGobble : MonoBehaviour {

	public string ChewAnim;
	public float EatRate = 0.1f;
	private Fish _thisFish = null;		// fish this gobble is attached to
	private PlayerFish _thisPlayerFish = null;
	private bool _isPlayer = false;	
	
	public bool isActive = true;
	
	// Use this for initialization
	
	void Start () 
    {
		_thisFish = Helper.GetFish(this);
	    //Debug.Log("Fish "+gameObject.name+" _thisFish = " +_thisFish);
	    _thisPlayerFish = Helper.GetPlayerFish(this);
		_isPlayer =  _thisPlayerFish != null;
		if (_thisFish == null) 
		{
			isActive =false;		// deactivate if not attached to a fish so we can easily edit prefabs in scene
		}
	}
	
	void TryToEat(Fish foe)
	{
		if(_thisFish.EatCheck(foe))	// if we CAN eat the other fish
		{
			if(foe.gameObject.tag=="Player")	// if we are trying to eat the player
			{
				if (_thisFish.CanDamagePlayer) 	// and we are allowed to damage the player
				{
					Debug.LogError("Player was eaten by another fish!");
				}
			}
			else		// we are eating an AI fish
			{
				if(_isPlayer)	// then player ate AI fish
				{
					_thisFish.AddStats(foe);
					_thisFish.GrowByEating(foe.FishWeight);
					Debug.Log("New fish stats :  acquiredSpeedBoost = "+_thisFish.AcquiredSpeedBoost+"  energyMax = "+_thisFish.EnergyMax);
					if(Random.value <0.2f ) {Instantiate(GlobalData.SpriteYUM , transform.position+ new Vector3(0f,0f,-0.5f), Quaternion.identity );}
				}
			}
			// either way the foe was eaten.
			foe.BeingEaten( _thisFish, transform.Find("EatPos") , EatRate);
			GlobalData.SoundMan.playRandomBite(_thisFish.audio);
			//transform.parent.animation.Play(ChewAnim);
			_thisFish.NumFishBeingEaten++;
			
		}
	}
	
	void OnTriggerEnter(Collider other) 
    {	
   		if (!isActive) { return;}
		//Debug.Log ("Trying to eat : "+col.gameObject.name);
		if (_thisFish == null) // OOps!
		{
			Debug.LogError("Error - DamageGobble attached to an object with no Fish class! Name = "+name);
		}
		if (_thisFish.CanEat)
		{
			Fish foe = other.gameObject.GetComponentInChildren<Fish>();
			if(foe!=null)
			{
				if (_thisFish.CanDamagePlayer || other.gameObject.GetComponentInChildren<PlayerFish>() == null)	// if the other fish is not the player, or they are the player and we are allowed to damage them
				{
					TryToEat(foe);		// try to eat the other fish - may not happen depending on FishWeight etc
				}
			}
		}
		//else do nothing, collider isnt a fish.
	}
}
