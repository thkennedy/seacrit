﻿using UnityEngine;
using System.Collections;

public class OpenableChest : OpenableItem {
	// This class is a chest that can be opened
	
	// Use this for initialization
	void Start () 
    {
		Init();		// base initialization from base class
	}


    public override void OnOpenCustom()  // virtual method to allow custom anims etc in derived classes
    {
		//transform.Find("chestLid").animation.Play("ChestOpen");
		transform.Find("chestLid").animation.Play();
		//animation.Play("ChestOpening");
		Debug.Log("Opening chest!");
	}
}
