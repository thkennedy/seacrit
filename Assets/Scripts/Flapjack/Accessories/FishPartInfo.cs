﻿using UnityEngine;
using System.Collections;

public class FishPartInfo : MonoBehaviour {
	
	// class that contains useful info about his fish part
	public string DiffuseColorComponentName = "Put the name of the meshRenderer transform here ie Shark";
	public string DiffuseColorNameInMaterial = "Put the color variable name here, ie _Color";
	
}
