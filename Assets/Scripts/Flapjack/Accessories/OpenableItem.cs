﻿using UnityEngine;
using System.Collections;

public class OpenableItem : MonoBehaviour {
	
	// This is an item that can be opened by the player for a cost
	// Override onOpenCustom() in derived classes for custom behaviour
	
	// What it costs to to open the item:
	public float CostSize = 0.0f;
	public float CostEnergy = 0.0f;
	public float CostEnergyMax = 0.0f;
	public float CostASpeedBoost = 0.0f;
	public float CostAttack = 0.0f;
	public float CostDefense = 0.0f;
	public bool IsOpen = false;
	
	public GameObject itemToReveal;
	public float revealDelay = 1.0f;		// time in seconds before we reveal the item
	
	private float _revealTimer;
	private bool _isRevealing;
	
	void Start () 
    {
		Init();
	}
	
	public void Init()
	{
		if (itemToReveal != null)
		{
			itemToReveal.SetActive(false);
		}
	}
	
	public void DoOpen(PlayerFish pf)
	{
		// first pay the price!
		pf.FishWeight -= CostSize;
		pf.Energy -= CostEnergy;
		pf.EnergyMax -= CostEnergyMax;
		pf.AcquiredSpeedBoost -= CostASpeedBoost;
		pf.Attack -= CostAttack;
		pf.Defense -= CostDefense;
		
		// update the status
		IsOpen = true;
		
		// reveal an item after a delay (to allow for any animations etc)
		_revealTimer = 0.0f;
		_isRevealing = true;
		
		// do any custom behaviour
		OnOpenCustom();
	}
	
	public void Update()
	{
		if (_isRevealing)
		{
			_revealTimer += Time.deltaTime;
			if ( _revealTimer >= revealDelay) // then reveal the item
			{
				RevealItem(itemToReveal);
				_isRevealing = false;
			}
		}
	}
	
	public void RevealItem(GameObject item) 
	{
		item.SetActive(true);
	}

    public virtual void OnOpenCustom() // virtual method to allow custom anims etc in derived classes
    { 

	}
	
	public bool CanOpen(PlayerFish pf) // returns true if the player can pay the cost to open this item
	{
		if (pf.FishWeight < CostSize) { return false; }
		if (pf.Energy < CostEnergy) { return false; }
		if (pf.EnergyMax < CostEnergyMax) { return false; }
		if (pf.AcquiredSpeedBoost < CostASpeedBoost) { return false; }
		if (pf.Attack < CostAttack) { return false; }
		if (pf.Defense < CostDefense) { return false; }
		return true;
	}
	
	void OnCollisionEnter(Collision other)
	{
		PlayerFish pf = other.gameObject.GetComponent<PlayerFish>();
		if (pf != null)
		{
			if ( !IsOpen && CanOpen(pf) ) 
			{ 
				DoOpen(pf); 
			}
		}
	}
	
	//void OnTriggerEnter(Collider other) {	}
    
}
