﻿using UnityEngine;
using System.Collections;

public class RotationStopper : MonoBehaviour {
	
	// This class fixes the rotation of this component in world-space
	
	public Vector3 TargetRotation;
	private Quaternion _qrot;

	void Start()
	{
		_qrot = Quaternion.Euler(TargetRotation);
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = _qrot;
	
	}
}
