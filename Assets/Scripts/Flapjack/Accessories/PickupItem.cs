﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour {
	// This class implements an object that can be picked up by the player
	
	// PARTS THAT GET PICKED UP
	
	// Fish parts
	public Component Head = null;		// head component to replace
	public Component Body = null;		// body component to replace
	
	// Abilities
	//public GameObject ALmb; // assigned to body
	//public GameObject ARmb; // assigned to head
	
	// Creation settings
	public float GrowTime = 1.0f;			// how long it takes to shrink to nothing and be destroyed
	private bool _doGrow;
	private Vector3 _growRate;
	private Vector3 _finalSize;
	private float _growTimer;
	
	
	// Destruction settings
	public float ShrinkTime = 1.0f;			// how long it takes to shrink to nothing and be destroyed
	private bool _doShrink;
	private Vector3 _shrinkRate;
	
	// Disable time
	public float AvailableDelay = 1.0f;
	private float _availableDelay;
	
	private bool _hasBeenUsed;
	
	private bool startDone = false;

	void Start() 
    {
		_growRate = transform.localScale / GrowTime;
		_finalSize = transform.localScale;
		transform.localScale = Vector3.zero;
		_doGrow = true;
		_availableDelay = AvailableDelay;
		Debug.Log("Start _availableDelay = "+_availableDelay);
		startDone = true;
	}
	
	void FixedUpdate () 
    {
    	    if (!startDone) { return;}
    	    _availableDelay -= Time.deltaTime;
		if (_doGrow) // then we are growing it
		{
			transform.localScale = transform.localScale + _growRate * Time.deltaTime;
			_growTimer += Time.deltaTime;
			if ( _growTimer >= GrowTime)
			{
				_doGrow = false;
				transform.localScale = _finalSize;
			}
		}
		
		if (_doShrink) // then we are killing it
		{
			transform.localScale = transform.localScale - _shrinkRate * Time.deltaTime;
		}
	}
	
	public void SwapHead(PlayerFish pf)
	{
		Component newhead = null;		// body component to replace
		try
		{
			if (pf.Head != null)
			{
			    Destroy(pf.Head.gameObject); 
			    pf.Head = null;
			}
			newhead = (Component)Instantiate(Head);
			Transform root = pf.Body.transform.Find("Root");
			newhead.transform.parent = root;
			newhead.transform.position = root.position;
			newhead.transform.rotation = root.transform.rotation;
			newhead.transform.localScale = new Vector3(1f, 1f, 1f);
			newhead.name = "Head";
			pf.Head = newhead;
			UpdateFishComponentColors(newhead, pf.Body);
		}
		catch
		{
			Debug.LogError("Error trying to create the players body!");
			return;
		}
	}
	
	public void SwapBody(PlayerFish pf)
	{
		Component newbody = null;		// body component to replace
		try
		{
			if (pf.Body != null)
			{
			    Destroy(pf.Body.gameObject); 
                pf.Body = null;
			}
			newbody = (Component)Instantiate(Body);
			newbody.transform.parent = pf.gameObject.transform;
			newbody.transform.position = pf.gameObject.transform.position;
			newbody.transform.rotation = pf.gameObject.transform.rotation;
			newbody.transform.localScale = new Vector3(1f, 1f, 1f);
			newbody.name = "Body";
			pf.Body = newbody;
			pf.Head.transform.parent = newbody.transform.Find("Root");
			UpdateFishComponentColors(pf.Head, newbody);
		}
		catch
		{
			Debug.LogError("Error trying to create the players body!");
			return;
		}
	}
	
	public static void UpdateFishComponentColors(Component head, Component body)
	{
		FishPartInfo headinfo = head.GetComponentInChildren<FishPartInfo>();
		FishPartInfo bodyinfo = body.GetComponentInChildren<FishPartInfo>();
		//Debug.Log("bodyinfo = " +bodyinfo);
		SkinnedMeshRenderer headmesh = head.transform.Find(headinfo.DiffuseColorComponentName).GetComponentInChildren<SkinnedMeshRenderer>();
		SkinnedMeshRenderer bodymesh = body.transform.Find(bodyinfo.DiffuseColorComponentName).GetComponentInChildren<SkinnedMeshRenderer>();
		//Debug.Log("bodymesh = " +bodymesh);
		UpdateFishMaterials(headmesh,headinfo.DiffuseColorNameInMaterial, bodymesh, bodyinfo.DiffuseColorNameInMaterial);
	}
	
	public static void UpdateFishMaterials(SkinnedMeshRenderer headmesh, string headColorName, SkinnedMeshRenderer bodymesh, string bodyColorName)
	{
		string[] matparamsColors = {"_DiffuseColor","_DiffuseColor2"};
		string[] matparamsFloats = {"_DiffuseMultiply","_DiffusePower","_DiffusePowerPower","_Specular","_Gloss"};
		foreach( string s in matparamsColors)
		{
			Color col = bodymesh.materials[0].GetColor(s);
			foreach (Material m in headmesh.materials)
			{
				m.SetColor(s,col);
			}
		}
		
		foreach( string s  in matparamsFloats)
		{
			float x = bodymesh.materials[0].GetFloat(s);
			foreach (Material m in headmesh.materials)
			{
				m.SetFloat(s,x);
			}
		}
		
	}

	public void OnPickup(PlayerFish pf) // Called when the player picks up this item
	{
		if (Body != null) { SwapBody(pf); }
		if (Head != null) { SwapHead(pf); }
		
		AbilityManager.UpdateAllAbilityLists(pf);
	} 
	
	void OnTriggerEnter(Collider other) {
		if (!startDone) { return;}
		if (_hasBeenUsed) { return;}
		if (_availableDelay > 0f) { return;}
		PlayerFish pf = Helper.GetPlayerFish(other);
		if (pf != null)
		{
			_hasBeenUsed = true;
			Debug.Log("Pickup! _availableDelay = "+_availableDelay);
			OnPickup(pf);
			Helper.ChangeLayersRecursively(pf.transform,"FishPlayer");
			Kill();
		}
    }
    
    private void Kill()
	{
		_shrinkRate = transform.localScale / ShrinkTime;
		_doShrink = true;
		Destroy(gameObject,ShrinkTime);
	}
}
