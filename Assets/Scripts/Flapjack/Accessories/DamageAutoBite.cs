﻿using UnityEngine;
using System.Collections;

public class DamageAutoBite : MonoBehaviour {

	public string BiteAnim;
	public float cooldown = 1.0f;
	public float biteDamage = 0.1f;
	
	private Fish _thisFish = null;		// fish we are attached to
	private PlayerFish _thisPlayerFish = null;
	private Bite _bite = null;
	private bool _isPlayer = false;		
	private float _pauseTimer = 0.0f;
	
	void Start () 
    {
		_thisFish = Helper.GetFish(transform);
		Debug.Log("Fish "+gameObject.name+" _thisFish = " +_thisFish);
	    _thisPlayerFish = transform.root.GetComponentInChildren<PlayerFish>();
	    _bite = transform.GetComponentInChildren<Bite>();
	    if (_bite == null) { Debug.LogError("No Bite found in DamageAutoBite - did you forget to attach one?");}
		_isPlayer =  _thisPlayerFish != null;
	}
	
	// Update is called once per frame
	void Update () {
		_pauseTimer += Time.deltaTime;
		
	
		_thisFish.isBiting = _thisFish.isAlive() && _pauseTimer <= cooldown ;
		//_thisFish.IsBiting = true;
	}
	
	void DoBite()
	{
		// HACK 
		//gameObject.GetComponent<Bite>().bite(transform.parent.gameObject, 1, false, biteDamage);
		if (_bite!=null)
		{
			_bite.onRelease(1.0f);
		}
		// TODO - animation
		_pauseTimer = 0.0f;
		//Debug.Log("Bite! "+transform.parent.gameObject.name);
		_thisFish.isBiting = _thisFish.isAlive();
	}
	
	public void checkBite(Collider other)
	{
		if (_pauseTimer < cooldown) { return;}
		// otherwise good to go
		if (Helper.GetFish(other.transform) !=null) // then a fish has intersected the trigger
		{
		    DoBite();
		}
	}
	
	void OnTriggerEnter(Collider other) 
	{	
		checkBite(other);
	}
	
	void OnTriggerStay(Collider other) 
	{	
		checkBite(other);
	}
		
}
