﻿using UnityEngine;
using System.Collections;

public class DamageTrigger : MonoBehaviour {
	// This is a trigger that deals damage. Can be attached to fish and other parts
	// It calls the method "ModifyDealtDamage(float amt)" in any attach fish class to allow it to modify the raw values
	
	public string TriggerName;
	public float DamageInstant = 0.1f;			// instant damage when touching the trigger
	public float DamageInstantCooldown = 0.1f;	// cooldown - should always be >0 to avoid multiple triggers accidentally
	public float DamagePerSecond = 0.01f;
	public bool CanDamagePlayer = true;
	public bool IsTriggering = false;
	
	public string AnimInstantDamage;

	private float _cooldownTimer;
	
	void Start()
	{
		gameObject.animation.Play(AnimInstantDamage);
	}
	
	public void Update()
	{
		_cooldownTimer += Time.deltaTime;
	}
		
	void OnTriggerEnter(Collider other) {
		
		if (_cooldownTimer < DamageInstantCooldown) {return;}	
		// first see if the other collider is a fish
		//Debug.Log("Trigger called : "+TriggerName);
		Fish foe =  other.gameObject.GetComponentInChildren<Fish>();
		if (foe == null) {return;}
		
		// check if we are allowed to damage the player
		PlayerFish pf = other.gameObject.GetComponentInChildren<PlayerFish>();
		if (pf != null && !CanDamagePlayer) // then trying to damage the player but we're not allowed to
		{
			return;
		}
		
		// now see if we should modify the damaged based on a fish we are attached to 
		float x = DamageInstant;
		Fish f = gameObject.GetComponentInChildren<Fish>();
		if (f != null)
		{
			x = f.ModifyDealtDamage(x);
		}
		
		// apply damage
		if (x > 0f)
		{
			//Debug.Log("Dealing damage! : "+TriggerName);
			//Debug.Log("gameObject : "+gameObject);
			//Debug.Log("animation : "+gameObject.animation);
			foe.TakeDamage(x,Helper.GetFish(this));
			//gameObject.animation.Play(AnimInstantDamage);
			_cooldownTimer = 0.0f;
		}
		
	}
}
