﻿using UnityEngine;
using System.Collections;

public class Floater : MonoBehaviour {
	// This class just floats an object up and then kills it
	
	public float KillTravelDistance = float.MaxValue;
	public float KillTime = float.MaxValue;
	public float KillMaxY = float.MaxValue;
	public Vector3 velocity = Vector3.zero;
	public Vector3 startPos = Vector3.zero;
	public float shrinkTime = 1.0f;			// how long it takes to shrink to nothing and be destroyed
	
	private float timeAlive = 0.0f;
	private bool doShrink = false;
	private Vector3 shrinkRate;
	
	
	// Use this for initialization
	void Start () {
		startPos = transform.position;
	}
	
	void FixedUpdate () {
		
		transform.position = transform.position + velocity * Time.deltaTime;
		timeAlive += Time.deltaTime;
		if (doShrink) // then we are killing it
		{
			transform.localScale = transform.localScale - shrinkRate * Time.deltaTime;
		}
		else	// check if we should kill it
		{
			if ( (transform.position - startPos).magnitude > KillTravelDistance )	// distance kill
			{
				Kill();
			}
			if ( timeAlive > KillTime )	// time kill
			{
				Kill();
			}
			if ( transform.position.y > KillMaxY )	// height kill
			{
				Kill();
			}
		}
		
	}
	
	private void Kill()
	{
		shrinkRate = transform.localScale / shrinkTime;
		doShrink = true;
		Destroy(gameObject,shrinkTime);
	}
}
