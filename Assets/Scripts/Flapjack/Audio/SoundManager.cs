﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
	/*DOCUMENTATION:
	 DESCRIPTION & USE OF CLASS
	 Summary:  Handles all audio in FlapJack
	 
	 NOTE: no method of the class is static.  There should only be one instance of SoundManager, which is attached to
	 Manager   .  To access the instance, use: 
	 
	 SoundManager sM = GameObject.Find("Manager").GetComponent<SoundManager>();
	 
	 Use for regular fish: Every fish has their own AudioSource.  In order to play a particular sound, a fish will
	 pass a reference to its audiosource to SoundManager and the sound they want to play (given that's a parameter).
	 Every function where a sound can be chosen (i.e. it's not randomly chosen) has an associated enum, which can
	 be used to specify which sound should be played.  Fishes don't have access to all functions.
	 
	 Calling Example:  sM.playAbilitySound(aSource, enumAbilitySounds.gobble);
	 
	 Use for the player: The player [should have] two audio sources, to allow for the smooth transitions of sound.  The player
	 can access all functions.To access a function that players only have access to, pass the enum of the type of sound you 
	 want to play and then pass the enum of the sound you want to play.
	 
	 Calling Example:  sM.changePlayerMusic(arraySelection.aMusic,enumAreaMusic.shallowOcean);
	 
	 */
	
	#region variables
	#region allFish sounds

	public AudioClip[] biteSounds = new AudioClip[0];//Bite sounds
	public AudioClip[] abilitySounds = new AudioClip[0];//Ability sounds
	public AudioClip[] fishSounds = new AudioClip[0];//sounds associated with each fish

	#endregion
	#region Player only variables
  
	int zone;
	public AudioClip[] combatMusic   = new AudioClip[0];//combat music: 6 (for easy nomral, easy special; med norm, med spec; hard norm, hard spec)
	public AudioClip[] eventSounds	 = new AudioClip[0];//sounds for leveing up, finding something special, easter eggs, etc
	public AudioClip[] coralReef     = new AudioClip[0]; //Area music
	public AudioClip[] shallowOcean  = new AudioClip[0]; //Area music
	public AudioClip[] midOcean      = new AudioClip[0]; //Area music
	public AudioClip[] deeperOcean   = new AudioClip[0]; //Area music
	public AudioClip[] toxicOcean    = new AudioClip[0]; //Area music
	public AudioClip[] arcticOcean   = new AudioClip[0]; //Area music
  
	private AudioClip[][] onlyPlayerAudioArray = new AudioClip[8][];
	#endregion
	#region variables for controlling the player's audio soruces

	bool sourceOne = true;bool sourceTwo = false;public bool playingMusic=false;

	public AudioSource playerAudioSource1; 
	private float playerAudioSource1VolV;
	
	public AudioSource playerAudioSource2;
	private float playerAudioSource2VolV;

	GameObject player;

	#endregion	
	#endregion

	#region functions
    void Start()
    {
		onlyPlayerAudioArray[0] = combatMusic; onlyPlayerAudioArray[1] = eventSounds;onlyPlayerAudioArray[2] = coralReef;
		onlyPlayerAudioArray[3] = shallowOcean;onlyPlayerAudioArray[4] = midOcean   ;onlyPlayerAudioArray[5] = deeperOcean;
		onlyPlayerAudioArray[6] = toxicOcean;  onlyPlayerAudioArray[7] = arcticOcean;
	}

	// play a biting sound
	public void playRandomBite(AudioSource fishA)
   	{
		fishA.PlayOneShot(biteSounds[Random.Range(0,biteSounds.Length)]);
	}
	
	// play an ability sound
	public void playAbilitySound(AudioSource fishA,int ability)
	{
		fishA.PlayOneShot(abilitySounds[ability]);
	}
	
	// play a sound associated with a fish
	public void playFishSound(AudioSource fishA,int fish)
	{
		fishA.PlayOneShot(fishSounds[fish]);
	}
	
	#endregion
	
	#region playerOnlyFunctions
	public void changeZones(int z)
	{
		zone = z;
		playingMusic = false;
	}
	void onlyPlayerSounds(int arraySelection,int indexSelection)
	{
		if(sourceOne)//playerAudioSource1 is the active AudioSource
		{
			playingMusic = true;
			sourceOne = false;
			sourceTwo = true;
			playerAudioSource2.Stop();
			playerAudioSource2.PlayOneShot(onlyPlayerAudioArray[arraySelection][indexSelection]);
			//if type is not an event
			if(arraySelection != 1 || arraySelection != 0)
			{
				StartCoroutine(waitForAudio(arraySelection,indexSelection,true));
			}
			else//event
			{
				StartCoroutine(waitForAudio(arraySelection,indexSelection,false));
				//switch back to original audiosource
				sourceTwo = false;
				sourceOne = true;
			}
		}
		else//playerAudioSource2 is the active AudioSource
		{
			playingMusic = true;
			sourceTwo = false;
			sourceOne = true;
			playerAudioSource1.Stop();
			playerAudioSource1.PlayOneShot(onlyPlayerAudioArray[arraySelection][indexSelection]);
			//if type is not an event / combat situation
			if(arraySelection != 1 || arraySelection != 0)
			{
				StartCoroutine(waitForAudio(arraySelection,indexSelection,true));
			}
			else//event
			{
				StartCoroutine(waitForAudio(arraySelection,indexSelection,false));
				//switch back to original audiosource
				sourceOne = false;
				sourceTwo = true;
			}
		}
	}
	
	IEnumerator waitForAudio(int type,int selection,bool music)
	{
		yield return new WaitForSeconds(onlyPlayerAudioArray[type][selection].length);
		if(music)playingMusic = false;
	}

	void Update()
	{	
		if(sourceOne && !sourceTwo) //playerAudioSource1 is the active AudioSource
		{
			playerAudioSource1.volume = Mathf.SmoothDamp(playerAudioSource1.volume,1,ref playerAudioSource1VolV,2f);
			playerAudioSource2.volume = Mathf.SmoothDamp(playerAudioSource2.volume,0,ref playerAudioSource2VolV,2f);
			if(!playingMusic)//if music has stopped, change
			{
				onlyPlayerSounds(2+zone,Random.Range(0,onlyPlayerAudioArray[2+zone].Length));
			}
		}
		else if (sourceTwo && !sourceOne) //playerAudioSource2 is the active AudioSource
		{
			playerAudioSource2.volume = Mathf.SmoothDamp(playerAudioSource2.volume,1,ref playerAudioSource2VolV,2f);
			playerAudioSource1.volume = Mathf.SmoothDamp(playerAudioSource1.volume,0,ref playerAudioSource1VolV,2f);
			if(!playingMusic)
			{
				onlyPlayerSounds(2+zone,Random.Range(0,onlyPlayerAudioArray[2+zone].Length));
			}
		}
		else //both true or false
		{
			playerAudioSource1.volume = 1;playerAudioSource2.volume = 1;
			if(!playerAudioSource1.isPlaying) sourceOne = false;
			if(!playerAudioSource2.isPlaying) sourceTwo = false;
		}
	}
	bool canChange = false;
	#endregion
}