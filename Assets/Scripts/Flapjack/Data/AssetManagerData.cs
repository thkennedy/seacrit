﻿using System;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

[Serializable]
[XmlRoot("assetsData", Namespace = "")]
public class AssetManagerData 
{
    [XmlElement("fish", Namespace = "")]
    public FishAsset[] FishRaces { get; private set; }
	//
	public FishAsset playerAsset{get{return FishRaces[1];}}
	
	private int _start = 2; // index at which the fish races start
	
	private int _postConstructCounter = 0;
	
    public override string ToString()
    {
        var builder = new StringBuilder();
        foreach (var fish in FishRaces)
        {
            builder.AppendLine(fish.ToString());
        }
        return builder.ToString();
    }
	
	public void PostConstruct()
	{
		/* *
		 * Option A: call post constructor on the whole array and modify the data.
		 * each racial value will be modified using the __BASE__ value.
		 * */
		if(FishRaces.Length<2){
			Debug.LogError("PostConstruct called too soon. Fish array is too short!");
		}
		if(_postConstructCounter>0){
			Debug.LogError("Can't call PostConstruct twice!");
		}
		/*
		if(FishRaces[0].FishName!="__BASE__"){
			Debug.LogError("Fish[0] in xml isn't base fish or isn't named correctly! the correct name is: \"__BASE__\". Exiting....");
		}
        */
		for(int i = 1; i<FishRaces.Length; i++)
		{
			FishRaces[i].ModifyData(FishRaces[0]);
		}
        Debug.Log("DataAfterPostConstruct: " + ToString());
		_postConstructCounter++;
        //Debug.Break ();
	}
}
  
[Serializable]
[XmlRoot("fish", Namespace = "")]
public class FishAsset
{
    [XmlAttribute("name", Namespace = "")]
    public string FishName { get; private set; }

    [XmlAttribute("assetLocation", Namespace = "")]
    public string AssetLocation { get; private set; }
	
    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Fish Name: " + FishName);
        builder.AppendLine("Asset Location: " + AssetLocation);
        return builder.ToString();
    }

	public void ModifyData(FishAsset baseFish)
	{
        if (string.IsNullOrEmpty(FishName))
        {
            Debug.LogWarning("FishName is empty, geting defualt: " + baseFish.FishName);
            FishName = baseFish.FishName;
        }

        if (string.IsNullOrEmpty(AssetLocation))
        {
            AssetLocation = baseFish.AssetLocation;
        }

	}
}

