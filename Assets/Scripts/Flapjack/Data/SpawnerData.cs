﻿using System;
using System.Text;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("areas", Namespace = "")]
public class Areas
{
    [XmlElement("area", Namespace = "")]
    public Area[] TheAreas { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Areas");
        foreach (var area in TheAreas)
        {
            builder.AppendLine(area.ToString());
        }
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("area", Namespace = "")]
public class Area
{
    [XmlAttribute("name", Namespace = "")]
    public string AreaName { get; set; }

    [XmlElement("CommonSpwans", Namespace = "")]
    public CommonSpwans CommonSpwansList { get; set; }

    [XmlElement("RareSpwans", Namespace = "")]
    public RareSpwans RareSpwansList { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Area Name: " + AreaName);
        builder.AppendLine("Common Spwans: " + CommonSpwansList.ToString());
        builder.AppendLine("Rare Spwans: " + RareSpwansList.ToString());
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("CommonSpwans", Namespace = "")]
public class CommonSpwans
{
    [XmlAttribute("Probability", Namespace = "")]
    public float CommonSpwansProbability { get; set; }

    [XmlElement("SpwanList", Namespace = "")]
    public SpwanList[] SpwanListContent { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Common Spwans Probability: " + CommonSpwansProbability);
        builder.AppendLine("Spwan List: " + SpwanListContent.ToString());
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("RareSpwans", Namespace = "")]
public class RareSpwans
{
    [XmlAttribute("Probability", Namespace = "")]
    public float RareSpwansProbability { get; set; }

    [XmlElement("SpwanList", Namespace = "")]
    public SpwanList[] SpwanListContent { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Rare Spwans Probability: " + RareSpwansProbability);
        foreach (var list in SpwanListContent)
        {
            builder.AppendLine(list.ToString());
        }
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("SpwanList", Namespace = "")]
public class SpwanList
{
    [XmlAttribute("Probability", Namespace = "")]
    public float SpwanListProbability { get; set; }

    [XmlAttribute("Time", Namespace = "")]
    public float LastingTime { get; set; }

    [XmlAttribute("spawnRate", Namespace = "")]
    public float ListSpawnRate { get; set; }

    [XmlElement("fish", Namespace = "")]
    public FishData[] Fishs { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Spwan List Probability: " + SpwanListProbability);
        builder.AppendLine("Spwan List Time: " + LastingTime);
        builder.AppendLine("List Spwan Rate: " + ListSpawnRate);
        foreach (var fish in Fishs)
        {
            builder.AppendLine(fish.ToString());
        }
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("fish", Namespace = "")]
public class FishData
{
    [XmlAttribute("name", Namespace = "")]
    public string FishName { get; set; }

    [XmlAttribute("minGroupMembers", Namespace = "")]
    public int MinimumGroup { get; set; }

    [XmlAttribute("maxGroupMembers", Namespace = "")]
    public int MaximumGroup { get; set; }

    [XmlAttribute("singleProbability", Namespace = "")]
    public int SingleProbability { get; set; }

    [XmlAttribute("groupProbability", Namespace = "")]
    public int GroupProbability { get; set; }

    [XmlAttribute("startingSizeMultiplier", Namespace = "")]
    public float SizeMultiplier { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Fish Name: " + FishName);
        builder.AppendLine("Minimum Group: " + MinimumGroup.ToString());
        builder.AppendLine("Maximum Group: " + MaximumGroup.ToString());
        builder.AppendLine("Single Probability: " + SingleProbability.ToString());
        builder.AppendLine("Group Probability: " + GroupProbability.ToString());
        builder.AppendLine("Size Multiplier: " + SizeMultiplier.ToString());
        return builder.ToString();
    }
}