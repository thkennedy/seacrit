﻿using System;
using System.Text;
using System.Xml.Serialization;


[Serializable]
[XmlRoot("zones", Namespace = "")]
public class Zones
{
    [XmlElement("zone", Namespace = "")]
    public Zone[] TheZones { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Zones");
        foreach (var zone in TheZones)
        {
            builder.AppendLine(zone.ToString());
        }
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("zone", Namespace = "")]
public class Zone
{
    [XmlAttribute("name", Namespace = "")]
    public string ZoneName { get; set; }

    [XmlElement("fillLight", Namespace = "")]
    public FillLight ZoneFillLight { get; set; }

    [XmlElement("rimLight", Namespace = "")]
    public RimLight ZoneRimLight { get; set; }

    [XmlElement("accentLight", Namespace = "")]
    public AccentLight ZoneAccentLight { get; set; }

    [XmlElement("ambientLightColor", Namespace = "")]
    public AmbientLightColor ZoneAmbientLightColor { get; set; }

    [XmlElement("fogColor", Namespace = "")]
    public FogColor ZoneFogColor { get; set; }

    [XmlElement("gradientColor", Namespace = "")]
    public GradientColor ZoneGradientColor { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Zone Name: " + ZoneName);
        builder.AppendLine("Fill Light: " + ZoneFillLight.ToString());
        builder.AppendLine("Rim Light: " + ZoneRimLight.ToString());
        builder.AppendLine("Accent Light: " + ZoneAccentLight.ToString());
        builder.AppendLine("Ambient Light Color: " + ZoneAmbientLightColor.ToString());
        builder.AppendLine("Fog Color: " + ZoneFogColor.ToString());
        builder.AppendLine("Gradient Color: " + ZoneGradientColor.ToString());
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("fillLight", Namespace = "")]
public class FillLight
{
    [XmlAttribute("red", Namespace = "")]
    public byte Red { get; set; }

    [XmlAttribute("green", Namespace = "")]
    public byte Green { get; set; }

    [XmlAttribute("blue", Namespace = "")]
    public byte Blue { get; set; }

    [XmlAttribute("intensity", Namespace = "")]
    public float Intensity { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Fill Light Color: " + Red +", "+Green+", "+Blue);
        builder.AppendLine("Fill Light Intensity: " + Intensity.ToString());
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("rimLight", Namespace = "")]
public class RimLight
{
    [XmlAttribute("red", Namespace = "")]
    public byte Red { get; set; }

    [XmlAttribute("green", Namespace = "")]
    public byte Green { get; set; }

    [XmlAttribute("blue", Namespace = "")]
    public byte Blue { get; set; }

    [XmlAttribute("intensity", Namespace = "")]
    public float Intensity { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Rim Light Color: " + Red + ", " + Green + ", " + Blue);
        builder.AppendLine("Rim Light Intensity: " + Intensity.ToString());
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("accentLight", Namespace = "")]
public class AccentLight
{
    [XmlAttribute("red", Namespace = "")]
    public byte Red { get; set; }

    [XmlAttribute("green", Namespace = "")]
    public byte Green { get; set; }

    [XmlAttribute("blue", Namespace = "")]
    public byte Blue { get; set; }

    [XmlAttribute("intensity", Namespace = "")]
    public float Intensity { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Accent Light Color: " + Red + ", " + Green + ", " + Blue);
        builder.AppendLine("Accent Light Intensity: " + Intensity.ToString());
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("ambientLightColor", Namespace = "")]
public class AmbientLightColor
{
    [XmlAttribute("red", Namespace = "")]
    public byte Red { get; set; }

    [XmlAttribute("green", Namespace = "")]
    public byte Green { get; set; }

    [XmlAttribute("blue", Namespace = "")]
    public byte Blue { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Ambient Light Color: " + Red + ", " + Green + ", " + Blue);
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("fogColor", Namespace = "")]
public class FogColor
{
    [XmlAttribute("red", Namespace = "")]
    public byte Red { get; set; }

    [XmlAttribute("green", Namespace = "")]
    public byte Green { get; set; }

    [XmlAttribute("blue", Namespace = "")]
    public byte Blue { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Fog Color: " + Red + ", " + Green + ", " + Blue);
        return builder.ToString();
    }
}

[Serializable]
[XmlRoot("gradientColor", Namespace = "")]
public class GradientColor
{
    [XmlAttribute("red", Namespace = "")]
    public byte Red { get; set; }

    [XmlAttribute("green", Namespace = "")]
    public byte Green { get; set; }

    [XmlAttribute("blue", Namespace = "")]
    public byte Blue { get; set; }

    [XmlAttribute("alpah", Namespace = "")]
    public byte Alpah { get; set; }

    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.AppendLine("Gradient Color: " + Red + ", " + Green + ", " + Blue + ", " + Alpah);
        return builder.ToString();
    }
}
