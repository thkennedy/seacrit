﻿using UnityEngine;
using System.Collections;

public class CustomMousePointer : MonoBehaviour {

	public Texture2D cursor;

	public bool showCursor = true;

	// Use this for initialization
	void Start () {
		if (showCursor)
		{
			Screen.showCursor = false; // hide system cursor
		}
	}
	
	void OnGUI()
	{
		if (showCursor)
		{
			GUI.DrawTexture( new Rect(Input.mousePosition.x - cursor.width/2+1, Screen.height - Input.mousePosition.y - cursor.height /2 +1, cursor.width, cursor.height),cursor);
		}
	}
}

