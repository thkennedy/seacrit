﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    enum SpwanTypes
    {
        Common,
        Rare
    }

    private SpwanTypes _spwanType;

    public GameObject Enemy;

    public float MaxFishInScene = 50;
    private const float MaxGroupsInScene = 10;
    private Areas _areasData;
    private int _currentArea;
    private int _currentSpwanList;
    private GameObject _player;
    private CameraScript cam;
    private float _spwanWidth;
    private float _spwanHeight;
	private float _spwanRate = 0.15f;
	public float SpawnTimer = 0f;
    public float SpwanListTimer = 0f;
    private float _spwanListTimeWait;
    public int ActiveFishCount;
	private List<PoolResource> _fishHandles; //handles for ACTIVE resources.
	public bool SpawnGroups = true; //Should spawner spawn groups? this var should be moved to GLOBALDATA!
	private int _activeGroupCount = 0;
	private float _spawnerFrustumMod = 1.5f;
	private float _frustumsToVanish = 1.8f;
	private static Spawner _instance; //needed for static stuff!!
	
    private bool _startSpwaning = true;

	void Awake()
    	{
    		_instance = this; // A Pseudo Singelton trick...
    	}

	// Use this for initialization
	void Start ()
		{
			var gd = GameObject.Find("Managers").GetComponent<GlobalData>();
			_areasData = Helper.Deserialize<Areas>(gd.XMLSpawner);
            Debug.Log(_areasData.ToString());
    		//_player = GlobalData.player;
			cam = GameObject.Find("Main Camera").GetComponent<CameraScript>();
    		_fishHandles = new List<PoolResource>();

			//create player
			
			_player = FishBuilder.CreatePlayer();
			_player.transform.position = GameObject.Find("PlayerStart").transform.position;
    	}

	void FixedUpdate ()
    	{
			if (_startSpwaning)
            {
				SpawnTimer += Time.deltaTime;
                SpwanListTimer += Time.deltaTime;

                if (SpawnTimer >= _spwanRate)
                {
                    SpawnTimer = 0f;
                    UpdateWorldStatus();
                    FishStatusUpdate();
                    // randomize between single or group
					if (Random.value < MaxFishInScene/(MaxGroupsInScene + MaxFishInScene) &&
                       	ActiveFishCount < MaxFishInScene)
                    {
                        SpwanSingleFish(Movement.Zplane.Normal);
                       // SpwanSingleFish(Movement.Zplane.Back);
                    }
                    else
                    {
                        //SpwanFishGroup();
                    }
                }

                if (SpwanListTimer >= _spwanListTimeWait)
                {
                    Debug.Log("Spwan list time has passed, getting new list...");
                    SpwanListTimer = 0f;
                    OnSpwanListUpdate();
                }
            }
    	}

    void UpdateWorldStatus()
        {
			_spwanWidth = cam.frustumWidth / 2 * _spawnerFrustumMod;
            _spwanHeight = cam.frustumHeight / 2 * _spawnerFrustumMod;
        }

    public void UpdateCurrentArea(int areaIndex)
        {
            _currentArea = areaIndex;
            UpdateWorldStatus();
        }

    #region Cleanup
    void BasicKillRequest(PoolResource handle)
	{
		//In-spawner function to kill-off a given handle
		 
		if(handle.isPartOfGroup){_activeGroupCount--;}
		else{ActiveFishCount--;}
		
		_fishHandles.Remove(handle);
        PoolManager.PoolDestroy(handle.gameObject);
	}
 
    public static void GotEaten(PoolResource p)
	{
		//remove dead/eaten fish from spawner
		if(_instance._fishHandles.Contains(p))
		{
			_instance._fishHandles.Remove(p);
		}
		else
		{
			Debug.LogError("trying to remove a fish from spawner. spawner doesnt contain such fish!");
		}
		_instance.BasicKillRequest(p);
	}
 
    void FishStatusUpdate()
    {
		var removeList = new List<PoolResource>();
		foreach(var handle in _fishHandles)
		{
			float dist = Vector3.Distance(_player.transform.position, handle.gameObject.transform.position);
			//the max dist a fish is allowed to wander off. based on a preset number of frustums, and level of interest:
            float maxDist = cam.frustumWidth * _frustumsToVanish;
			
			if(dist>maxDist){
				//if(GlobalData.drawDebugRays) Debug.DrawRay(_player.transform.position,handle.position - _player.transform.position,Color.gray,3f);
				removeList.Add(handle);
                //Debug.LogError("removing " + handle.gameObject.name);
			}
			else
			{
				/* Draw group cues for debug purposes
				 * NOT IMPLEMENTED YET
				*/
			}
		}
		
		//the actual removal of far away fish
		foreach(PoolResource h in removeList){
			BasicKillRequest(h);
		}
	}
    #endregion
    
    # region Spawning
    
    public void OnSpwanListUpdate()
    {
        // when entering a zone or when a list time is up, decide which spwan list will be used
        // this part is vary hard coded, think of a better way to do it
        float percent = 0;
        float commonSpwansProbability = _areasData.TheAreas[_currentArea].CommonSpwansList.CommonSpwansProbability;
        float rareSpwansProbability = _areasData.TheAreas[_currentArea].RareSpwansList.RareSpwansProbability;
        SpwanList[] spwanList = null;

        // add the percent of each spwan list type
        percent += commonSpwansProbability;
        percent += rareSpwansProbability;
        
        var randomSpwanListType = Random.Range(0, percent);
        _spwanType = randomSpwanListType <= rareSpwansProbability ? SpwanTypes.Rare : SpwanTypes.Common;
        
        Debug.Log("Spwan type is: " + _spwanType);
        
        _currentSpwanList = GetCurrentZoneList();

        switch (_spwanType)
        {
            case SpwanTypes.Common:
                spwanList = _areasData.TheAreas[_currentArea].CommonSpwansList.SpwanListContent;
                break;
            case SpwanTypes.Rare:
                spwanList = _areasData.TheAreas[_currentArea].RareSpwansList.SpwanListContent;
                break;
        }
        _spwanListTimeWait = spwanList[_currentSpwanList].LastingTime;
        _spwanRate = spwanList[_currentSpwanList].ListSpawnRate;

        _startSpwaning = true;
    }
    
    private int GetCurrentZoneList()
    {
        var percent = new List<float>();
        var spwanIndex = 0;
        SpwanList[] spwanList;

        switch (_spwanType)
        {
            case SpwanTypes.Common:
                // add the percent of each spwan list to a list
                spwanList = _areasData.TheAreas[_currentArea].CommonSpwansList.SpwanListContent;
                for (int i = 0; i < spwanList.Length; i++)
                {
                    // if first spwan list
                    if (i == 0)
                    {
                        percent.Add(spwanList[i].SpwanListProbability);
                    }
                    else
                    {
                        percent.Add(spwanList[i].SpwanListProbability + percent[i - 1]);
                    }
                }
            break;
            case  SpwanTypes.Rare:
                // add the percent of each spwan list to a list
                spwanList = _areasData.TheAreas[_currentArea].RareSpwansList.SpwanListContent;
                for (int i = 0; i < spwanList.Length; i++)
                {
                    // if first spwan list
                    if (i == 0)
                    {
                        percent.Add(spwanList[i].SpwanListProbability);
                    }
                    else
                    {
                        percent.Add(spwanList[i].SpwanListProbability + percent[i - 1]);
                    }
                }
            break;
        }

        var randomSpwanList = Random.Range(0, percent[percent.Count - 1]);

        // decide wich spwan list to use
        for (int i = 0; i < percent.Count; i++)
        {
            if (i == 0)
            {
                if (randomSpwanList < percent[i])
                {
                    spwanIndex = i;
                }
            }
            else
            {
                if (randomSpwanList < percent[i] && randomSpwanList > percent[i - 1])
                {
                    spwanIndex = i;
                }
            }
        }

        return spwanIndex;
    }
    
    private void SpwanSingleFish(Movement.Zplane zPlane)
    {
        var shouldSpwan = false;
        FishData[] fish = null;
        switch (_spwanType)
        {
                case SpwanTypes.Common:
                    fish = _areasData.TheAreas[_currentArea].CommonSpwansList.SpwanListContent[_currentSpwanList].Fishs;
                break;
                case SpwanTypes.Rare:
                    fish = _areasData.TheAreas[_currentArea].RareSpwansList.SpwanListContent[_currentSpwanList].Fishs;
                break;
        }
		var percent = new List<float>();
        var fishToSpwanIndex = 0;
        // add the percent of each fish to a list
        for (int i = 0; i < fish.Length; i++)
        {
            // if first fish
            if (i == 0)
            {
                percent.Add(fish[i].SingleProbability);
            }
            else
            {
                percent.Add(fish[i].SingleProbability + percent[i - 1]);
            }
        }

        var randomFish = Random.Range(0, percent[percent.Count - 1]);
        
        // decide wich fish to spwan
        for (int i = 0; i < percent.Count; i++)
        {
            if (i == 0)
            {
                if (randomFish < percent[i])
                {
                    fishToSpwanIndex = i;
                }
            }
            else
            {
                if (randomFish < percent[i] && randomFish > percent[i - 1])
                {
                    fishToSpwanIndex = i;
                }
            }
        }

        // get fish name by fishToSpwanIndex
        var fishName = fish[fishToSpwanIndex].FishName;

        // chose wich side of screen to spwan
        var enemyPos = Vector3.zero;
		
		var r = Random.value;
		//deciding probabilities based on player movement
		Vector3 vel = _player.rigidbody.velocity;
		//calculate probs
		float minP = 0.15f, maxP = 0.35f;
		float[] rngR = {0f, vel.x>0?maxP:minP};
		float[] rngL = {rngR[1], rngR[1] + (vel.x<0?maxP:minP)};
		float[] rngT = {rngL[1], rngL[1] + (vel.y>0?maxP:minP)};
		float[] rngB = {rngT[1], rngT[1] + (vel.x<0?maxP:minP)};

        enemyPos = PickRandomSpwanPos(rngR[0], rngR[1], rngL[0], rngL[1], rngT[0], rngT[1], rngB[0], rngB[1], r);

        // try to get a proper spwan point 10 times, if can't, don't spwan
        for (var i = 0; i < 10; i++)
        {
            if (!shouldSpwan)
            {
                if (!Helper.isValidSpawnPoint(enemyPos, fish[fishToSpwanIndex].SizeMultiplier) || enemyPos.y > 10)
                {
                    enemyPos = PickRandomSpwanPos(rngR[0], rngR[1], rngL[0], rngL[1], rngT[0], rngT[1], rngB[0], rngB[1], r);
                }
                else
                {
                    shouldSpwan = true;
                }
            }
        }

        if (shouldSpwan)
        {
            var handle = PoolManager.PoolCreate(fishName, enemyPos, Quaternion.identity, zPlane);
            ActiveFishCount++;
            _fishHandles.Add(handle);
        }
    }
    
    private Vector3 PickRandomSpwanPos(float R1, float R2, float L1, float L2, float T1, float T2, float B1, float B2, float random)
    {
        var enemyPos = Vector3.zero;

        // right
        if (random >= R1 && random <= R2)
        {
            enemyPos = _player.transform.position + new Vector3(_spwanWidth, Random.Range(-_spwanHeight, _spwanHeight), 0);
        }
        // left
        if (random > L1 && random <= L2)
        {
            enemyPos = _player.transform.position - new Vector3(_spwanWidth, Random.Range(-_spwanHeight, _spwanHeight), 0);
        }
        // top
        if (random > T1 && random <= T2)
        {
            enemyPos = _player.transform.position + new Vector3(Random.Range(-_spwanWidth, _spwanWidth), _spwanHeight, 0);
        }
        // bottom
        if (random > B1 && random <= B2)
        {
            enemyPos = _player.transform.position - new Vector3(Random.Range(-_spwanWidth, _spwanWidth), _spwanHeight, 0);
        }

        return enemyPos;
    }
    
    private void SpwanFishGroup()
    {
        //var maxEnemy = _areasData.TheAreas[0].Fishs[0].MaximumGroup;
        //var enemyPos = _player.transform.position + new Vector3(_spwanWidth, Random.Range(0, _spwanHeight), 0);
        //if (_enemyCounter < maxEnemy)
        //{
        //    // TODO: use Asset Manager
        //    Instantiate(Enemy, enemyPos, Quaternion.identity);
        //    _enemyCounter++;
        //}
    }
    #endregion

}