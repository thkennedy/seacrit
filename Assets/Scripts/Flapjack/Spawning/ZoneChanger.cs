﻿using UnityEngine;
using System.Collections;

public class ZoneChanger : MonoBehaviour {

    public enum ZoneNames
    {
        Shallow1 = 0,
        Shallow2,
        Shallow3,
		Allfish,
		Test,
    }

    public ZoneNames Zone;

    private Zones _zoneData;
    private Zone _currentZone;

    public Spawner Spawner;

    public Light FillLight;
    public Color FillLightColor;
    public float FillLightIntensity;

    public Light RimLight;
    public Color RimLightColor;
    public float RimLightIntensity;

    public Light AccentLight;
    public Color AccentLightColor;
    public float AccentLightIntensity;

    public Color AmbientLight;
    public Color FogColor;

    public float Smooth;

	// Use this for initialization
	void Start ()
	{
		var gd = GameObject.Find("Managers").GetComponent<GlobalData>();
		_zoneData = Helper.Deserialize<Zones>(gd.XMLZoneData);
        Debug.Log(_zoneData.ToString());

	    UpdateCurrentZone();
	}
	
    void OnTriggerEnter(Collider fish)
    {
    	    if (fish.transform.root.tag != "Player") { return;}
    	    PlayerFish pf = Helper.GetPlayerFish(fish);
    	    if (pf ==null ) { return;}
        
    	    // otherwise it is the player
		// make sure we use the correct zone
		if (pf.currentZone == _zoneData.TheZones[(int)Zone] ){ return; } // don't update
		
		UpdateCurrentZone();
		pf.currentZone = _currentZone;
		// change music
		GlobalData.SoundMan.changeZones((int)Zone);
		// change spwan list
		Spawner.UpdateCurrentArea((int)Zone);
		Spawner.OnSpwanListUpdate();
		
		Debug.Log("player entered zone: " + Zone.ToString());
        
    }

    void OnTriggerStay(Collider fish)
    {
        if (fish.transform.root.tag == "Player")
        {
            // update everything
            UpdateFillLight();
            UpdateRimLight();
            UpdateAccentLight();
            UpdateAmbientLight();
            UpdateFogColor();
        }
    }

    private void UpdateCurrentZone()
    {
        // setup current zone for easy acceses
        _currentZone = _zoneData.TheZones[(int)Zone];
    }

    #region OLD XML CODE
    //private void UpdateFillLight()
    //{
    //    var theZoneFillLight = _currentZone.ZoneFillLight;
    //    var color = new Color(theZoneFillLight.Red/255.0f, theZoneFillLight.Green/255.0f, theZoneFillLight.Blue/255.0f, 1.0f);
    //    FillLight.color = Color.Lerp(FillLight.color, color, Smooth*Time.deltaTime);
    //    FillLight.intensity = Mathf.Lerp(FillLight.intensity, theZoneFillLight.Intensity, Smooth * Time.deltaTime);
    //}
    //
    //private void UpdateRimLight()
    //{
    //    var theZoneRimLight = _currentZone.ZoneRimLight;
    //    var color = new Color(theZoneRimLight.Red/255.0f, theZoneRimLight.Green/255.0f, theZoneRimLight.Blue/255.0f, 1.0f);
    //    RimLight.color = Color.Lerp(RimLight.color, color, Smooth * Time.deltaTime);
    //    RimLight.intensity = Mathf.Lerp(RimLight.intensity, theZoneRimLight.Intensity, Smooth * Time.deltaTime);
    //}
    //
    //private void UpdateAccentLight()
    //{
    //    var theZoneAccentLight = _currentZone.ZoneAccentLight;
    //    var color = new Color(theZoneAccentLight.Red/255.0f, theZoneAccentLight.Green/255.0f, theZoneAccentLight.Blue/255.0f, 255);
    //    AccentLight.color = Color.Lerp(AccentLight.color, color, Smooth * Time.deltaTime);
    //    AccentLight.intensity = Mathf.Lerp(AccentLight.intensity, theZoneAccentLight.Intensity, Smooth * Time.deltaTime);
    //
    //}
    //
    //private void UpdateAmbientLight()
    //{
    //	    var theZoneAmbientLight = _currentZone.ZoneAmbientLightColor;
    //	    var color = new Color(theZoneAmbientLight.Red/255.0f, theZoneAmbientLight.Green/255.0f, theZoneAmbientLight.Blue/255.0f, 1.0f);
    //	    RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, color, Smooth * Time.deltaTime);
    //}
    //
    //private void UpdateFogColor()
    //{
    //	    var theZoneFogColor = _currentZone.ZoneFogColor;
    //	    var color = new Color(theZoneFogColor.Red/255.0f, theZoneFogColor.Green/255.0f, theZoneFogColor.Blue/255.0f,1.0f);
    //	    RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor, color, Smooth * Time.deltaTime);
    //}
    #endregion

    private void UpdateFillLight()
    {
        FillLight.color = Color.Lerp(FillLight.color, FillLightColor, Smooth * Time.deltaTime);
        FillLight.intensity = Mathf.Lerp(FillLight.intensity, FillLightIntensity, Smooth * Time.deltaTime);
    }

    private void UpdateRimLight()
    {
        RimLight.color = Color.Lerp(RimLight.color, RimLightColor, Smooth * Time.deltaTime);
        RimLight.intensity = Mathf.Lerp(RimLight.intensity, RimLightIntensity, Smooth * Time.deltaTime);
    }

    private void UpdateAccentLight()
    {
        AccentLight.color = Color.Lerp(AccentLight.color, AccentLightColor, Smooth * Time.deltaTime);
        AccentLight.intensity = Mathf.Lerp(AccentLight.intensity, AccentLightIntensity, Smooth * Time.deltaTime);
    }

    private void UpdateAmbientLight()
    {
        RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, AmbientLight, Smooth * Time.deltaTime);
    }

    private void UpdateFogColor()
    {
        RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor, FogColor, Smooth * Time.deltaTime);
    }
}
