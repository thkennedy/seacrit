﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour
{
    public StartMenu.MenuActions Action;
    private StartMenu _menu;

    void Start()
    {
        _menu = GameObject.Find("UI Root (2D)").GetComponent<StartMenu>();
    }

    void OnClick()
    {
        _menu.MenuAction(Action);
    }
}
