﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour
{
    public enum MenuActions
    {
        StartGame,
        About,
        Credits,
        HelpUs,
        Exit,
        Back
    }

    public GameObject MainMenu;
    public GameObject About;
    public GameObject Credits;
    public GameObject HelpUs;
    public GameObject Back;

    void Awake()
    {
        HideAll();
        MainMenu.SetActive(true);
    }

    private void HideAll()
    {
        MainMenu.SetActive(false);
        About.SetActive(false);
        Credits.SetActive(false);
        HelpUs.SetActive(false);
        Back.SetActive(false);
    }

    public void MenuAction(MenuActions action)
    {
        switch (action)
        {
                case MenuActions.StartGame:
                    StartGame();
                break;
                case MenuActions.About:
                    HideAll();
                    About.SetActive(true);
                    Back.SetActive(true);
                break;
                case MenuActions.Credits:
                    HideAll();
                    Credits.SetActive(true);
                    Back.SetActive(true);
                break;
                case MenuActions.HelpUs:
                    HideAll();
                    HelpUs.SetActive(true);
                    Back.SetActive(true);
                break;
                case MenuActions.Exit:
                    ExitGame();
                break;
                case MenuActions.Back:
                    HideAll();
                    MainMenu.SetActive(true);
                break;
        }
    }

    private void StartGame()
    {
        Debug.Log("Starting game...");
        Application.LoadLevel(1);
    }

    private void ShowAbout()
    {
        Debug.Log("Showing about window...");
    }

    private void ShowCredits()
    {
        Debug.Log("Showing credits window...");
    }

    private void ShowHelpUs()
    {
        Debug.Log("Showing help us window...");
    }

    private void ExitGame()
    {
        Debug.Log("Closing game...");
        Application.Quit();
    }
}