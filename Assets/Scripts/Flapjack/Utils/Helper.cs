using System;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public static class Helper
{
    public static T Deserialize<T>(string path)
    {
            var serializer = new XmlSerializer(typeof(T));
            using(var reader = XmlReader.Create(path, new XmlReaderSettings
            {
            	CheckCharacters = false,
            	ConformanceLevel = ConformanceLevel.Auto,
            	IgnoreComments = true,
            	IgnoreWhitespace = true,
             	ValidationFlags =
             	System.Xml.Schema.XmlSchemaValidationFlags.None,
            	ValidationType = ValidationType.None,
        	}))
        {
            var xmlData = (T) serializer.Deserialize(reader);
            reader.Close();
        	return xmlData;
    	}
    }
	public static T Deserialize<T>(TextAsset ta)
	{
		var serializer = new XmlSerializer(typeof(T));
		using(var reader = new System.IO.StringReader(ta.text) )
		{
			var xmlData = (T) serializer.Deserialize(reader);
			reader.Close();
			return xmlData;
		}
	}

    public static bool isValidSpawnPoint(Vector3 pt, float radius = 0.2f) // check if this point is a valid place to spawn a fish. 
	{
		float d = 10f;	// distance to cast in from. If this is too small we will miss the collision with the seafloor.
		if ( Physics.Raycast(pt + new Vector3(0f, 0f, -d),new Vector3(0f ,0f, 1), d,  1<< LayerMask.NameToLayer("SceneGeometry")) ) { return false;} // collision with geometry in z direction

		// check for enough room around this point
		return !Physics.CheckSphere(pt, radius, 1<< LayerMask.NameToLayer("SceneGeometry"));
	}
	    
	public static void SetCollidersEnable(GameObject go, bool e)
	{
       	foreach (Collider c in go.GetComponentsInChildren<Collider>(true))
       	{
       		c.enabled = e;
    	}
    }
         
    public static Fish GetFish(Component c)	// TODO - generic function?
    { 	
		// searches up the tree for a Fish component, or returns null if none
		Transform t = c.transform;
		Fish f = null;
		while (t!=null && f ==null)
		{
			f = t.GetComponent<Fish>();
			t = t.parent;
		}
		return f;
	}
	public static PlayerFish GetPlayerFish(Component c)
    { 	
		// searches up the tree for a Fish component, or returns null if none
		Transform t = c.transform;
		PlayerFish f = null;
		while (t!=null && f ==null)
		{
			f = t.GetComponent<PlayerFish>();
			t = t.parent;
		}
		return f;
	}
	    
	public static T GetAncestor<T>(Component c) where T : Component
    { 	
		// searches up the tree for a component of type T, or returns null if none
        // Includes any component that are directly attached to c.transform
		Transform tr = c.transform;
		T x = null;
		while (tr!=null && x ==null)
		{
			x = tr.GetComponent<T>();
			tr = tr.parent;
		}
		return x;
	}
	    
	public static void ChangeLayersRecursively(this Transform trans, string name)
	{
		trans.gameObject.layer = LayerMask.NameToLayer(name);
		foreach(Transform child in trans)
		{       
			child.ChangeLayersRecursively(name);
		}
	}  
}