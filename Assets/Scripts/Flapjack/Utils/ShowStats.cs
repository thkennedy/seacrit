﻿using UnityEngine;
using System.Collections;

public class ShowStats : MonoBehaviour {

	// UPDATE VARS
	public float UpdateSpeed=1f;
	private float UpdateCounter;
	
	// FPS
	public float FPSResolution=0.1f;
	private float lastFPS;
	private float smoothedFPS;
	
	// DISPLAY
	private string _displayMsg;
	
	// misc
	private Spawner spawn;
	private PlayerFish playerfish;
	
	// Use this for initialization
	void Start () {
		UpdateCounter = UpdateSpeed;
		spawn = GameObject.Find("Managers").GetComponent<Spawner>();
		playerfish = GameObject.Find("PlayerFish").GetComponent<PlayerFish>();
	}
	
	// Update is called once per frame
	void Update () {
		lastFPS = 1.0f / Time.deltaTime;
		if ( !float.IsNaN(lastFPS))
		{
			smoothedFPS = FPSResolution * lastFPS + (1f - FPSResolution) * smoothedFPS;
		}
		UpdateCounter -=Time.deltaTime;
		if (UpdateCounter < 0f)
		{
			UpdateCounter = UpdateSpeed;
			_displayMsg  =  "FPS : "+smoothedFPS.ToString("000");
			_displayMsg += "\nActive Fish : " + spawn.ActiveFishCount.ToString("000");
			_displayMsg += "\n\nPlayer Size: " + playerfish.GetSize().ToString("0.00") + " / " + playerfish.FishWeightMax.ToString("0.00") ;
		}
		
	}
	void OnGUI()
	{	
		GUI.Label(new Rect(0,0,Screen.width,Screen.height), _displayMsg );
		
	}
	
}
