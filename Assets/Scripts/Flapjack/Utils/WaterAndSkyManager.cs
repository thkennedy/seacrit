﻿using UnityEngine;
using System.Collections;
    public class WaterAndSkyManager : MonoBehaviour
    {
        //general variables
        public Transform player;
        
		//wave variables
        public GameObject wave;
        public GameObject bone1;
        public GameObject bone2;
        public float basicSwayStrength = 10f;
        public float basicSwayFrequency = 2f;
        Vector3 start1;
        Vector3 start2;
        public bool chaoticSway = false;
        public float cSwayMod = 2;
        float sideMod;
        float sideModV;
        float sideModTime;
       
		//sky variables
        public GameObject sky;
		
		//sun
		public GameObject sun;

		//caustics
		public GameObject causticsLight;
	
        #region initialization
        public void Initialise(GameObject p)
        {
            start1 = new Vector3(bone1.transform.localPosition.x,bone1.transform.localPosition.y,bone1.transform.localPosition.z);
            start2 = new Vector3(bone2.transform.localPosition.x,bone2.transform.localPosition.y,bone2.transform.localPosition.z);
			player = p.transform;
        }

        #endregion
	
        //update function
        void Update ()
        {
        	   if (player!=null)
        	   {
			  //water swaying
			  if(chaoticSway) cSwayMod = 2;
			  else 			cSwayMod = 1;	
			
			  sideModTime += Time.deltaTime;
			  if(sideModTime>(1.5/cSwayMod))
			  {
				 sideModTime = 0;
				 sideMod = Random.Range(-0.04f,0.04f);
			  }
			
			  bone1.transform.localPosition = new Vector3 (start1.x + Mathf.Sin(Time.time * basicSwayFrequency) * basicSwayStrength + Mathf.Sin(Time.time * basicSwayFrequency/2) * (basicSwayStrength)*cSwayMod, start1.y, start1.z + Mathf.SmoothDamp(bone1.transform.localPosition.z,sideMod,ref sideModV,0.5f));
			  bone2.transform.localPosition = new Vector3 (start2.x + Mathf.Sin(Time.time * basicSwayFrequency) *-1* basicSwayStrength + Mathf.Sin(Time.time * basicSwayFrequency/2) * (basicSwayStrength)*cSwayMod, start2.y, start2.z + Mathf.SmoothDamp(bone2.transform.localPosition.z,sideMod,ref sideModV,0.5f)*-1);
				
			  //positioning
			  wave.transform.position = new Vector3(player.position.x,wave.transform.position.y,wave.transform.position.z);
			  sky.transform.position = new Vector3(player.position.x,sky.transform.position.y,sky.transform.position.z);
				sun.transform.position = new Vector3(player.position.x - 2,sun.transform.position.y,sun.transform.position.z);
	
				//caustics
				causticsLight.transform.position = new Vector3(player.transform.position.x,transform.position.y,-5);
				
				//make the light ignore the player if they're above water
				if(player.transform.position.y>0){} //light.cullingMask = 1;
				//else light.cullingMask = 1;
		   }
        }
    }