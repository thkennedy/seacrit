// Shader created with Shader Forge Beta 0.17 
// Shader Forge (c) Joachim 'Acegikmo' Holmer
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.17;sub:START;pass:START;ps:lgpr:1,nrmq:1,limd:1,blpr:2,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:False,uamb:True,mssp:True,ufog:False,aust:False,igpj:True,qofs:0,lico:1,qpre:3,flbk:,rntp:2,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.2616124,fgcb:0.5661765,fgca:1,fgde:0.01,fgrn:3.5,fgrf:6.7;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-19-OUT;n:type:ShaderForge.SFN_Rotator,id:3,x:32935,y:32495|UVIN-10-UVOUT,ANG-18-OUT;n:type:ShaderForge.SFN_Tex2d,id:4,x:33311,y:33040,tex:7217395c117871c44877f4373e5bdb24,ntxv:0,isnm:False|UVIN-9-UVOUT,TEX-13-TEX;n:type:ShaderForge.SFN_Time,id:5,x:33490,y:32667;n:type:ShaderForge.SFN_Vector1,id:7,x:33467,y:32575,v1:-1;n:type:ShaderForge.SFN_Multiply,id:8,x:33288,y:32531|A-5-T,B-7-OUT;n:type:ShaderForge.SFN_Rotator,id:9,x:32948,y:32645|UVIN-10-UVOUT,ANG-16-OUT;n:type:ShaderForge.SFN_TexCoord,id:10,x:33678,y:32436,uv:0;n:type:ShaderForge.SFN_Tex2dAsset,id:13,x:33708,y:33072,ptlb:node_13,tex:7217395c117871c44877f4373e5bdb24;n:type:ShaderForge.SFN_Tex2d,id:14,x:33311,y:32915,tex:7217395c117871c44877f4373e5bdb24,ntxv:0,isnm:False|UVIN-3-UVOUT,TEX-13-TEX;n:type:ShaderForge.SFN_Add,id:15,x:33025,y:32911|A-14-RGB,B-4-RGB;n:type:ShaderForge.SFN_Multiply,id:16,x:33107,y:32689|A-17-OUT,B-5-T;n:type:ShaderForge.SFN_ValueProperty,id:17,x:33288,y:32775,ptlb:Spin Speed,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:18,x:33107,y:32497|A-17-OUT,B-8-OUT;n:type:ShaderForge.SFN_Multiply,id:19,x:32956,y:33089|A-23-OUT,B-20-OUT;n:type:ShaderForge.SFN_ValueProperty,id:20,x:33076,y:33302,ptlb:Shine Strength,v1:1;n:type:ShaderForge.SFN_Power,id:23,x:33151,y:33142|VAL-15-OUT,EXP-29-OUT;n:type:ShaderForge.SFN_ValueProperty,id:29,x:33362,y:33284,ptlb:Shine Power,v1:1;n:type:ShaderForge.SFN_Vector2,id:30,x:33678,y:32654,v1:1,v2:-1;proporder:13-17-20-29;pass:END;sub:END;*/

Shader "Shader Forge/DualRotateGlow" {
    Properties {
        _node13 ("node_13", 2D) = "white" {}
        _SpinSpeed ("Spin Speed", Float ) = 0.1
        _ShineStrength ("Shine Strength", Float ) = 1
        _ShinePower ("Shine Power", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node13; uniform float4 _node13_ST;
            uniform float _SpinSpeed;
            uniform float _ShineStrength;
            uniform float _ShinePower;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_17 = _SpinSpeed;
                float4 node_5 = _Time + _TimeEditor;
                float node_3_ang = (node_17*(node_5.g*(-1.0)));
                float node_3_spd = 1.0;
                float node_3_cos = cos(node_3_spd*node_3_ang);
                float node_3_sin = sin(node_3_spd*node_3_ang);
                float2 node_3_piv = float2(0.5,0.5);
                float2 node_10 = i.uv0;
                float2 node_3 = (mul(node_10.rg-node_3_piv,float2x2( node_3_cos, -node_3_sin, node_3_sin, node_3_cos))+node_3_piv);
                float node_9_ang = (node_17*node_5.g);
                float node_9_spd = 1.0;
                float node_9_cos = cos(node_9_spd*node_9_ang);
                float node_9_sin = sin(node_9_spd*node_9_ang);
                float2 node_9_piv = float2(0.5,0.5);
                float2 node_9 = (mul(node_10.rg-node_9_piv,float2x2( node_9_cos, -node_9_sin, node_9_sin, node_9_cos))+node_9_piv);
                float3 emissive = (pow((tex2D(_node13,TRANSFORM_TEX(node_3, _node13)).rgb+tex2D(_node13,TRANSFORM_TEX(node_9, _node13)).rgb),_ShinePower)*_ShineStrength);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
