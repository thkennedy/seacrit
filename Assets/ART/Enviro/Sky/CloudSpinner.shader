// Shader created with Shader Forge Beta 0.17 
// Shader Forge (c) Joachim 'Acegikmo' Holmer
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.17;sub:START;pass:START;ps:lgpr:1,nrmq:1,limd:1,blpr:3,bsrc:0,bdst:6,culm:0,dpts:2,wrdp:False,uamb:True,mssp:True,ufog:False,aust:True,igpj:True,qofs:0,lico:1,qpre:3,flbk:,rntp:2,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.2616124,fgcb:0.5661765,fgca:1,fgde:0.01,fgrn:3.5,fgrf:6.7;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-9-OUT;n:type:ShaderForge.SFN_Rotator,id:3,x:33281,y:32808|UVIN-8-UVOUT,ANG-6-OUT;n:type:ShaderForge.SFN_Tex2d,id:4,x:33095,y:32818,ptlb:node_4,tex:05fb4a4e150b29d4c91252edffdcadea,ntxv:0,isnm:False|UVIN-3-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:5,x:33706,y:32819,ptlb:Spin Speed,v1:1;n:type:ShaderForge.SFN_Multiply,id:6,x:33528,y:32786|A-7-T,B-5-OUT;n:type:ShaderForge.SFN_Time,id:7,x:33706,y:32664;n:type:ShaderForge.SFN_TexCoord,id:8,x:33528,y:32616,uv:0;n:type:ShaderForge.SFN_Multiply,id:9,x:33044,y:33052|A-11-OUT,B-10-OUT;n:type:ShaderForge.SFN_ValueProperty,id:10,x:33205,y:33201,ptlb:Multiplier,v1:1;n:type:ShaderForge.SFN_Power,id:11,x:33217,y:33004|VAL-4-RGB,EXP-15-OUT;n:type:ShaderForge.SFN_ValueProperty,id:12,x:33580,y:33045,ptlb:Power,v1:1;n:type:ShaderForge.SFN_Sin,id:14,x:33675,y:33178|IN-17-OUT;n:type:ShaderForge.SFN_Add,id:15,x:33360,y:33083|A-12-OUT,B-16-OUT;n:type:ShaderForge.SFN_Multiply,id:16,x:33514,y:33217|A-14-OUT,B-19-OUT;n:type:ShaderForge.SFN_Multiply,id:17,x:33857,y:33178|A-7-T,B-18-OUT;n:type:ShaderForge.SFN_ValueProperty,id:18,x:34006,y:33267,ptlb:Fuzz Speed,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:19,x:33726,y:33360,ptlb:Fuzz Ammount,v1:1;proporder:4-5-10-12-18-19;pass:END;sub:END;*/

Shader "Shader Forge/CloudSpinner" {
    Properties {
        _node4 ("node_4", 2D) = "white" {}
        _SpinSpeed ("Spin Speed", Float ) = 1
        _Multiplier ("Multiplier", Float ) = 1
        _Power ("Power", Float ) = 1
        _FuzzSpeed ("Fuzz Speed", Float ) = 1
        _FuzzAmmount ("Fuzz Ammount", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            ZWrite Off
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node4; uniform float4 _node4_ST;
            uniform float _SpinSpeed;
            uniform float _Multiplier;
            uniform float _Power;
            uniform float _FuzzSpeed;
            uniform float _FuzzAmmount;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_7 = _Time + _TimeEditor;
                float node_3_ang = (node_7.g*_SpinSpeed);
                float node_3_spd = 1.0;
                float node_3_cos = cos(node_3_spd*node_3_ang);
                float node_3_sin = sin(node_3_spd*node_3_ang);
                float2 node_3_piv = float2(0.5,0.5);
                float2 node_3 = (mul(i.uv0.rg-node_3_piv,float2x2( node_3_cos, -node_3_sin, node_3_sin, node_3_cos))+node_3_piv);
                float3 emissive = (pow(tex2D(_node4,TRANSFORM_TEX(node_3, _node4)).rgb,(_Power+(sin((node_7.g*_FuzzSpeed))*_FuzzAmmount)))*_Multiplier);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
