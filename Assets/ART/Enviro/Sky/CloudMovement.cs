﻿using UnityEngine;
using System.Collections;

public class CloudMovement : MonoBehaviour {
	
	public float horizontalSpeed = 1f;
	public float verticalBob = 1f;
	public float verticalBobRate = 1f;
	public float verticalPosition;
	// Use this for initialization
	void Start () {
		verticalPosition = transform.localPosition.y;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (transform.position.x + horizontalSpeed,
			verticalPosition + Mathf.Sin(Time.time * verticalBobRate) * verticalBob,
			transform.position.z);
	}
}
