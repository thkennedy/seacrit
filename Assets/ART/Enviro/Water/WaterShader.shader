// Shader created with Shader Forge Beta 0.28 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.28;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.1776371,fgcb:0.8308824,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:202,x:32719,y:32712|diff-476-OUT,spec-464-OUT,gloss-469-OUT,emission-476-OUT,voffset-335-OUT;n:type:ShaderForge.SFN_Tex2d,id:203,x:33530,y:32649,tex:1ca6f543584d9ac48afa79720d029bc6,ntxv:0,isnm:False|UVIN-408-UVOUT,MIP-363-OUT,TEX-242-TEX;n:type:ShaderForge.SFN_Time,id:207,x:33810,y:32998;n:type:ShaderForge.SFN_Panner,id:226,x:33287,y:33063,spu:1,spv:0|DIST-236-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:231,x:33835,y:33284;n:type:ShaderForge.SFN_ValueProperty,id:235,x:33776,y:33221,ptlb:Time Scalar,ptin:_TimeScalar,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:236,x:33598,y:32998|A-207-T,B-235-OUT;n:type:ShaderForge.SFN_Multiply,id:238,x:33617,y:33316|A-231-X,B-240-OUT;n:type:ShaderForge.SFN_ValueProperty,id:240,x:33794,y:33455,ptlb:Pannar Scalar,ptin:_PannarScalar,glob:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:241,x:33266,y:32779,tex:1ca6f543584d9ac48afa79720d029bc6,ntxv:0,isnm:False|UVIN-226-UVOUT,MIP-363-OUT,TEX-242-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:242,x:33788,y:32736,ptlb:Texture,ptin:_Texture,glob:False,tex:1ca6f543584d9ac48afa79720d029bc6;n:type:ShaderForge.SFN_TexCoord,id:325,x:33877,y:32491,uv:0;n:type:ShaderForge.SFN_NormalVector,id:334,x:33312,y:33345,pt:False;n:type:ShaderForge.SFN_Multiply,id:335,x:32795,y:33237|A-375-OUT,B-334-OUT,C-365-OUT;n:type:ShaderForge.SFN_Vector1,id:363,x:33945,y:32931,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:365,x:33365,y:33590,ptlb:Vertex Offset Scalar,ptin:_VertexOffsetScalar,glob:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:375,x:33019,y:33127|A-241-A,B-387-A;n:type:ShaderForge.SFN_Panner,id:385,x:33266,y:33208,spu:-1,spv:0|DIST-236-OUT;n:type:ShaderForge.SFN_Tex2d,id:387,x:33384,y:32905,tex:1ca6f543584d9ac48afa79720d029bc6,ntxv:0,isnm:False|UVIN-385-UVOUT,TEX-242-TEX;n:type:ShaderForge.SFN_Panner,id:408,x:33627,y:32491,spu:1,spv:0|UVIN-325-UVOUT,DIST-238-OUT;n:type:ShaderForge.SFN_ValueProperty,id:422,x:33493,y:33718,ptlb:Vertex Offset Scalar_copy_copy,ptin:_VertexOffsetScalar_copy_copy,glob:False,v1:0.1;n:type:ShaderForge.SFN_Slider,id:464,x:33101,y:32560,ptlb:Specular,ptin:_Specular,min:0,cur:10.82707,max:30;n:type:ShaderForge.SFN_Slider,id:469,x:33123,y:32670,ptlb:Gloss,ptin:_Gloss,min:0,cur:0.6315789,max:3;n:type:ShaderForge.SFN_Lerp,id:476,x:33014,y:32299|A-203-RGB,B-482-RGB,T-488-OUT;n:type:ShaderForge.SFN_TexCoord,id:477,x:33515,y:32246,uv:0;n:type:ShaderForge.SFN_Color,id:482,x:33337,y:32384,ptlb:Fog Color,ptin:_FogColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Power,id:488,x:33241,y:32205|VAL-477-V,EXP-491-OUT;n:type:ShaderForge.SFN_Vector1,id:491,x:33489,y:32131,v1:6;proporder:242-235-240-365-464-469-482;pass:END;sub:END;*/

Shader "Shader Forge/WaterShader" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _TimeScalar ("Time Scalar", Float ) = 1
        _PannarScalar ("Pannar Scalar", Float ) = 1
        _VertexOffsetScalar ("Vertex Offset Scalar", Float ) = 0.1
        _Specular ("Specular", Range(0, 30)) = 10.82707
        _Gloss ("Gloss", Range(0, 3)) = 0.6315789
        _FogColor ("Fog Color", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform float _PannarScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                float4 node_207 = _Time + _TimeEditor;
                float node_236 = (node_207.g*_TimeScalar);
                float2 node_504 = o.uv0;
                float2 node_226 = (node_504.rg+node_236*float2(1,0));
                float node_363 = 0.0;
                float2 node_385 = (node_504.rg+node_236*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_226, _Texture),0.0,node_363)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_385, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float2 node_408 = (i.uv0.rg+(objPos.r*_PannarScalar)*float2(1,0));
                float node_363 = 0.0;
                float3 node_476 = lerp(tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_408, _Texture),0.0,node_363)).rgb,_FogColor.rgb,pow(i.uv0.g,6.0));
                float3 emissive = node_476;
///////// Gloss:
                float gloss = exp2(_Gloss*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float _Specular_var = _Specular;
                float3 specularColor = float3(_Specular_var,_Specular_var,_Specular_var);
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * node_476;
                finalColor += specular;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform float _PannarScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                float4 node_207 = _Time + _TimeEditor;
                float node_236 = (node_207.g*_TimeScalar);
                float2 node_505 = o.uv0;
                float2 node_226 = (node_505.rg+node_236*float2(1,0));
                float node_363 = 0.0;
                float2 node_385 = (node_505.rg+node_236*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_226, _Texture),0.0,node_363)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_385, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
///////// Gloss:
                float gloss = exp2(_Gloss*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float _Specular_var = _Specular;
                float3 specularColor = float3(_Specular_var,_Specular_var,_Specular_var);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float2 node_408 = (i.uv0.rg+(objPos.r*_PannarScalar)*float2(1,0));
                float node_363 = 0.0;
                float3 node_476 = lerp(tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_408, _Texture),0.0,node_363)).rgb,_FogColor.rgb,pow(i.uv0.g,6.0));
                finalColor += diffuseLight * node_476;
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float4 uv0 : TEXCOORD5;
                float3 normalDir : TEXCOORD6;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                float4 node_207 = _Time + _TimeEditor;
                float node_236 = (node_207.g*_TimeScalar);
                float2 node_506 = o.uv0;
                float2 node_226 = (node_506.rg+node_236*float2(1,0));
                float node_363 = 0.0;
                float2 node_385 = (node_506.rg+node_236*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_226, _Texture),0.0,node_363)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_385, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                float4 node_207 = _Time + _TimeEditor;
                float node_236 = (node_207.g*_TimeScalar);
                float2 node_507 = o.uv0;
                float2 node_226 = (node_507.rg+node_236*float2(1,0));
                float node_363 = 0.0;
                float2 node_385 = (node_507.rg+node_236*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_226, _Texture),0.0,node_363)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_385, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
