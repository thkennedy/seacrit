// Shader created with Shader Forge Beta 0.31 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.31;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:3,bsrc:0,bdst:6,culm:0,dpts:2,wrdp:True,ufog:False,aust:False,igpj:True,qofs:0,qpre:4,rntp:5,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.2616124,fgcb:0.5661765,fgca:1,fgde:0.29,fgrn:3.5,fgrf:7.69,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-641-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33318,y:32595,tex:a0913c0175c84d0428dfde6a68d64ac5,ntxv:0,isnm:False|UVIN-560-UVOUT,TEX-75-TEX;n:type:ShaderForge.SFN_Panner,id:22,x:33589,y:32269,spu:0,spv:0.1|UVIN-109-OUT,DIST-592-OUT;n:type:ShaderForge.SFN_Panner,id:27,x:33270,y:32337,spu:0.1,spv:0|UVIN-22-UVOUT,DIST-595-OUT;n:type:ShaderForge.SFN_Multiply,id:33,x:33072,y:32939|A-565-OUT,B-35-RGB;n:type:ShaderForge.SFN_Color,id:35,x:33442,y:32978,ptlb:WaterColor,ptin:_WaterColor,glob:False,c1:0,c2:0.2627451,c3:0.5647059,c4:1;n:type:ShaderForge.SFN_Tex2dAsset,id:75,x:33683,y:32830,ptlb:node_75,ptin:_node_75,glob:False,tex:a0913c0175c84d0428dfde6a68d64ac5;n:type:ShaderForge.SFN_Tex2d,id:77,x:32794,y:32446,tex:a0913c0175c84d0428dfde6a68d64ac5,ntxv:0,isnm:False|UVIN-534-OUT,TEX-75-TEX;n:type:ShaderForge.SFN_Add,id:79,x:33096,y:32679|A-77-B,B-2-B;n:type:ShaderForge.SFN_TexCoord,id:108,x:34047,y:32297,uv:0;n:type:ShaderForge.SFN_Multiply,id:109,x:33776,y:32293|A-110-OUT,B-108-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:110,x:33853,y:32182,ptlb:Tiling,ptin:_Tiling,glob:False,v1:1;n:type:ShaderForge.SFN_ObjectPosition,id:451,x:34489,y:32639;n:type:ShaderForge.SFN_Multiply,id:534,x:32959,y:32373|A-541-OUT,B-548-OUT;n:type:ShaderForge.SFN_Multiply,id:541,x:33135,y:32240|A-27-UVOUT,B-110-OUT;n:type:ShaderForge.SFN_Vector1,id:548,x:33149,y:32524,v1:1.5;n:type:ShaderForge.SFN_Panner,id:560,x:33514,y:32476,spu:0.1,spv:0|UVIN-562-UVOUT,DIST-596-OUT;n:type:ShaderForge.SFN_Panner,id:562,x:33776,y:32460,spu:0,spv:0.11|UVIN-108-UVOUT,DIST-592-OUT;n:type:ShaderForge.SFN_Power,id:565,x:33232,y:32818|VAL-79-OUT,EXP-567-OUT;n:type:ShaderForge.SFN_ValueProperty,id:567,x:33428,y:32881,ptlb:Power,ptin:_Power,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:568,x:33002,y:33087|A-33-OUT,B-570-OUT;n:type:ShaderForge.SFN_ValueProperty,id:570,x:33168,y:33140,ptlb:Multiplyp,ptin:_Multiplyp,glob:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:581,x:33931,y:32802|A-451-X,B-583-OUT;n:type:ShaderForge.SFN_ValueProperty,id:583,x:34191,y:32483,ptlb:Pan,ptin:_Pan,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:592,x:33955,y:32540|A-451-Y,B-583-OUT;n:type:ShaderForge.SFN_Time,id:594,x:34160,y:32737;n:type:ShaderForge.SFN_Add,id:595,x:33776,y:32670|A-581-OUT,B-594-TSL;n:type:ShaderForge.SFN_Subtract,id:596,x:33839,y:32938|A-581-OUT,B-594-TSL;n:type:ShaderForge.SFN_Parallax,id:597,x:33514,y:33156|HEI-79-OUT;n:type:ShaderForge.SFN_Add,id:598,x:32995,y:32818|A-33-OUT,B-625-OUT;n:type:ShaderForge.SFN_ComponentMask,id:600,x:33238,y:33218,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-602-V;n:type:ShaderForge.SFN_TexCoord,id:602,x:33690,y:33295,uv:0;n:type:ShaderForge.SFN_Multiply,id:603,x:32615,y:33341|A-619-OUT,B-606-RGB;n:type:ShaderForge.SFN_Color,id:606,x:33212,y:33400,ptlb:GradientColor,ptin:_GradientColor,glob:False,c1:0,c2:0.2627451,c3:0.5647059,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:612,x:33002,y:33376,ptlb:GradientPower,ptin:_GradientPower,glob:False,v1:1;n:type:ShaderForge.SFN_Power,id:618,x:33044,y:33236|VAL-600-OUT,EXP-612-OUT;n:type:ShaderForge.SFN_Multiply,id:619,x:32817,y:33283|A-618-OUT,B-621-OUT;n:type:ShaderForge.SFN_ValueProperty,id:621,x:32952,y:33442,ptlb:GradientMultiply,ptin:_GradientMultiply,glob:False,v1:1;n:type:ShaderForge.SFN_Clamp01,id:623,x:33735,y:33192|IN-634-OUT;n:type:ShaderForge.SFN_Multiply,id:625,x:32449,y:33224|A-623-OUT,B-603-OUT;n:type:ShaderForge.SFN_ValueProperty,id:627,x:34252,y:33099,ptlb:GradientHeght,ptin:_GradientHeght,glob:False,v1:7.6;n:type:ShaderForge.SFN_Subtract,id:634,x:34015,y:33021|A-627-OUT,B-642-OUT;n:type:ShaderForge.SFN_ValueProperty,id:636,x:34301,y:33404,ptlb:GradientHeght_copy,ptin:_GradientHeght_copy,glob:False,v1:7.6;n:type:ShaderForge.SFN_Clamp01,id:638,x:33960,y:33523|IN-640-OUT;n:type:ShaderForge.SFN_Subtract,id:640,x:34064,y:33326|A-636-OUT,B-642-OUT;n:type:ShaderForge.SFN_Multiply,id:641,x:32877,y:32604|A-638-OUT,B-598-OUT;n:type:ShaderForge.SFN_Multiply,id:642,x:34239,y:32920|A-451-Y,B-643-OUT;n:type:ShaderForge.SFN_Vector1,id:643,x:34452,y:32954,v1:0.25;proporder:35-75-110-567-570-583-606-612-621-627-636;pass:END;sub:END;*/

Shader "Shader Forge/waterBackgroundShader" {
    Properties {
        _WaterColor ("WaterColor", Color) = (0,0.2627451,0.5647059,1)
        _node_75 ("node_75", 2D) = "white" {}
        _Tiling ("Tiling", Float ) = 1
        _Power ("Power", Float ) = 1
        _Multiplyp ("Multiplyp", Float ) = 0
        _Pan ("Pan", Float ) = 1
        _GradientColor ("GradientColor", Color) = (0,0.2627451,0.5647059,1)
        _GradientPower ("GradientPower", Float ) = 1
        _GradientMultiply ("GradientMultiply", Float ) = 1
        _GradientHeght ("GradientHeght", Float ) = 7.6
        _GradientHeght_copy ("GradientHeght_copy", Float ) = 7.6
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay"
            "RenderType"="Overlay"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _WaterColor;
            uniform sampler2D _node_75; uniform float4 _node_75_ST;
            uniform float _Tiling;
            uniform float _Power;
            uniform float _Pan;
            uniform float4 _GradientColor;
            uniform float _GradientPower;
            uniform float _GradientMultiply;
            uniform float _GradientHeght;
            uniform float _GradientHeght_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
////// Lighting:
////// Emissive:
                float4 node_451 = objPos;
                float node_642 = (node_451.g*0.25);
                float node_581 = (node_451.r*_Pan);
                float4 node_594 = _Time + _TimeEditor;
                float node_592 = (node_451.g*_Pan);
                float2 node_108 = i.uv0;
                float2 node_534 = (((((_Tiling*node_108.rg)+node_592*float2(0,0.1))+(node_581+node_594.r)*float2(0.1,0))*_Tiling)*1.5);
                float2 node_560 = ((node_108.rg+node_592*float2(0,0.11))+(node_581-node_594.r)*float2(0.1,0));
                float node_79 = (tex2D(_node_75,TRANSFORM_TEX(node_534, _node_75)).b+tex2D(_node_75,TRANSFORM_TEX(node_560, _node_75)).b);
                float3 node_33 = (pow(node_79,_Power)*_WaterColor.rgb);
                float3 emissive = (saturate((_GradientHeght_copy-node_642))*(node_33+(saturate((_GradientHeght-node_642))*((pow(i.uv0.g.r,_GradientPower)*_GradientMultiply)*_GradientColor.rgb))));
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
