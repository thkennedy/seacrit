// Shader created with Shader Forge Beta 0.28 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.28;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:2,bsrc:0,bdst:0,culm:2,dpts:2,wrdp:False,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.1776371,fgcb:0.8308824,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-14-OUT;n:type:ShaderForge.SFN_Panner,id:2,x:33227,y:32785,spu:1,spv:0|UVIN-13-UVOUT,DIST-3-OUT;n:type:ShaderForge.SFN_Multiply,id:3,x:33436,y:32820|A-6-OUT,B-5-TSL;n:type:ShaderForge.SFN_Time,id:5,x:33759,y:32872;n:type:ShaderForge.SFN_ValueProperty,id:6,x:33680,y:32747,ptlb:SpinRate,ptin:_SpinRate,glob:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:12,x:33175,y:32576,ptlb:Texture,ptin:_Texture,tex:56e68305a8a9caf41b3f6b4abda46558,ntxv:0,isnm:False|UVIN-2-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:13,x:33680,y:32552,uv:0;n:type:ShaderForge.SFN_Multiply,id:14,x:33020,y:32828|A-12-RGB,B-16-RGB;n:type:ShaderForge.SFN_Color,id:16,x:33253,y:32985,ptlb:Color,ptin:_Color,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_OneMinus,id:19,x:32591,y:32360;n:type:ShaderForge.SFN_OneMinus,id:20,x:32591,y:32360;n:type:ShaderForge.SFN_OneMinus,id:21,x:32591,y:32360;proporder:6-12-16;pass:END;sub:END;*/

Shader "Shader Forge/DashShader" {
    Properties {
        _SpinRate ("SpinRate", Float ) = 1
        _Texture ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _SpinRate;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_5 = _Time + _TimeEditor;
                float2 node_2 = (i.uv0.rg+(_SpinRate*node_5.r)*float2(1,0));
                float3 emissive = (tex2D(_Texture,TRANSFORM_TEX(node_2, _Texture)).rgb*_Color.rgb);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
