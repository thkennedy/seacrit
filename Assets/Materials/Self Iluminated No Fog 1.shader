// Shader created with Shader Forge Beta 0.28 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.28;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:False,aust:False,igpj:True,qofs:0,qpre:4,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.165061,fgcb:0.7720588,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:297,x:32719,y:32712|emission-348-OUT,alpha-354-OUT;n:type:ShaderForge.SFN_Tex2d,id:298,x:33257,y:32820,ptlb:Diffuse,ptin:_Diffuse,tex:e9fac503af0ef514ca0a0f6827fab358,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:347,x:33272,y:32497,ptlb:Color,ptin:_Color,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:348,x:33016,y:32785|A-347-RGB,B-298-RGB;n:type:ShaderForge.SFN_Multiply,id:354,x:32960,y:32903|A-347-A,B-298-A;proporder:298-347;pass:END;sub:END;*/

Shader "Shader Forge/Self Iluminated No Fog" {
    Properties {
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_347 = _Color;
                float2 node_360 = i.uv0;
                float4 node_298 = tex2D(_Diffuse,TRANSFORM_TEX(node_360.rg, _Diffuse));
                float3 emissive = (node_347.rgb*node_298.rgb);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,(node_347.a*node_298.a));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
