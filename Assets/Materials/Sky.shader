// Shader created with Shader Forge Beta 0.28 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.28;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.1776371,fgcb:0.8308824,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-9-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33412,y:32628,ptlb:Sky,ptin:_Sky,tex:7f9ef5dcb08b468439a56081d7fec6e9,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3,x:32878,y:32474|A-72-OUT,B-12-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4,x:33527,y:32322,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5-V;n:type:ShaderForge.SFN_TexCoord,id:5,x:33875,y:32440,uv:0;n:type:ShaderForge.SFN_OneMinus,id:6,x:33332,y:32322|IN-4-OUT;n:type:ShaderForge.SFN_Panner,id:7,x:33787,y:32822,spu:0.1111,spv:0|DIST-169-OUT;n:type:ShaderForge.SFN_Tex2d,id:8,x:33548,y:32822,tex:258bce57f8bf8e04ab3c9b77c0311499,ntxv:0,isnm:False|UVIN-7-UVOUT,TEX-49-TEX;n:type:ShaderForge.SFN_Lerp,id:9,x:32996,y:32683|A-47-OUT,B-2-RGB,T-60-OUT;n:type:ShaderForge.SFN_Power,id:10,x:33131,y:32352|VAL-6-OUT,EXP-14-OUT;n:type:ShaderForge.SFN_ValueProperty,id:12,x:33056,y:32579,ptlb:Lerp Mutiply,ptin:_LerpMutiply,glob:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:14,x:33336,y:32466,ptlb:Lerp Power,ptin:_LerpPower,glob:False,v1:2;n:type:ShaderForge.SFN_Add,id:47,x:33206,y:32898|A-8-RGB,B-48-RGB;n:type:ShaderForge.SFN_Tex2d,id:48,x:33419,y:33042,tex:258bce57f8bf8e04ab3c9b77c0311499,ntxv:0,isnm:False|UVIN-53-OUT,TEX-49-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:49,x:33682,y:32993,ptlb:Space Texture,ptin:_SpaceTexture,glob:False,tex:258bce57f8bf8e04ab3c9b77c0311499;n:type:ShaderForge.SFN_Multiply,id:53,x:33419,y:33247|A-183-UVOUT,B-55-OUT;n:type:ShaderForge.SFN_ValueProperty,id:55,x:33628,y:33297,ptlb:Space Tile,ptin:_SpaceTile,glob:False,v1:2;n:type:ShaderForge.SFN_Clamp01,id:60,x:32664,y:32495|IN-3-OUT;n:type:ShaderForge.SFN_Subtract,id:72,x:32902,y:32332|A-10-OUT,B-74-OUT;n:type:ShaderForge.SFN_ValueProperty,id:74,x:32957,y:32243,ptlb:Lerp Subtract,ptin:_LerpSubtract,glob:False,v1:0.5;n:type:ShaderForge.SFN_Vector4,id:80,x:32362,y:31985,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4,id:81,x:32360,y:31988,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4,id:82,x:32360,y:31988,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Vector1,id:98,x:32493,y:32220,v1:0;n:type:ShaderForge.SFN_Vector1,id:99,x:32493,y:32220,v1:0;n:type:ShaderForge.SFN_ObjectPosition,id:160,x:34176,y:32854;n:type:ShaderForge.SFN_Multiply,id:169,x:33972,y:32728|A-171-OUT,B-160-X;n:type:ShaderForge.SFN_ValueProperty,id:171,x:34158,y:32674,ptlb:Space Pan,ptin:_SpacePan,glob:False,v1:1;n:type:ShaderForge.SFN_Panner,id:183,x:33871,y:33112,spu:0.1,spv:0|DIST-186-OUT;n:type:ShaderForge.SFN_ValueProperty,id:185,x:34119,y:33132,ptlb:Space Pan 2,ptin:_SpacePan2,glob:False,v1:0.51111;n:type:ShaderForge.SFN_Multiply,id:186,x:33947,y:32940|A-160-X,B-185-OUT;proporder:2-12-14-49-74-55-171-185;pass:END;sub:END;*/

Shader "Shader Forge/Sky" {
    Properties {
        _Sky ("Sky", 2D) = "white" {}
        _LerpMutiply ("Lerp Mutiply", Float ) = 2
        _LerpPower ("Lerp Power", Float ) = 2
        _SpaceTexture ("Space Texture", 2D) = "white" {}
        _LerpSubtract ("Lerp Subtract", Float ) = 0.5
        _SpaceTile ("Space Tile", Float ) = 2
        _SpacePan ("Space Pan", Float ) = 1
        _SpacePan2 ("Space Pan 2", Float ) = 0.51111
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _Sky; uniform float4 _Sky_ST;
            uniform float _LerpMutiply;
            uniform float _LerpPower;
            uniform sampler2D _SpaceTexture; uniform float4 _SpaceTexture_ST;
            uniform float _SpaceTile;
            uniform float _LerpSubtract;
            uniform float _SpacePan;
            uniform float _SpacePan2;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
////// Lighting:
////// Emissive:
                float4 node_160 = objPos;
                float2 node_200 = i.uv0;
                float2 node_7 = (node_200.rg+(_SpacePan*node_160.r)*float2(0.1111,0));
                float2 node_53 = ((node_200.rg+(node_160.r*_SpacePan2)*float2(0.1,0))*_SpaceTile);
                float3 emissive = lerp((tex2D(_SpaceTexture,TRANSFORM_TEX(node_7, _SpaceTexture)).rgb+tex2D(_SpaceTexture,TRANSFORM_TEX(node_53, _SpaceTexture)).rgb),tex2D(_Sky,TRANSFORM_TEX(node_200.rg, _Sky)).rgb,saturate(((pow((1.0 - i.uv0.g.r),_LerpPower)-_LerpSubtract)*_LerpMutiply)));
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
