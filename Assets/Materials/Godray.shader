// Shader created with Shader Forge Beta 0.31 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.31;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:2,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:False,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.1776371,fgcb:0.8308824,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32786,y:32709|emission-109-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:34143,y:32597,tex:a0913c0175c84d0428dfde6a68d64ac5,ntxv:0,isnm:False|UVIN-12-UVOUT,TEX-30-TEX;n:type:ShaderForge.SFN_Time,id:9,x:35150,y:32725;n:type:ShaderForge.SFN_Multiply,id:10,x:34941,y:32770|A-9-TSL,B-11-OUT;n:type:ShaderForge.SFN_ValueProperty,id:11,x:35150,y:32859,ptlb:Ray Pan Modifie,ptin:_RayPanModifie,glob:False,v1:1;n:type:ShaderForge.SFN_Panner,id:12,x:34381,y:32639,spu:1,spv:0|UVIN-140-OUT,DIST-10-OUT;n:type:ShaderForge.SFN_Tex2d,id:29,x:34016,y:32364,tex:a0913c0175c84d0428dfde6a68d64ac5,ntxv:0,isnm:False|TEX-30-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:30,x:34574,y:32276,ptlb:Texture Asset,ptin:_TextureAsset,glob:False,tex:a0913c0175c84d0428dfde6a68d64ac5;n:type:ShaderForge.SFN_Power,id:45,x:33749,y:32824|VAL-66-OUT,EXP-68-OUT;n:type:ShaderForge.SFN_TexCoord,id:50,x:34941,y:32596,uv:0;n:type:ShaderForge.SFN_Multiply,id:51,x:33830,y:32589|A-29-R,B-69-OUT;n:type:ShaderForge.SFN_Add,id:52,x:33714,y:32443|A-29-R,B-51-OUT;n:type:ShaderForge.SFN_Tex2d,id:59,x:34158,y:32842,tex:a0913c0175c84d0428dfde6a68d64ac5,ntxv:0,isnm:False|UVIN-62-UVOUT,TEX-30-TEX;n:type:ShaderForge.SFN_Panner,id:62,x:34358,y:32858,spu:1,spv:0|UVIN-64-OUT,DIST-10-OUT;n:type:ShaderForge.SFN_Append,id:64,x:34558,y:32891|A-137-OUT,B-50-V;n:type:ShaderForge.SFN_OneMinus,id:65,x:34941,y:32990|IN-50-U;n:type:ShaderForge.SFN_Multiply,id:66,x:33961,y:32769|A-2-G,B-59-G;n:type:ShaderForge.SFN_ValueProperty,id:68,x:33941,y:32965,ptlb:Godmray Power,ptin:_GodmrayPower,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:69,x:33582,y:32967|A-45-OUT,B-71-OUT;n:type:ShaderForge.SFN_ValueProperty,id:71,x:33768,y:33062,ptlb:Godray Multiply,ptin:_GodrayMultiply,glob:False,v1:1;n:type:ShaderForge.SFN_Power,id:100,x:33433,y:32486|VAL-52-OUT,EXP-106-OUT;n:type:ShaderForge.SFN_Multiply,id:102,x:33267,y:32629|A-100-OUT,B-104-OUT;n:type:ShaderForge.SFN_ValueProperty,id:104,x:33453,y:32724,ptlb:Main Multiply,ptin:_MainMultiply,glob:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:106,x:33626,y:32628,ptlb:Main Power,ptin:_MainPower,glob:False,v1:1;n:type:ShaderForge.SFN_Color,id:108,x:33245,y:32814,ptlb:Color,ptin:_Color,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:109,x:33119,y:32722|A-102-OUT,B-108-RGB;n:type:ShaderForge.SFN_Multiply,id:137,x:34659,y:33125|A-65-OUT,B-143-OUT;n:type:ShaderForge.SFN_Multiply,id:138,x:34653,y:32701|A-50-U,B-143-OUT;n:type:ShaderForge.SFN_Append,id:140,x:34522,y:32749|A-138-OUT,B-50-V;n:type:ShaderForge.SFN_ValueProperty,id:143,x:34843,y:33261,ptlb:RayTile,ptin:_RayTile,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:167,x:33093,y:33002|A-109-OUT,B-174-OUT;n:type:ShaderForge.SFN_Sin,id:168,x:33623,y:33182|IN-317-OUT;n:type:ShaderForge.SFN_Add,id:174,x:33271,y:33163|A-168-OUT,B-286-OUT;n:type:ShaderForge.SFN_Time,id:189,x:34162,y:33144;n:type:ShaderForge.SFN_Vector1,id:286,x:33494,y:33377,v1:1.5;n:type:ShaderForge.SFN_Add,id:310,x:33742,y:33343|A-317-OUT,B-335-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:311,x:34105,y:33488;n:type:ShaderForge.SFN_Multiply,id:317,x:33917,y:33218|A-189-T,B-319-OUT;n:type:ShaderForge.SFN_ValueProperty,id:319,x:34162,y:33374,ptlb:Fade Rate,ptin:_FadeRate,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:335,x:33864,y:33549|A-311-X,B-336-OUT;n:type:ShaderForge.SFN_Vector1,id:336,x:34000,y:33626,v1:3;n:type:ShaderForge.SFN_FragmentPosition,id:346,x:33967,y:33380;proporder:11-30-68-71-104-106-108-143-319;pass:END;sub:END;*/

Shader "Shader Forge/Godray" {
    Properties {
        _RayPanModifie ("Ray Pan Modifie", Float ) = 1
        _TextureAsset ("Texture Asset", 2D) = "white" {}
        _GodmrayPower ("Godmray Power", Float ) = 1
        _GodrayMultiply ("Godray Multiply", Float ) = 1
        _MainMultiply ("Main Multiply", Float ) = 1
        _MainPower ("Main Power", Float ) = 1
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _RayTile ("RayTile", Float ) = 1
        _FadeRate ("Fade Rate", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _RayPanModifie;
            uniform sampler2D _TextureAsset; uniform float4 _TextureAsset_ST;
            uniform float _GodmrayPower;
            uniform float _GodrayMultiply;
            uniform float _MainMultiply;
            uniform float _MainPower;
            uniform float4 _Color;
            uniform float _RayTile;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_411 = i.uv0;
                float4 node_29 = tex2D(_TextureAsset,TRANSFORM_TEX(node_411.rg, _TextureAsset));
                float4 node_9 = _Time + _TimeEditor;
                float node_10 = (node_9.r*_RayPanModifie);
                float2 node_50 = i.uv0;
                float2 node_12 = (float2((node_50.r*_RayTile),node_50.g)+node_10*float2(1,0));
                float2 node_62 = (float2(((1.0 - node_50.r)*_RayTile),node_50.g)+node_10*float2(1,0));
                float3 node_109 = ((pow((node_29.r+(node_29.r*(pow((tex2D(_TextureAsset,TRANSFORM_TEX(node_12, _TextureAsset)).g*tex2D(_TextureAsset,TRANSFORM_TEX(node_62, _TextureAsset)).g),_GodmrayPower)*_GodrayMultiply))),_MainPower)*_MainMultiply)*_Color.rgb);
                float3 emissive = node_109;
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
