// Shader created with Shader Forge Beta 0.31 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.31;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5588235,fgcg:0.5588235,fgcb:0.5588235,fgca:1,fgde:0.29,fgrn:3.5,fgrf:7.69,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-298-OUT,diffpow-249-OUT,spec-86-OUT,gloss-91-OUT,normal-5-RGB,emission-150-OUT,alpha-698-R;n:type:ShaderForge.SFN_Tex2d,id:2,x:33635,y:32196,ptlb:Diffuse,ptin:_Diffuse,tex:7dfeb8deb0ac00945a7a665bcf350cae,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5,x:34031,y:33132,ptlb:Normal Map,ptin:_NormalMap,tex:6ebb6d2710ee16e408a7912aba84c1dc,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:17,x:33420,y:32273|A-661-OUT,B-290-OUT;n:type:ShaderForge.SFN_Power,id:18,x:33240,y:32299|VAL-17-OUT,EXP-292-OUT;n:type:ShaderForge.SFN_ValueProperty,id:86,x:33153,y:32800,ptlb:Specular,ptin:_Specular,glob:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:91,x:33153,y:32898,ptlb:Gloss,ptin:_Gloss,glob:False,v1:1;n:type:ShaderForge.SFN_Fresnel,id:110,x:33634,y:33396|EXP-115-OUT;n:type:ShaderForge.SFN_ValueProperty,id:115,x:33806,y:33494,ptlb:Fresnel,ptin:_Fresnel,glob:False,v1:10;n:type:ShaderForge.SFN_Multiply,id:146,x:33481,y:33507|A-110-OUT,B-147-OUT;n:type:ShaderForge.SFN_ValueProperty,id:147,x:33662,y:33619,ptlb:Fresnel Multiply,ptin:_FresnelMultiply,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:150,x:33316,y:33442|A-157-RGB,B-146-OUT;n:type:ShaderForge.SFN_Color,id:157,x:33495,y:33334,ptlb:Fresnel Color,ptin:_FresnelColor,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:249,x:33168,y:32705,ptlb:Diffuse Power Power,ptin:_DiffusePowerPower,glob:False,v1:1;n:type:ShaderForge.SFN_Color,id:261,x:33260,y:32482,ptlb:Diffuse Color,ptin:_DiffuseColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:264,x:33042,y:32508|A-18-OUT,B-261-RGB;n:type:ShaderForge.SFN_ValueProperty,id:290,x:33635,y:32398,ptlb:Diffuse Multiply,ptin:_DiffuseMultiply,glob:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:292,x:33471,y:32470,ptlb:Diffuse Power,ptin:_DiffusePower,glob:False,v1:1;n:type:ShaderForge.SFN_Lerp,id:298,x:32823,y:32553|A-696-OUT,B-314-OUT,T-325-OUT;n:type:ShaderForge.SFN_Color,id:300,x:33674,y:32926,ptlb:Accent Color,ptin:_AccentColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:304,x:34152,y:32630,ptlb:Accent,ptin:_Accent,tex:7dfeb8deb0ac00945a7a665bcf350cae,ntxv:0,isnm:False|UVIN-318-OUT;n:type:ShaderForge.SFN_ValueProperty,id:311,x:33507,y:32798,ptlb:Accent Multiply,ptin:_AccentMultiply,glob:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:314,x:33324,y:32686|A-641-OUT,B-311-OUT;n:type:ShaderForge.SFN_ValueProperty,id:316,x:34357,y:32608,ptlb:Accent Tile,ptin:_AccentTile,glob:False,v1:1;n:type:ShaderForge.SFN_TexCoord,id:317,x:34828,y:32145,uv:0;n:type:ShaderForge.SFN_Multiply,id:318,x:34107,y:32402|A-319-UVOUT,B-316-OUT;n:type:ShaderForge.SFN_Rotator,id:319,x:34340,y:32389|UVIN-354-OUT,ANG-324-OUT;n:type:ShaderForge.SFN_Slider,id:324,x:34549,y:32531,ptlb:Rotation,ptin:_Rotation,min:0,cur:0,max:360;n:type:ShaderForge.SFN_Multiply,id:325,x:33806,y:32763|A-304-A,B-327-OUT;n:type:ShaderForge.SFN_ValueProperty,id:327,x:34034,y:32846,ptlb:Accent Opacity,ptin:_AccentOpacity,glob:False,v1:1;n:type:ShaderForge.SFN_Append,id:354,x:34463,y:32231|A-355-OUT,B-317-V;n:type:ShaderForge.SFN_Multiply,id:355,x:34706,y:32370|A-317-U,B-357-OUT;n:type:ShaderForge.SFN_ValueProperty,id:357,x:34964,y:32392,ptlb:Accent Horizotal tile,ptin:_AccentHorizotaltile,glob:False,v1:1;n:type:ShaderForge.SFN_Color,id:639,x:34641,y:32705,ptlb:Accent Color 2,ptin:_AccentColor2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:641,x:33599,y:32607|A-639-RGB,B-300-RGB,T-304-A;n:type:ShaderForge.SFN_Slider,id:659,x:33606,y:32087,ptlb:BW,ptin:_BW,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:661,x:33365,y:32051|A-2-RGB,B-663-OUT,T-659-OUT;n:type:ShaderForge.SFN_Multiply,id:663,x:33581,y:31854|A-667-OUT,B-665-OUT;n:type:ShaderForge.SFN_Vector1,id:665,x:33801,y:31967,v1:0.3;n:type:ShaderForge.SFN_Add,id:667,x:33801,y:31830|A-669-OUT,B-2-B;n:type:ShaderForge.SFN_Add,id:669,x:33991,y:31707|A-2-R,B-2-G;n:type:ShaderForge.SFN_Lerp,id:696,x:33017,y:32311|A-718-OUT,B-264-OUT,T-2-A;n:type:ShaderForge.SFN_VertexColor,id:698,x:33235,y:33058;n:type:ShaderForge.SFN_Multiply,id:718,x:32849,y:32081|A-720-RGB,B-18-OUT;n:type:ShaderForge.SFN_Color,id:720,x:33089,y:32060,ptlb:Diffuse Color2,ptin:_DiffuseColor2,glob:False,c1:1,c2:1,c3:1,c4:1;proporder:5-2-86-91-115-147-157-249-261-290-292-300-304-311-316-324-327-357-639-659-720;pass:END;sub:END;*/

Shader "Shader Forge/PlayerShader2" {
    Properties {
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Specular ("Specular", Float ) = 1
        _Gloss ("Gloss", Float ) = 1
        _Fresnel ("Fresnel", Float ) = 10
        _FresnelMultiply ("Fresnel Multiply", Float ) = 1
        _FresnelColor ("Fresnel Color", Color) = (0.5,0.5,0.5,1)
        _DiffusePowerPower ("Diffuse Power Power", Float ) = 1
        _DiffuseColor ("Diffuse Color", Color) = (1,1,1,1)
        _DiffuseMultiply ("Diffuse Multiply", Float ) = 2
        _DiffusePower ("Diffuse Power", Float ) = 1
        _AccentColor ("Accent Color", Color) = (1,1,1,1)
        _Accent ("Accent", 2D) = "white" {}
        _AccentMultiply ("Accent Multiply", Float ) = 2
        _AccentTile ("Accent Tile", Float ) = 1
        _Rotation ("Rotation", Range(0, 360)) = 0
        _AccentOpacity ("Accent Opacity", Float ) = 1
        _AccentHorizotaltile ("Accent Horizotal tile", Float ) = 1
        _AccentColor2 ("Accent Color 2", Color) = (1,1,1,1)
        _BW ("BW", Range(0, 1)) = 0
        _DiffuseColor2 ("Diffuse Color2", Color) = (1,1,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Fresnel;
            uniform float _FresnelMultiply;
            uniform float4 _FresnelColor;
            uniform float _DiffusePowerPower;
            uniform float4 _DiffuseColor;
            uniform float _DiffuseMultiply;
            uniform float _DiffusePower;
            uniform float4 _AccentColor;
            uniform sampler2D _Accent; uniform float4 _Accent_ST;
            uniform float _AccentMultiply;
            uniform float _AccentTile;
            uniform float _Rotation;
            uniform float _AccentOpacity;
            uniform float _AccentHorizotaltile;
            uniform float4 _AccentColor2;
            uniform float _BW;
            uniform float4 _DiffuseColor2;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.vertexColor = v.vertexColor;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_725 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(node_725.rg, _NormalMap))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = pow(max( 0.0, NdotL), _DiffusePowerPower) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float3 emissive = (_FresnelColor.rgb*(pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)*_FresnelMultiply));
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_2 = tex2D(_Diffuse,TRANSFORM_TEX(node_725.rg, _Diffuse));
                float node_663 = (((node_2.r+node_2.g)+node_2.b)*0.3);
                float3 node_18 = pow((lerp(node_2.rgb,float3(node_663,node_663,node_663),_BW)*_DiffuseMultiply),_DiffusePower);
                float node_319_ang = _Rotation;
                float node_319_spd = 1.0;
                float node_319_cos = cos(node_319_spd*node_319_ang);
                float node_319_sin = sin(node_319_spd*node_319_ang);
                float2 node_319_piv = float2(0.5,0.5);
                float2 node_317 = i.uv0;
                float2 node_319 = (mul(float2((node_317.r*_AccentHorizotaltile),node_317.g)-node_319_piv,float2x2( node_319_cos, -node_319_sin, node_319_sin, node_319_cos))+node_319_piv);
                float2 node_318 = (node_319*_AccentTile);
                float4 node_304 = tex2D(_Accent,TRANSFORM_TEX(node_318, _Accent));
                finalColor += diffuseLight * lerp(lerp((_DiffuseColor2.rgb*node_18),(node_18*_DiffuseColor.rgb),node_2.a),(lerp(_AccentColor2.rgb,_AccentColor.rgb,node_304.a)*_AccentMultiply),(node_304.a*_AccentOpacity));
                finalColor += specular;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,i.vertexColor.r);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Fresnel;
            uniform float _FresnelMultiply;
            uniform float4 _FresnelColor;
            uniform float _DiffusePowerPower;
            uniform float4 _DiffuseColor;
            uniform float _DiffuseMultiply;
            uniform float _DiffusePower;
            uniform float4 _AccentColor;
            uniform sampler2D _Accent; uniform float4 _Accent_ST;
            uniform float _AccentMultiply;
            uniform float _AccentTile;
            uniform float _Rotation;
            uniform float _AccentOpacity;
            uniform float _AccentHorizotaltile;
            uniform float4 _AccentColor2;
            uniform float _BW;
            uniform float4 _DiffuseColor2;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.vertexColor = v.vertexColor;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_726 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(node_726.rg, _NormalMap))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = pow(max( 0.0, NdotL), _DiffusePowerPower) * attenColor;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_2 = tex2D(_Diffuse,TRANSFORM_TEX(node_726.rg, _Diffuse));
                float node_663 = (((node_2.r+node_2.g)+node_2.b)*0.3);
                float3 node_18 = pow((lerp(node_2.rgb,float3(node_663,node_663,node_663),_BW)*_DiffuseMultiply),_DiffusePower);
                float node_319_ang = _Rotation;
                float node_319_spd = 1.0;
                float node_319_cos = cos(node_319_spd*node_319_ang);
                float node_319_sin = sin(node_319_spd*node_319_ang);
                float2 node_319_piv = float2(0.5,0.5);
                float2 node_317 = i.uv0;
                float2 node_319 = (mul(float2((node_317.r*_AccentHorizotaltile),node_317.g)-node_319_piv,float2x2( node_319_cos, -node_319_sin, node_319_sin, node_319_cos))+node_319_piv);
                float2 node_318 = (node_319*_AccentTile);
                float4 node_304 = tex2D(_Accent,TRANSFORM_TEX(node_318, _Accent));
                finalColor += diffuseLight * lerp(lerp((_DiffuseColor2.rgb*node_18),(node_18*_DiffuseColor.rgb),node_2.a),(lerp(_AccentColor2.rgb,_AccentColor.rgb,node_304.a)*_AccentMultiply),(node_304.a*_AccentOpacity));
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * i.vertexColor.r,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
