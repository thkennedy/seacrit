// Shader created with Shader Forge Beta 0.31 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.31;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:False,aust:False,igpj:True,qofs:0,qpre:4,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.1776371,fgcb:0.8308824,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:409,x:32719,y:32712|diff-596-OUT,emission-596-OUT,alpha-438-OUT;n:type:ShaderForge.SFN_Tex2d,id:413,x:33586,y:32527,ptlb:Texture,ptin:_Texture,tex:e980decad3315f943a4645146ecb9e11,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Clamp,id:414,x:33383,y:32791|IN-421-OUT,MIN-415-OUT,MAX-416-OUT;n:type:ShaderForge.SFN_Vector1,id:415,x:33728,y:32855,v1:0;n:type:ShaderForge.SFN_Vector1,id:416,x:33728,y:32917,v1:1;n:type:ShaderForge.SFN_Subtract,id:421,x:33409,y:32656|A-413-A,B-529-OUT;n:type:ShaderForge.SFN_Power,id:437,x:33195,y:32822|VAL-414-OUT,EXP-440-OUT;n:type:ShaderForge.SFN_Multiply,id:438,x:33099,y:32984|A-437-OUT,B-447-OUT;n:type:ShaderForge.SFN_Slider,id:440,x:33394,y:32946,ptlb:Power,ptin:_Power,min:0.01,cur:1,max:3;n:type:ShaderForge.SFN_Slider,id:447,x:33327,y:33054,ptlb:Multiply,ptin:_Multiply,min:0.01,cur:1,max:10;n:type:ShaderForge.SFN_OneMinus,id:449,x:33760,y:32608|IN-524-OUT;n:type:ShaderForge.SFN_Color,id:502,x:33017,y:32571,ptlb:Color,ptin:_Color,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:524,x:33951,y:32696,ptlb:Progress,ptin:_Progress,glob:False,v1:0.5;n:type:ShaderForge.SFN_Clamp,id:529,x:33586,y:32714|IN-449-OUT,MIN-531-OUT,MAX-533-OUT;n:type:ShaderForge.SFN_Vector1,id:531,x:33951,y:32792,v1:0.03;n:type:ShaderForge.SFN_Vector1,id:533,x:33951,y:32879,v1:1;n:type:ShaderForge.SFN_Multiply,id:596,x:32977,y:32755|A-502-RGB,B-413-RGB;proporder:413-440-447-502-524;pass:END;sub:END;*/

Shader "Shader Forge/RadialUI" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _Power ("Power", Range(0.01, 3)) = 1
        _Multiply ("Multiply", Range(0.01, 10)) = 1
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Progress ("Progress", Float ) = 0.5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Power;
            uniform float _Multiply;
            uniform float4 _Color;
            uniform float _Progress;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float2 node_612 = i.uv0;
                float4 node_413 = tex2D(_Texture,TRANSFORM_TEX(node_612.rg, _Texture));
                float3 node_596 = (_Color.rgb*node_413.rgb);
                float3 emissive = node_596;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * node_596;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,(pow(clamp((node_413.a-clamp((1.0 - _Progress),0.03,1.0)),0.0,1.0),_Power)*_Multiply));
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Power;
            uniform float _Multiply;
            uniform float4 _Color;
            uniform float _Progress;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float2 node_613 = i.uv0;
                float4 node_413 = tex2D(_Texture,TRANSFORM_TEX(node_613.rg, _Texture));
                float3 node_596 = (_Color.rgb*node_413.rgb);
                finalColor += diffuseLight * node_596;
/// Final Color:
                return fixed4(finalColor * (pow(clamp((node_413.a-clamp((1.0 - _Progress),0.03,1.0)),0.0,1.0),_Power)*_Multiply),0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
