// Shader created with Shader Forge Beta 0.28 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.28;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:0,bsrc:0,bdst:0,culm:1,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.1776371,fgcb:0.8308824,fgca:1,fgde:0.14,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-477-OUT,spec-465-OUT,gloss-470-OUT,emission-477-OUT,voffset-337-OUT;n:type:ShaderForge.SFN_Tex2d,id:204,x:33594,y:32713,tex:1ca6f543584d9ac48afa79720d029bc6,ntxv:0,isnm:False|UVIN-409-UVOUT,MIP-364-OUT,TEX-246-TEX;n:type:ShaderForge.SFN_Time,id:208,x:33874,y:33062;n:type:ShaderForge.SFN_Panner,id:227,x:33351,y:33127,spu:1,spv:0|DIST-238-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:232,x:33899,y:33348;n:type:ShaderForge.SFN_ValueProperty,id:236,x:33840,y:33285,ptlb:Time Scalar,ptin:_TimeScalar,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:238,x:33662,y:33062|A-208-T,B-236-OUT;n:type:ShaderForge.SFN_Multiply,id:240,x:33681,y:33380|A-232-X,B-242-OUT;n:type:ShaderForge.SFN_ValueProperty,id:242,x:33858,y:33519,ptlb:Pannar Scalar,ptin:_PannarScalar,glob:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:244,x:33330,y:32843,tex:1ca6f543584d9ac48afa79720d029bc6,ntxv:0,isnm:False|UVIN-227-UVOUT,MIP-364-OUT,TEX-246-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:246,x:33852,y:32800,ptlb:Texture,ptin:_Texture,glob:False,tex:1ca6f543584d9ac48afa79720d029bc6;n:type:ShaderForge.SFN_TexCoord,id:326,x:33941,y:32555,uv:0;n:type:ShaderForge.SFN_NormalVector,id:335,x:33376,y:33409,pt:False;n:type:ShaderForge.SFN_Multiply,id:337,x:32859,y:33301|A-376-OUT,B-335-OUT,C-366-OUT;n:type:ShaderForge.SFN_Vector1,id:364,x:34009,y:32995,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:366,x:33429,y:33654,ptlb:Vertex Offset Scalar,ptin:_VertexOffsetScalar,glob:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:376,x:33083,y:33191|A-244-A,B-388-A;n:type:ShaderForge.SFN_Panner,id:386,x:33330,y:33272,spu:-1,spv:0|DIST-238-OUT;n:type:ShaderForge.SFN_Tex2d,id:388,x:33448,y:32969,tex:1ca6f543584d9ac48afa79720d029bc6,ntxv:0,isnm:False|UVIN-386-UVOUT,TEX-246-TEX;n:type:ShaderForge.SFN_Panner,id:409,x:33691,y:32555,spu:1,spv:0|UVIN-326-UVOUT,DIST-240-OUT;n:type:ShaderForge.SFN_ValueProperty,id:423,x:33557,y:33782,ptlb:Vertex Offset Scalar_copy_copy,ptin:_VertexOffsetScalar_copy_copy,glob:False,v1:0.1;n:type:ShaderForge.SFN_Slider,id:465,x:33165,y:32624,ptlb:Specular,ptin:_Specular,min:0,cur:10.82707,max:30;n:type:ShaderForge.SFN_Slider,id:470,x:33187,y:32734,ptlb:Gloss,ptin:_Gloss,min:0,cur:0.6315789,max:3;n:type:ShaderForge.SFN_Lerp,id:477,x:33078,y:32363|A-204-RGB,B-483-RGB,T-479-V;n:type:ShaderForge.SFN_TexCoord,id:479,x:33579,y:32310,uv:0;n:type:ShaderForge.SFN_Color,id:483,x:33401,y:32448,ptlb:Fog Color,ptin:_FogColor,glob:False,c1:1,c2:1,c3:1,c4:1;proporder:242-246-483-236-366-465-470;pass:END;sub:END;*/

Shader "Shader Forge/WaterShader2" {
    Properties {
        _PannarScalar ("Pannar Scalar", Float ) = 1
        _Texture ("Texture", 2D) = "white" {}
        _FogColor ("Fog Color", Color) = (1,1,1,1)
        _TimeScalar ("Time Scalar", Float ) = 1
        _VertexOffsetScalar ("Vertex Offset Scalar", Float ) = 0.1
        _Specular ("Specular", Range(0, 30)) = 10.82707
        _Gloss ("Gloss", Range(0, 3)) = 0.6315789
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform float _PannarScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(-v.normal,0), _World2Object).xyz;
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                float4 node_208 = _Time + _TimeEditor;
                float node_238 = (node_208.g*_TimeScalar);
                float2 node_630 = o.uv0;
                float2 node_227 = (node_630.rg+node_238*float2(1,0));
                float node_364 = 0.0;
                float2 node_386 = (node_630.rg+node_238*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_227, _Texture),0.0,node_364)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_386, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float2 node_409 = (i.uv0.rg+(objPos.r*_PannarScalar)*float2(1,0));
                float node_364 = 0.0;
                float3 node_477 = lerp(tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_409, _Texture),0.0,node_364)).rgb,_FogColor.rgb,i.uv0.g);
                float3 emissive = node_477;
///////// Gloss:
                float gloss = exp2(_Gloss*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float _Specular_var = _Specular;
                float3 specularColor = float3(_Specular_var,_Specular_var,_Specular_var);
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * node_477;
                finalColor += specular;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Front
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform float _PannarScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(-v.normal,0), _World2Object).xyz;
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                float4 node_208 = _Time + _TimeEditor;
                float node_238 = (node_208.g*_TimeScalar);
                float2 node_631 = o.uv0;
                float2 node_227 = (node_631.rg+node_238*float2(1,0));
                float node_364 = 0.0;
                float2 node_386 = (node_631.rg+node_238*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_227, _Texture),0.0,node_364)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_386, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
///////// Gloss:
                float gloss = exp2(_Gloss*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float _Specular_var = _Specular;
                float3 specularColor = float3(_Specular_var,_Specular_var,_Specular_var);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float2 node_409 = (i.uv0.rg+(objPos.r*_PannarScalar)*float2(1,0));
                float node_364 = 0.0;
                float3 node_477 = lerp(tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_409, _Texture),0.0,node_364)).rgb,_FogColor.rgb,i.uv0.g);
                finalColor += diffuseLight * node_477;
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            Cull Front
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float4 uv0 : TEXCOORD5;
                float3 normalDir : TEXCOORD6;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(-v.normal,0), _World2Object).xyz;
                float4 node_208 = _Time + _TimeEditor;
                float node_238 = (node_208.g*_TimeScalar);
                float2 node_632 = o.uv0;
                float2 node_227 = (node_632.rg+node_238*float2(1,0));
                float node_364 = 0.0;
                float2 node_386 = (node_632.rg+node_238*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_227, _Texture),0.0,node_364)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_386, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float _TimeScalar;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _VertexOffsetScalar;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(-v.normal,0), _World2Object).xyz;
                float4 node_208 = _Time + _TimeEditor;
                float node_238 = (node_208.g*_TimeScalar);
                float2 node_633 = o.uv0;
                float2 node_227 = (node_633.rg+node_238*float2(1,0));
                float node_364 = 0.0;
                float2 node_386 = (node_633.rg+node_238*float2(-1,0));
                v.vertex.xyz += ((tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_227, _Texture),0.0,node_364)).a*tex2Dlod(_Texture,float4(TRANSFORM_TEX(node_386, _Texture),0.0,0)).a)*v.normal*_VertexOffsetScalar);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
