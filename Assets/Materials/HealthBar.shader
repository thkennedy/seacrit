// Shader created with Shader Forge Beta 0.33 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.33;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0.3379312,fgcb:1,fgca:1,fgde:0.13,fgrn:2.41,fgrf:7.61,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-31-OUT,alpha-10-OUT;n:type:ShaderForge.SFN_TexCoord,id:3,x:33862,y:32782,uv:0;n:type:ShaderForge.SFN_Add,id:4,x:33110,y:32939|A-29-OUT,B-6-OUT;n:type:ShaderForge.SFN_Slider,id:6,x:33338,y:33075,ptlb:Health,ptin:_Health,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Clamp01,id:8,x:33053,y:32720|IN-4-OUT;n:type:ShaderForge.SFN_Power,id:10,x:32966,y:33217|VAL-8-OUT,EXP-24-OUT;n:type:ShaderForge.SFN_Color,id:11,x:33276,y:33194,ptlb:Full Health Color,ptin:_FullHealthColor,glob:False,c1:0.1172414,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:21,x:33479,y:33194,ptlb:Low Healt Color,ptin:_LowHealtColor,glob:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:24,x:33169,y:33330,ptlb:Power,ptin:_Power,glob:False,v1:500;n:type:ShaderForge.SFN_ValueProperty,id:26,x:33147,y:33515,ptlb:Multiply,ptin:_Multiply,glob:False,v1:0;n:type:ShaderForge.SFN_OneMinus,id:29,x:33538,y:32700|IN-3-U;n:type:ShaderForge.SFN_Lerp,id:31,x:33249,y:32459|A-21-RGB,B-11-RGB,T-6-OUT;proporder:11-21-6-24;pass:END;sub:END;*/

Shader "Custom/HealthBar" {
    Properties {
        _FullHealthColor ("Full Health Color", Color) = (0.1172414,1,0,1)
        _LowHealtColor ("Low Healt Color", Color) = (1,0,0,1)
        _Health ("Health", Range(0, 1)) = 1
        _Power ("Power", Float ) = 500
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float _Health;
            uniform float4 _FullHealthColor;
            uniform float4 _LowHealtColor;
            uniform float _Power;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_LowHealtColor.rgb,_FullHealthColor.rgb,_Health);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,pow(saturate(((1.0 - i.uv0.r)+_Health)),_Power));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
