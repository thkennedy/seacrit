// Shader created with Shader Forge Beta 0.33 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.33;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,blpr:0,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5588235,fgcg:0.5588235,fgcb:0.5588235,fgca:1,fgde:0.29,fgrn:3.5,fgrf:7.69,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-549-OUT,diffpow-249-OUT,spec-86-OUT,gloss-91-OUT,normal-5-RGB,emission-150-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33635,y:32196,ptlb:Diffuse,ptin:_Diffuse,tex:7dfeb8deb0ac00945a7a665bcf350cae,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5,x:33984,y:32997,ptlb:Normal Map,ptin:_NormalMap,tex:6ebb6d2710ee16e408a7912aba84c1dc,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:17,x:33420,y:32273|A-480-OUT,B-290-OUT;n:type:ShaderForge.SFN_Power,id:18,x:33240,y:32299|VAL-17-OUT,EXP-292-OUT;n:type:ShaderForge.SFN_ValueProperty,id:86,x:33153,y:32800,ptlb:Specular,ptin:_Specular,glob:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:91,x:33153,y:32898,ptlb:Gloss,ptin:_Gloss,glob:False,v1:1;n:type:ShaderForge.SFN_Fresnel,id:110,x:33634,y:33396|EXP-115-OUT;n:type:ShaderForge.SFN_ValueProperty,id:115,x:33806,y:33494,ptlb:Fresnel,ptin:_Fresnel,glob:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:146,x:33481,y:33507|A-110-OUT,B-147-OUT;n:type:ShaderForge.SFN_ValueProperty,id:147,x:33662,y:33619,ptlb:Fresnel Multiply,ptin:_FresnelMultiply,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:150,x:33316,y:33442|A-157-RGB,B-146-OUT;n:type:ShaderForge.SFN_Color,id:157,x:33495,y:33334,ptlb:Fresnel Color,ptin:_FresnelColor,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:249,x:33168,y:32705,ptlb:Diffuse Power Power,ptin:_DiffusePowerPower,glob:False,v1:1;n:type:ShaderForge.SFN_Color,id:261,x:33930,y:32277,ptlb:Diffuse Color,ptin:_DiffuseColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:264,x:33153,y:32512|A-18-OUT,B-261-RGB;n:type:ShaderForge.SFN_ValueProperty,id:290,x:33635,y:32398,ptlb:Diffuse Multiply,ptin:_DiffuseMultiply,glob:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:292,x:33471,y:32470,ptlb:Diffuse Power,ptin:_DiffusePower,glob:False,v1:1;n:type:ShaderForge.SFN_Lerp,id:298,x:32823,y:32553|A-503-OUT,B-314-OUT,T-325-OUT;n:type:ShaderForge.SFN_Color,id:300,x:33641,y:32975,ptlb:Accent Color,ptin:_AccentColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:304,x:34016,y:32574,ptlb:Accent,ptin:_Accent,tex:7dfeb8deb0ac00945a7a665bcf350cae,ntxv:0,isnm:False|UVIN-318-OUT;n:type:ShaderForge.SFN_ValueProperty,id:311,x:33507,y:32798,ptlb:Accent Multiply,ptin:_AccentMultiply,glob:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:314,x:33331,y:32659|A-460-OUT,B-311-OUT;n:type:ShaderForge.SFN_ValueProperty,id:316,x:34357,y:32608,ptlb:Accent Tile,ptin:_AccentTile,glob:False,v1:1;n:type:ShaderForge.SFN_TexCoord,id:317,x:34776,y:32167,uv:0;n:type:ShaderForge.SFN_Multiply,id:318,x:34068,y:32423|A-319-UVOUT,B-316-OUT;n:type:ShaderForge.SFN_Rotator,id:319,x:34340,y:32389|UVIN-418-OUT,ANG-324-OUT;n:type:ShaderForge.SFN_Slider,id:324,x:34549,y:32531,ptlb:Rotation,ptin:_Rotation,min:0,cur:0,max:360;n:type:ShaderForge.SFN_Multiply,id:325,x:33828,y:32834|A-304-A,B-327-OUT;n:type:ShaderForge.SFN_ValueProperty,id:327,x:33984,y:32893,ptlb:Accent Opacity,ptin:_AccentOpacity,glob:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:414,x:35028,y:32456,ptlb:Accent Horizotal tile,ptin:_AccentHorizotaltile,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:416,x:34822,y:32379|A-317-U,B-414-OUT;n:type:ShaderForge.SFN_Append,id:418,x:34527,y:32295|A-416-OUT,B-317-V;n:type:ShaderForge.SFN_Color,id:436,x:34579,y:32783,ptlb:Accent Color 2,ptin:_AccentColor2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:460,x:33629,y:32636|A-436-RGB,B-300-RGB,T-304-A;n:type:ShaderForge.SFN_Add,id:476,x:33927,y:31643|A-2-R,B-2-G;n:type:ShaderForge.SFN_Add,id:477,x:33737,y:31766|A-476-OUT,B-2-B;n:type:ShaderForge.SFN_Multiply,id:478,x:33517,y:31790|A-477-OUT,B-479-OUT;n:type:ShaderForge.SFN_Vector1,id:479,x:33737,y:31903,v1:0.3;n:type:ShaderForge.SFN_Lerp,id:480,x:33301,y:31987|A-2-RGB,B-478-OUT,T-481-OUT;n:type:ShaderForge.SFN_Slider,id:481,x:33542,y:32023,ptlb:BW,ptin:_BW,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:503,x:32835,y:32196|A-523-OUT,B-264-OUT,T-2-A;n:type:ShaderForge.SFN_Color,id:522,x:33025,y:31996,ptlb:Diffuse Color2,ptin:_DiffuseColor2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:523,x:32785,y:32017|A-522-RGB,B-18-OUT;n:type:ShaderForge.SFN_Tex2d,id:543,x:34285,y:33092,ptlb:Damage Alpha,ptin:_DamageAlpha,tex:d2536caf609cd8e40855e4c3ab62c357,ntxv:0,isnm:False|UVIN-318-OUT;n:type:ShaderForge.SFN_Multiply,id:547,x:33495,y:33139|A-543-A,B-556-OUT;n:type:ShaderForge.SFN_Lerp,id:549,x:33127,y:33038|A-298-OUT,B-551-RGB,T-547-OUT;n:type:ShaderForge.SFN_Color,id:551,x:34168,y:33338,ptlb:Damage Color,ptin:_DamageColor,glob:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Slider,id:556,x:33794,y:33237,ptlb:Damage,ptin:_Damage,min:0,cur:0,max:1;proporder:2-5-304-261-522-290-292-249-481-86-91-147-157-115-300-436-311-316-414-324-327-543-551-556;pass:END;sub:END;*/

Shader "Shader Forge/PlayerShader1" {
    Properties {
        _Diffuse ("Diffuse", 2D) = "white" {}
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _Accent ("Accent", 2D) = "white" {}
        _DiffuseColor ("Diffuse Color", Color) = (1,1,1,1)
        _DiffuseColor2 ("Diffuse Color2", Color) = (1,1,1,1)
        _DiffuseMultiply ("Diffuse Multiply", Float ) = 2
        _DiffusePower ("Diffuse Power", Float ) = 1
        _DiffusePowerPower ("Diffuse Power Power", Float ) = 1
        _BW ("BW", Range(0, 1)) = 0
        _Specular ("Specular", Float ) = 1
        _Gloss ("Gloss", Float ) = 1
        _FresnelMultiply ("Fresnel Multiply", Float ) = 1
        _FresnelColor ("Fresnel Color", Color) = (0.5,0.5,0.5,1)
        _Fresnel ("Fresnel", Float ) = 2
        _AccentColor ("Accent Color", Color) = (1,1,1,1)
        _AccentColor2 ("Accent Color 2", Color) = (1,1,1,1)
        _AccentMultiply ("Accent Multiply", Float ) = 2
        _AccentTile ("Accent Tile", Float ) = 1
        _AccentHorizotaltile ("Accent Horizotal tile", Float ) = 1
        _Rotation ("Rotation", Range(0, 360)) = 0
        _AccentOpacity ("Accent Opacity", Float ) = 0
        _DamageAlpha ("Damage Alpha", 2D) = "white" {}
        _DamageColor ("Damage Color", Color) = (1,0,0,1)
        _Damage ("Damage", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Fresnel;
            uniform float _FresnelMultiply;
            uniform float4 _FresnelColor;
            uniform float _DiffusePowerPower;
            uniform float4 _DiffuseColor;
            uniform float _DiffuseMultiply;
            uniform float _DiffusePower;
            uniform float4 _AccentColor;
            uniform sampler2D _Accent; uniform float4 _Accent_ST;
            uniform float _AccentMultiply;
            uniform float _AccentTile;
            uniform float _Rotation;
            uniform float _AccentOpacity;
            uniform float _AccentHorizotaltile;
            uniform float4 _AccentColor2;
            uniform float _BW;
            uniform float4 _DiffuseColor2;
            uniform sampler2D _DamageAlpha; uniform float4 _DamageAlpha_ST;
            uniform float4 _DamageColor;
            uniform float _Damage;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_599 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(node_599.rg, _NormalMap))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = pow(max( 0.0, NdotL), _DiffusePowerPower) * attenColor + UNITY_LIGHTMODEL_AMBIENT.rgb;
////// Emissive:
                float3 emissive = (_FresnelColor.rgb*(pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)*_FresnelMultiply));
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_2 = tex2D(_Diffuse,TRANSFORM_TEX(node_599.rg, _Diffuse));
                float node_478 = (((node_2.r+node_2.g)+node_2.b)*0.3);
                float3 node_18 = pow((lerp(node_2.rgb,float3(node_478,node_478,node_478),_BW)*_DiffuseMultiply),_DiffusePower);
                float node_319_ang = _Rotation;
                float node_319_spd = 1.0;
                float node_319_cos = cos(node_319_spd*node_319_ang);
                float node_319_sin = sin(node_319_spd*node_319_ang);
                float2 node_319_piv = float2(0.5,0.5);
                float2 node_317 = i.uv0;
                float2 node_319 = (mul(float2((node_317.r*_AccentHorizotaltile),node_317.g)-node_319_piv,float2x2( node_319_cos, -node_319_sin, node_319_sin, node_319_cos))+node_319_piv);
                float2 node_318 = (node_319*_AccentTile);
                float4 node_304 = tex2D(_Accent,TRANSFORM_TEX(node_318, _Accent));
                finalColor += diffuseLight * lerp(lerp(lerp((_DiffuseColor2.rgb*node_18),(node_18*_DiffuseColor.rgb),node_2.a),(lerp(_AccentColor2.rgb,_AccentColor.rgb,node_304.a)*_AccentMultiply),(node_304.a*_AccentOpacity)),_DamageColor.rgb,(tex2D(_DamageAlpha,TRANSFORM_TEX(node_318, _DamageAlpha)).a*_Damage));
                finalColor += specular;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Fresnel;
            uniform float _FresnelMultiply;
            uniform float4 _FresnelColor;
            uniform float _DiffusePowerPower;
            uniform float4 _DiffuseColor;
            uniform float _DiffuseMultiply;
            uniform float _DiffusePower;
            uniform float4 _AccentColor;
            uniform sampler2D _Accent; uniform float4 _Accent_ST;
            uniform float _AccentMultiply;
            uniform float _AccentTile;
            uniform float _Rotation;
            uniform float _AccentOpacity;
            uniform float _AccentHorizotaltile;
            uniform float4 _AccentColor2;
            uniform float _BW;
            uniform float4 _DiffuseColor2;
            uniform sampler2D _DamageAlpha; uniform float4 _DamageAlpha_ST;
            uniform float4 _DamageColor;
            uniform float _Damage;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_600 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(node_600.rg, _NormalMap))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = pow(max( 0.0, NdotL), _DiffusePowerPower) * attenColor;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_2 = tex2D(_Diffuse,TRANSFORM_TEX(node_600.rg, _Diffuse));
                float node_478 = (((node_2.r+node_2.g)+node_2.b)*0.3);
                float3 node_18 = pow((lerp(node_2.rgb,float3(node_478,node_478,node_478),_BW)*_DiffuseMultiply),_DiffusePower);
                float node_319_ang = _Rotation;
                float node_319_spd = 1.0;
                float node_319_cos = cos(node_319_spd*node_319_ang);
                float node_319_sin = sin(node_319_spd*node_319_ang);
                float2 node_319_piv = float2(0.5,0.5);
                float2 node_317 = i.uv0;
                float2 node_319 = (mul(float2((node_317.r*_AccentHorizotaltile),node_317.g)-node_319_piv,float2x2( node_319_cos, -node_319_sin, node_319_sin, node_319_cos))+node_319_piv);
                float2 node_318 = (node_319*_AccentTile);
                float4 node_304 = tex2D(_Accent,TRANSFORM_TEX(node_318, _Accent));
                finalColor += diffuseLight * lerp(lerp(lerp((_DiffuseColor2.rgb*node_18),(node_18*_DiffuseColor.rgb),node_2.a),(lerp(_AccentColor2.rgb,_AccentColor.rgb,node_304.a)*_AccentMultiply),(node_304.a*_AccentOpacity)),_DamageColor.rgb,(tex2D(_DamageAlpha,TRANSFORM_TEX(node_318, _DamageAlpha)).a*_Damage));
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
