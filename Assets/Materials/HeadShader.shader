// Shader created with Shader Forge Beta 0.31 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.31;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5588235,fgcg:0.5588235,fgcb:0.5588235,fgca:1,fgde:0.29,fgrn:3.5,fgrf:7.69,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-264-OUT,diffpow-249-OUT,spec-86-OUT,gloss-91-OUT,normal-5-RGB,emission-150-OUT,alpha-2-A;n:type:ShaderForge.SFN_Tex2d,id:2,x:33635,y:32196,ptlb:Diffuse,ptin:_Diffuse,tex:7dfeb8deb0ac00945a7a665bcf350cae,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5,x:34031,y:33132,ptlb:Normal Map,ptin:_NormalMap,tex:6ebb6d2710ee16e408a7912aba84c1dc,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:17,x:33420,y:32273|A-2-RGB,B-290-OUT;n:type:ShaderForge.SFN_Power,id:18,x:33240,y:32299|VAL-17-OUT,EXP-292-OUT;n:type:ShaderForge.SFN_ValueProperty,id:86,x:33360,y:32781,ptlb:Specular,ptin:_Specular,glob:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:91,x:33360,y:32877,ptlb:Gloss,ptin:_Gloss,glob:False,v1:1;n:type:ShaderForge.SFN_Fresnel,id:110,x:33634,y:33396|EXP-115-OUT;n:type:ShaderForge.SFN_ValueProperty,id:115,x:33806,y:33494,ptlb:Fresnel,ptin:_Fresnel,glob:False,v1:10;n:type:ShaderForge.SFN_Multiply,id:146,x:33481,y:33507|A-110-OUT,B-147-OUT;n:type:ShaderForge.SFN_ValueProperty,id:147,x:33662,y:33619,ptlb:Fresnel Multiply,ptin:_FresnelMultiply,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:150,x:33316,y:33442|A-157-RGB,B-146-OUT;n:type:ShaderForge.SFN_Color,id:157,x:33495,y:33334,ptlb:Fresnel Color,ptin:_FresnelColor,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:249,x:33360,y:32680,ptlb:Diffuse Power Power,ptin:_DiffusePowerPower,glob:False,v1:1;n:type:ShaderForge.SFN_Color,id:261,x:33260,y:32482,ptlb:Diffus Color,ptin:_DiffusColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:264,x:33042,y:32508|A-18-OUT,B-261-RGB;n:type:ShaderForge.SFN_ValueProperty,id:290,x:33635,y:32398,ptlb:Diffuse Multiply,ptin:_DiffuseMultiply,glob:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:292,x:33471,y:32470,ptlb:Diffuse Power,ptin:_DiffusePower,glob:False,v1:1;proporder:5-2-86-91-115-147-157-249-261-290-292;pass:END;sub:END;*/

Shader "Shader Forge/HeadShader" {
    Properties {
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Specular ("Specular", Float ) = 1
        _Gloss ("Gloss", Float ) = 1
        _Fresnel ("Fresnel", Float ) = 10
        _FresnelMultiply ("Fresnel Multiply", Float ) = 1
        _FresnelColor ("Fresnel Color", Color) = (0.5,0.5,0.5,1)
        _DiffusePowerPower ("Diffuse Power Power", Float ) = 1
        _DiffusColor ("Diffus Color", Color) = (1,1,1,1)
        _DiffuseMultiply ("Diffuse Multiply", Float ) = 2
        _DiffusePower ("Diffuse Power", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Fresnel;
            uniform float _FresnelMultiply;
            uniform float4 _FresnelColor;
            uniform float _DiffusePowerPower;
            uniform float4 _DiffusColor;
            uniform float _DiffuseMultiply;
            uniform float _DiffusePower;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_320 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(node_320.rg, _NormalMap))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = pow(max( 0.0, NdotL), _DiffusePowerPower) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float3 emissive = (_FresnelColor.rgb*(pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)*_FresnelMultiply));
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_2 = tex2D(_Diffuse,TRANSFORM_TEX(node_320.rg, _Diffuse));
                finalColor += diffuseLight * (pow((node_2.rgb*_DiffuseMultiply),_DiffusePower)*_DiffusColor.rgb);
                finalColor += specular;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,node_2.a);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Fresnel;
            uniform float _FresnelMultiply;
            uniform float4 _FresnelColor;
            uniform float _DiffusePowerPower;
            uniform float4 _DiffusColor;
            uniform float _DiffuseMultiply;
            uniform float _DiffusePower;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_321 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(node_321.rg, _NormalMap))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = pow(max( 0.0, NdotL), _DiffusePowerPower) * attenColor;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_2 = tex2D(_Diffuse,TRANSFORM_TEX(node_321.rg, _Diffuse));
                finalColor += diffuseLight * (pow((node_2.rgb*_DiffuseMultiply),_DiffusePower)*_DiffusColor.rgb);
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * node_2.a,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
